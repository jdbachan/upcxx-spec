\chapter{Collectives}
\label{ch:collectives}

\pnum
A \emph{collective operation} is a \upcxx operation that must be
matched across all participating processes. Informally, any two
processes that both participate in a pair of collective operations
must agree on their ordering. Furthermore, if a parameter or other
property of a
collective operation is specified as \emph{single-valued}, all
participating processes must provide the same value for the parameter
or property.

\pnum
A collective operation need not provide any actual synchronization
between processes, unless otherwise noted. The collective requirement
simply states a relative ordering property of calls to collective
operations that must be maintained in the parallel execution trace for
all executions of any valid program. Some implementations may include
unspecified synchronization between processes within collective
operations, but programs must not rely upon the presence or absence of
such unspecified synchronization for correctness.

\pnum
\upcxx provides several collective communication operations over
teams, described below.

\section{Common Requirements}
\label{sec:collective_requirements}

\pnum
For an execution of a \upcxx program to be valid, the collective
operations invoked by the program must obey the
following ordering constraints:
\begin{itemize}
\pitem For a collective operation $C$ over a team $T$, let
  $Participants(C)$ denote the set of processes that are members of
  $T$.
\pitem For a process $P \in Participants(C_1) ~\cap~
  Participants(C_2)$, let $Precedes_P(C_1, C_2)$ be true if and only
  if $C_1 \neq C_2$ and $C_1$ is initiated before $C_2$ on $P$.
\pitem Let $Collectives$ be the set of collective operations invoked
  during execution of the program. The collectives must satisfy the
  following property:
  \begin{equation}
    \begin{split}
      \forall C_1,&C_2 \in Collectives .~
      \forall P,Q \in Participants(C_1) \cap Participants(C_2) .~\\
      &Precedes_P(C_1, C_2) = Precedes_Q(C_1, C_2)
    \end{split}
  \end{equation}
\end{itemize}

\pnum
The constraints above formalize the notion that any two processes that
both participate in a pair of collectives must agree on their
ordering.

\pnum
A collective operation must be invoked by the thread that has the
master persona (\S\ref{sec:persona}) of a process.

\section{API Reference}

\begin{plstlisting}
enum class entry_barrier {
  none,
  internal,
  user
};
\end{plstlisting}
\Indx{entry\_barrier@{\tt entry\_barrier}}
\begin{quote}
\pnum
Constants used with some \upcxx operations to specify the entry
barrier to be used by the operation:
\begin{pitemize}
\item \code{none}: the operation has no entry barrier
\item \code{internal}: the operation should perform a barrier
  at entry that makes only internal-level progress
\item \code{user}: the operation should perform a barrier at
  entry that makes user-level progress
\end{pitemize}
\end{quote}

\begin{plstlisting}
void barrier(team &team = world());
\end{plstlisting}
\Indx{barrier@{\tt barrier}}
\begin{quote}
\collective{the given team}

\pnum
Performs a barrier operation over the given team. The call will not
return until all processes in the team have entered the call. There is no
implied relationship between this call and other in-flight operations.
This call will invoke user-level progress, so the caller may
expect incoming \RPCs to fire before it returns.

\ordering With respect to all threads participating in
this collective, all evaluations which are {\em sequenced-before} their
respective thread's invocation of this call will have a
{\em happens-before} relationship with all evaluations sequenced after
the call.

\progresslevel{user}
\end{quote}

\begin{plstlisting}
template<typename Completions=decltype(operation_cx::as_future())>
RType barrier_async(team &team = world(),
                    Completions cxs=Completions{});
\end{plstlisting}
\Indx{barrier\_async@{\tt barrier\_async}}
\begin{quote}
\collective{the given team}

\pnum
Initiates an asynchronous barrier operation over the given team. The
call will return without waiting for other processes to make the call.
Operation completion will be signaled after all other processes in the
team have entered the call.

\begin{completions}
\item{Operation} Indicates completion of the collective from the
  viewpoint of the caller, implying that all processes in the given team
  have entered the collective.
\end{completions}

\ordering With respect to all threads participating in
this collective, all evaluations which are {\em sequenced-before} their
respective thread's invocation of this call will have a
{\em happens-before} relationship with all evaluations sequenced after
the operation-completion notification actions (future readying,
promise fulfillment, or persona LPC enlistment).

\progresslevel{internal}
\end{quote}

\begin{plstlisting}
constexpr /*unspecified*/ op_fast_add;
constexpr /*unspecified*/ op_fast_mul;
constexpr /*unspecified*/ op_fast_min;
constexpr /*unspecified*/ op_fast_max;
constexpr /*unspecified*/ op_fast_bit_and;
constexpr /*unspecified*/ op_fast_bit_or;
constexpr /*unspecified*/ op_fast_bit_xor;
\end{plstlisting}
\Indx{op\_fast\_add@{\tt op\_fast\_add}}
\Indx{op\_fast\_mul@{\tt op\_fast\_mul}}
\Indx{op\_fast\_min@{\tt op\_fast\_min}}
\Indx{op\_fast\_max@{\tt op\_fast\_max}}
\Indx{op\_fast\_and@{\tt op\_fast\_bit\_and}}
\Indx{op\_fast\_bit\_or@{\tt op\_fast\_bit\_or}}
\Indx{op\_fast\_bit\_xor@{\tt op\_fast\_bit\_xor}}
\begin{quote}

\pnum
Instances of unspecified function-object types that have the following
overloaded function-call operator:

\begin{plstlisting}
  T operator()(T a, T b) const;
\end{plstlisting}

\pnum
The unspecified function-object types meet the requirements for the
\code{BinaryOp} template parameter to \code{reduce_one} and
\code{reduce_all} (e.g. they are referentially transparent and
concurrently invocable).

\pnum
For \code{op_fast_add}, \code{op_fast_mul}, \code{op_fast_min}, and
\code{op_fast_max}, the allowed types for \code{T} are those for which
\code{std::is_arithmetic<T>::value} is true. For
\code{op_fast_bit_and}, \code{op_fast_bit_or}, and
\code{op_fast_bit_xor}, the allowed types for \code{T} are those for
which \code{std::is_integral<T>::value} is true.

\begin{sloppypar}
\pnum
The operation performed by the function-call operator is,
respectively: binary \code{+}, binary \code{*}, \code{std::min},
\code{std::max}, binary \code{&}, \code{|}, and \code{^}.
If \code{T} is \code{bool}, then \code{op_fast_add} and
\code{op_fast_max} perform the same operation as
\code{op_fast_bit_or}, and \code{op_fast_mul} and \code{op_fast_min}
perform the same operation as \code{op_fast_bit_and}.
\end{sloppypar}
\end{quote}

\begin{plstlisting}
template<typename T, typename BinaryOp,
         typename Completions=decltype(operation_cx::as_future())>
RType reduce_one(T &&value, BinaryOp &&op,
                 intrank_t root, team &team = world(),
                 Completions cxs=Completions{});
template<typename T, typename BinaryOp,
         typename Completions=decltype(operation_cx::as_future())>
RType reduce_all(T &&value, BinaryOp &&op, team &team = world(),
                 Completions cxs=Completions{});

template<typename T, typename BinaryOp,
         typename Completions=decltype(operation_cx::as_future())>
RType reduce_one(const T *src, T *dst, size_t count,
                 BinaryOp &&op,
                 intrank_t root, team &team = world(),
                 Completions cxs=Completions{});
template<typename T, typename BinaryOp,
         typename Completions=decltype(operation_cx::as_future())>
RType reduce_all(const T *src, T *dst, size_t count,
                 BinaryOp &&op, team &team = world(),
                 Completions cxs=Completions{});
\end{plstlisting}
\Indx{reduce\_one@{\tt reduce\_one}}
\Indx{reduce\_all@{\tt reduce\_all}}
\begin{quote}

\collective{the given team}

\precondition \code{T} must be
DefinitelyTriviallySerializable. \code{BinaryOp} must be a
function-object type representing
an associative and commutative mathematical operation taking two values
of type \code{T} and returning a value implicitly convertible to
\code{T}. \code{BinaryOp} must be referentially transparent and
concurrently invocable. \code{BinaryOp} may not invoke any \upcxx
routine with a progress level other than none.
In the first and third variants, \code{root} must be single-valued
and a valid rank in \code{team}.
In the third variant, \code{src} and \code{dst} on the process whose
rank is \code{root} in the team may be equal but must not otherwise
overlap, and \code{count} must be single-valued across all
participants.
In the fourth variant, \code{src} and \code{dst} may be
equal but must not otherwise overlap, and \code{src == dst} and
\code{count} must both be single-valued.

\pnum
Performs a reduction operation over the processes in the given team.

\pnum
If the team contains only a single process, then the resulting
operation completion will produce \code{value} in the first two
variants. In the latter two variants, the contents of \code{src} will
be copied to \code{dst} if \code{src != dst}.

\pnum
If the team contains more than one process, initiates an asynchronous
reduction over
the values provided by each process. The reduction is performed in some
non-deterministic order by applying \code{op} to combine values and
intermediate results.
In the second and fourth variants, the order in which \code{op} is
applied may differ between processes, so the results may differ if
\code{op} is not fully associative and commutative (as with
floating-point arithmetic on some operands).
In the third and fourth variants, the contents of \code{src} are combined
element-wise across the processes in the team, with the results placed
in \code{dst}.

\pnum
In the first variant, the process whose rank is \code{root} in
\code{team} receives the result of the reduction as part of operation
completion, while the remaining processes receive an undefined value.

\pnum
In the second variant, each process receives the result of the
reduction as part of operation completion.

\pnum
In the third variant, operation completion signifies that the results
have been stored in \code{dst} on the process whose rank is
\code{root} in \code{team} and that \code{src} is no longer in use by
the reduction.
On the remaining processes, the argument \code{dst} is ignored, and
operation completion signifies that \code{src} is no longer in use by
the reduction.

\pnum
In the fourth variant, operation completion on each process signifies
that the results have been stored in \code{dst} on that process and
that \code{src} is no longer in use by the reduction.

\pnum
\emph{Advice to users:}
If \code{op} is one of \code{op_fast_*} and \code{T} is one of the
allowed types for \code{op}, implementations may offload the reduction
operations to the hardware network interface controller.

\begin{completions}
\item{Operation} Indicates completion of the collective from the
  viewpoint of the caller, implying that the results of the reduction
  are available to this process as described above. In the third and
  fourth variants, also signifies that the \code{src} buffer may be
  modified. In the first two
  variants, this completion produces a value of type \code{T}. In the
  latter two variants, this completion does not produce a value.
\end{completions}

\ordering With respect to all threads participating in
this collective, if a thread receives the results of the collective
(the calling thread on the root process in the first and third
variants; all calling threads in the second and fourth variants), all
evaluations which are {\em sequenced-before} that thread's invocation
of this call will have a
{\em happens-before} relationship with all evaluations sequenced after
the operation-completion notification actions (future readying,
promise fulfillment, or persona LPC enlistment).

\progresslevel{internal}
\end{quote}

\begin{plstlisting}
template<typename T,
         typename Completions=decltype(operation_cx::as_future())>
RType broadcast(T &&value, intrank_t root,
                team &team = world(),
                Completions cxs=Completions{});

template<typename T,
         typename Completions=decltype(operation_cx::as_future())>
RType broadcast(T *buffer, std::size_t count,
                intrank_t root, team &team = world(),
                Completions cxs=Completions{});
\end{plstlisting}
\Indx{broadcast@{\tt broadcast}}
\begin{quote}
\collective{the given team}

\precondition \code{root} must be single-valued and a valid rank in
\code{team}.
In the second variant, \code{count} must be single-valued.
The type \code{T} must be DefinitelyTriviallySerializable.

\pnum
Initiates an asynchronous broadcast (one-to-all) operation, with rank
\code{root} in \code{team} acting as the producer of the broadcast. In the first
variant, \code{value} will be asynchronously sent to all processes in the
team, encapsulated in operation completion, which will be signaled upon
receipt of the value. In the second variant, the objects in
\code{[buffer,buffer+count)} of rank \code{root} in \code{team} are sent to the
addresses \code{[buffer,buffer+count)} provided by the receiving
processes. Operation completion signals completion of the operation with
respect to the calling process. For the root, this indicates that the
given buffer is available for reuse, and for a receiver, it indicates
that the data have been received in its buffer.

\begin{completions}
\item{Operation} In the first variant, indicates that the value
  provided by the root is available at the caller, and this completion
  produces a value of type \code{T}.
  In the second variant, indicates completion of the collective from
  the viewpoint of the caller as described above, and this completion
  does not produce a value.
\end{completions}

\ordering With respect to all threads participating
in this collective, all evaluations which are {\em sequenced-before}
the producing thread's invocation of this call will have a {\em
  happens-before} relationship with all evaluations sequenced after
the operation-completion notification actions (future readying,
promise fulfillment, or persona LPC enlistment).

\progresslevel{internal}
\end{quote}
