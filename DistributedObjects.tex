\chapter{Distributed Objects}
\label{ch:distobject}

\commentout{
\noindent
\begin{plstlisting}[frame=single,style=upcxx] 
namespace upcxx {
 template<typename T>
 struct dist_object {
   dist_object(T value, team &team = world()); 
   template<typename ...Arg> 
   dist_object(team &team, Arg ...arg);
   ~dist_object();
   T* operator->() const;
   T& operator*() const;
   dist_id<T> id() const;
 };

 template<typename T>
 struct dist_id {
   dist_object<T>& here() const;
   future<dist_object<T>&> when_here() const;
 };
} // namespace upcxx
\end{plstlisting}
}


\section{Overview}
\pnum
In distributed-memory parallel programming, the concept of a single logical object 
partitioned over several processes is a useful capability in many contexts:
for example, geometric meshes, vectors, matrices, tensors, and associative maps.
Since \upcxx is a
communication library, it strives to focus on the mechanisms of 
communication as opposed to the various programming idioms for managing
distribution.  However,  a basic framework for users to
implement their own distributed objects is useful and also
enables \upcxx
to provide the user with the following valuable features:

\begin{penumerate}

\item{Universal distributed object naming: per-object names that can be transmitted
to other processes while retaining their meaning}.

\item{Name-to-this mapping: Mapping between the universal name and the
calling process's memory address holding that distributed object's state for the process
(the calling process's \code{this} pointer).}

\end{penumerate}

\pnum
The need for universal distributed object naming stems primarily from \RPC-based 
communication. If one process needs to remotely invoke code on a peer's
partition of a distributed object, there needs to be 
some mutually agreeable identifier for referring to that distributed
object. For simplicity, this 
identifier value should be: identical across all  processes so that it 
may be freely communicated while maintaining its meaning. Moreover,
the name should be TriviallyCopyable so that it may be serialized into 
\RPCs efficiently (including with the auto-capture \code{[=]} lambda 
syntax), hashable, and comparable so that it works well with standard
{\tt C++} containers. \upcxx provides distributed object names meeting these
criteria as well as the registry for mapping names to and from the
calling process's partition of the distributed object.
%object instance reference (\code{this}).

\section{Building Distributed Objects}

\pnum
Distributed objects are built with the \code{upcxx::dist_object<T>} constructor
over a specific \team (defaulting to team \code{world()}).
For all processes in the given team, each process constructs an instance of 
\code{dist_object<T>}, supplying a value of type \code{T} representing 
this process's instance value. All processes in the team must call this constructor collectively. 
Once construction completes, the distributed object has a universal 
name which can be used by any rank in the team to locate the resident
instance. When the \code{dist_object<T>} is destructed the \code{T} 
value is also destructed. At this point the name will cease to carry 
meaning on this process. Thus, the programmer should ensure that no process
destructs a distributed object until all name lookups destined for it
complete and all hanging references of the form \code{T&} or \code{T*}
to the value have expired.

\pnum
The names of \code{dist_object<T>}'s are encoded by the 
\code{dist_id<T>} type. This type is TriviallyCopyable, 
EqualityComparable, LessThanComparable, hashable, and
DefinitelyTriviallySerializable. It has the members \code{.here()} and
\code{.when_here()} for retrieving the resident \code{dist_object<T>}
instance registered with the name.

\section{Ensuring Distributed Existence}

\pnum
The \code{dist_object<T>} constructor requires it be called in a 
collective context, but it does not guarantee that, after the call, all 
other ranks in the team have exited or even reached the constructor.
Thus users are required to guard against the possibility that when an
\RPC carrying an distributed object's name executes, the recipient process may not yet have an
entry for that name in its registry. Possible ways to deal with this
include:

\begin{penumerate}

\item{Barrier: Before issuing communication containing a \code{dist_id<T>}
for a newly created distributed object, the relevant team completes a \barrier
to ensure global existence of the \code{dist_object<T>}.}

\item{Point to point: Before communicating a \code{dist_id<T>} with a given process, 
the initiating process uses some two-party protocol to ensure that the peer has 
constructed the \code{dist_object<T>}.}

\item{Asynchronous point-to-point: The user performs no synchronization to 
ensure remote existence. Instead, an \RPC is sent which, upon 
arrival, must wait asynchronously via a continuation for the peer to
construct the distributed object.}

\end{penumerate}

\pnum
\upcxx enables the asynchronous point-to-point approach implicitly when
\code{dist_object<T>&} arguments are given to any of the \RPC family
of functions (see Ch. \ref{ch:rpc}).

\section{API Reference}

\begin{plstlisting}
template<typename T>
class dist_object;
\end{plstlisting}
\Indx{dist\_object@{\tt dist\_object}}
\begin{quote}
\concepts MoveConstructible, Destructible
\end{quote}

\begin{plstlisting}
template<typename T>
dist_object<T>::dist_object(T value, team &team = world());
\end{plstlisting}
\Indx{dist\_object@{\tt dist\_object}!constructor}
\begin{quote}
\collective{the given team}

\pnum
Constructs this process's member of the distributed object identified by
the collective calling context across \code{team}. The initial value
for this process is given in \code{value}. The future returned from
\code{dist_id<T>::when_here}
for the corresponding \code{dist_id<T>}
will be readied during this constructor.
This implies that continuations waiting for that future will execute
before the constructor returns.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
template<typename T>
template<typename ...Arg>
dist_object<T>::dist_object(team &team, Arg &&...arg);
\end{plstlisting}
\Indx{dist\_object@{\tt dist\_object}!variadic constructor}
\begin{quote}
\collective{the given team}

\pnum
Constructs this process's member of the distributed object identified by
the collective calling context across \code{team}. The initial value
for this process is constructed with \code{T(std::forward<Arg>(arg)...)}.
The result is undefined if this call throws an exception.
The future returned from \code{dist_id<T>::when_here}
for the corresponding \code{dist_id<T>}
will be readied
during this constructor. This implies that continuations waiting for
that future will execute before the constructor returns.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
template<typename T>
dist_object<T>::dist_object(dist_object<T> &&other);
\end{plstlisting}
\Indx{dist\_object@{\tt dist\_object}!move constructor}
\begin{quote}
\precondition Calling thread must have the master persona.

\pnum
Makes this instance the calling process's representative of the
distributed object associated with \code{other}, transferring all
state from \code{other}. Invalidates \code{other}, and any subsequent
operations on \code{other}, except for destruction, produce undefined
behavior.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
template<typename T>
dist_object<T>::~dist_object();
\end{plstlisting}
\Indx{dist\_object@{\tt dist\_object}!destructor}
\begin{quote}
\precondition Calling thread must have the master persona.

\pnum
If this instance has not been invalidated by being passed to the move
constructor, then this will destroy the calling process's member of the
distributed object. \code{\~T()} will be invoked on the resident
instance, and further lookups on this process using the \code{dist_id<T>}
corresponding to this distributed object will have undefined behavior.
If this instance has been invalidated by a move, then this call will
have no effect.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
template<typename T>
dist_id<T> dist_object<T>::id() const;
\end{plstlisting}
\Indx{dist\_object@{\tt dist\_object}!id@{\tt id}}
\begin{quote}
\pnum
Returns the \code{dist_id<T>} representing the universal name of this distributed object.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
template<typename T>
team& dist_object<T>::team() const;
\end{plstlisting}
\Indx{dist\_object@{\tt dist\_object}!team@{\tt team}}
\begin{quote}
\precondition The \code{team} associated with this distributed
object must not have been destroyed.

\pnum
Retrieves a reference to the \code{team} instance associated with this
distributed object.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
template<typename T>
T* dist_object<T>::operator->() const;
\end{plstlisting}
\Indx{dist\_object@{\tt dist\_object}!operator->@{\tt operator->}}
\begin{quote}
\pnum
Access to the calling process's value instance for this distributed object.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
template<typename T>
T& dist_object<T>::operator*() const;
\end{plstlisting}
\Indx{dist\_object@{\tt dist\_object}!operator*@{\tt operator*}}
\begin{quote}
\pnum
Access to the calling process's value instance for this distributed object.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
template<typename T>
future<T> dist_object<T>::fetch(intrank_t rank) const;
\end{plstlisting}
\Indx{dist\_object@{\tt dist\_object}!fetch@{\tt fetch}}
\begin{quote}
\precondition \code{rank} must be a valid ID in the team
associated with this distributed object. \code{T} must be
DefinitelySerializable but not \code{view<U, IterType>}. \code{rank}'s
instance of this distributed object must
not have been destroyed by the owning process. 
The \code{team} associated with this
distributed object must not have been destroyed.

\pnum
Asynchronously retrieves a copy of the instance of this distributed
object associated with the peer index \code{rank} in this distributed
object's team. The result is encapsulated in the returned future. This
call is equivalent to:

\begin{plstlisting}
  rpc(team()[rank],
      [](dist_object<T> &obj) { return *obj; },
      *this)
\end{plstlisting}

\progresslevel{internal}
\end{quote}

\begin{plstlisting}
template<typename T>
struct dist_id<T>;
\end{plstlisting}
\Indx{dist\_id@{\tt dist\_id}}
\begin{quote}
\concepts DefaultConstructible, TriviallyCopyable,
StandardLayoutType, EqualityComparable, LessThanComparable, hashable

\uconcepts DefinitelyTriviallySerializable
\end{quote}

\begin{plstlisting}
template<typename T>
dist_id<T>::dist_id();
\end{plstlisting}
\Indx{dist\_id@{\tt dist\_id}!default constructor}
\begin{quote}
\pnum
Initializes this name to be an invalid ID.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
template<typename T>
future<dist_object<T>&> dist_id<T>::when_here() const;
\end{plstlisting}
\Indx{dist\_id@{\tt dist\_id}!when\_here@{\tt when\_here}}
\begin{quote}
\precondition This name must be a valid ID for the calling process. 
The \code{dist_object<T>} instance owned by the calling process that is
associated with this name must not have been destroyed. The calling
thread must have the master persona.

\begin{sloppypar}
\pnum
Retrieves a future representing when the calling process constructs the 
\code{dist_object<T>} corresponding to this name.
\end{sloppypar}

\progresslevel{none}
\end{quote}

\begin{plstlisting}
template<typename T>
dist_object<T>& dist_id<T>::here() const;
\end{plstlisting}
\Indx{dist\_id@{\tt dist\_id}!here@{\tt here}}
\begin{quote}
\precondition This name must be a valid ID for the calling process. 
The \code{dist_object<T>} instance owned by the calling process that is
associated with this name must have been previously constructed but
not yet destroyed. 
The calling thread must have the master persona.

\pnum
Retrieves a reference to the calling process's \code{dist_object<T>}
instance associated with this name.

\progresslevel{none}
\end{quote}
