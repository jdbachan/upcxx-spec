\chapter{Futures and Promises}
\label{ch:futures}

\commentout{
\noindent
\begin{plstlisting}[frame=single,style=upcxx] 
namespace upcxx {

 template<typename ...T>
 class future;

 template<typename ...T>
 future<T...> make_future(T ...results);

 template<typename ...T>
 bool future<T...>::ready() const;

 template<typename ...T>
 std::tuple<T...> const& future<T...>::result_tuple() const;

 template<typename ...T>
 template<int I=0>
 future_element_t<I, future<T...>> future<T...>::result() const;

 template<typename ...T>
 template<int I=0>
 future_element_moved_t<I, future<T...>>
   future<T...>::result_moved();

 template<typename ...T>
 template<typename Func>
 future_invoke_result_t<Func, T...> future<T...>::then(Func func);

 template<typename ...Futures>
 future<CTypes...> when_all(Futures ...fs);

 template<typename ...T>
 future<CTypes...> to_future(T ...futures_or_results);

 template<typename ...T>
 future_element_t<0, future <T...>> wait(future<T...> f);

 template<typename ...T>
 class promise;

 template<typename ...T>
 promise<T...>::promise();

 template<typename ...T>
 void promise<T...>::require_anonymous(std::intptr_t count);

 template<typename ...T>
 template<typename ...U>
 void promise<T...>::fulfill_result(U &&...results);

 template<typename ...T>
 void promise<T...>::fulfill_anonymous(std::intptr_t count);

 template<typename ...T>
 void promise<T...>::finalize_anonymous();

 template<typename ...T>
 future<T...> promise<T...>::get_future() const;

} // namespace upcxx
\end{plstlisting}
}

\section{Overview}
\pnum
In \upcxx, the primary mechanisms by which a programmer interacts with
non-blocking operations are futures and promises.\footnote{Another mechanism, persona-targeted continuations, is discussed in \S\ref{sec:personas}.} These two mechanisms,
usually bound together under the umbrella concept of \emph{futures},
are present in the {\tt C++11} standard. However, while we borrow some
of the high-level concepts of {\tt C++}'s futures, many of
the semantics of \code{upcxx::future} and \code{upcxx::promise} differ
from those of \code{std::future} and \code{std::promise}. In
particular, while futures in {\tt C++} facilitate communicating
between threads, the intent of \upcxx futures is solely to provide an
interface for managing and composing non-blocking operations, and they
cannot be used directly to communicate between threads or processes.

\pnum
A non-blocking operation is associated with a state that encapsulates
both the status of the operation as well as any result values. Each
such operation has an associated \emph{promise} object, which can
either be explicitly created by the user or implicitly by the runtime
when a non-blocking operation is invoked. A promise represents the
producer side of the operation, and it is through the promise that the
results of the operation are supplied and its dependencies fulfilled.
A \emph{future} is the interface through which the status of the
operation can be queried and the results retrieved, and multiple
future objects may be associated with the same promise. A
future thus represents the consumer side of a non-blocking operation.

\section{The Basics of Asynchronous Communication}
\pnum
A programmer can invoke a non-blocking operation to be serviced by
another process, such as a one-sided get operation (Ch. \ref{ch:rputrget})
or a remote procedure call (Ch. \ref{ch:rpc}). Such an operation
creates an implicit promise and returns an associated future object to
the user. When the operation completes, the future becomes ready, and
it can be used to access the results. The following demonstrates an example using
a remote get (see Ch. \ref{ch:progress} on how to make progress with \upcxx):

\begin{numlisting}
global_ptr<double> ptr = /* obtain some remote pointer */;
future<double> fut = rget(ptr);          // initiate a remote get
// ...call into upcxx::progress() elided...
if (fut.ready()) {                       // check for readiness
  double value = fut.result();           // retrieve result
  std::cout << "got: " << value << '\n'; // use result
}
\end{numlisting}

\pnum
In general, a non-blocking operation will not complete immediately, so
if a user needs to wait on the readiness of a future, they must do so
in a loop. To facilitate this, we provide the \code{wait} member function,
which waits on a future to complete while ensuring that sufficient
progress (Ch. \ref{ch:progress}) is made on internal and user-level
state:

\begin{numlisting}
global_ptr<double> ptr = /* obtain some remote pointer */;
future<double> fut = rget(ptr);          // initiate a remote get
double value = fut.wait();               // wait for completion and
                                         //   retrieve result
std::cout << "got: " << value << '\n';   // use result
\end{numlisting}

\pnum
An alternative to waiting for completion of a future is to attach a
\emph{callback} or \emph{completion handler} to the future, to be
executed when the future completes. This callback can be any function object,
including lambda (anonymous) functions, that can
be called on the results of the future, and is attached
using \code{then}.

\begin{numlisting}
global_ptr<double> ptr = /* obtain some remote pointer */;
auto fut = 
rget(ptr).then(   // initiate a remote get and register a callback
  // lambda callback function
  [](double value) {
    std::cout << "got: " << value << '\n';   // use result
  }
);
\end{numlisting}

\pnum
The return value of \code{then} is another future representing the
results of the callback, if any. This permits the specification of a
sequence of operations, each of which depends on the results of the
previous one.

\pnum
A future can also represent the completion of a combination of several
non-blocking operations. Unlike the standard {\tt C++} future,
\code{upcxx::future} is a variadic template, encapsulating an
arbitrary number of result values that can come from different
operations. The following example constructs a future that represents
the results of two existing futures:

\begin{numlisting}
future<double> fut1 = /* one future */;
future<int> fut2 = /* another future */;
future<double, int> combined = when_all(fut1, fut2);
\end{numlisting}

\pnum
Here, \code{combined} represents the state and results of two futures,
and it will be ready when both \code{fut1} and \code{fut2} are ready.
The results of \code{combined} are a \code{std::tuple} whose
components are the results of the source futures.

\section{Working with Promises}

\pnum
In addition to the implicit promises created by non-blocking
operations, a user may explicitly create a promise object, obtain
associated future objects, and then register non-blocking operations
on the promise. This is useful in several cases, such as when a future
is required before a non-blocking operation can be initiated, or where
a single promise is used to count dependencies.

\commentout{
Several non-blocking operations in \upcxx provide promise-based
interfaces. These operations fulfill the results of a promise upon
completion, implicitly acting as the producer. The following is an
example of using a promise with a remote get:

\begin{numlisting}
global_ptr<double> ptr = /* obtain some remote pointer */;
promise<double> prom;                   // explicit creation
future<double> fut = prom.get_future(); // store future somewhere
/* do some other work */
rget(ptr, operation_cx::as_promise(prom));  // pass promise to rget
double value = fut.wait();              // wait for completion and
                                        //   retrieve result
std::cout << "got: " << value << '\n';  // use result
\end{numlisting}
}

\pnum
A promise can also be used to count \emph{anonymous dependencies},
keeping track of operations that complete without producing a value.
Upon creation, a promise has a dependency count of one, representing
the unfulfilled results or, if there are none, an anonymous
dependency. Further anonymous dependencies can then be registered on
the promise. When registration is complete, the original dependency
can then be fulfilled to signal the end of registration. The following
example keeps track of several remote put operations with a single
promise:

\begin{numlisting}
global_ptr<int> ptrs[10] = /* some remote pointers */;
// create a promise with no results
// the dependency count starts at one
promise<> prom;

// do 10 puts, registering each of them on the promise
for (int k = 0; k < 10; k++) {
  // rput implicitly registers itself on the given promise
  rput(k, ptrs[k], operation_cx::as_promise(prom));
}

// fulfill initial anonymous dependency, since registration is done
future<> fut = prom.finalize();

// wait for the rput operations to complete
fut.wait();
\end{numlisting}


\section{Advanced Callbacks}

\pnum
Polling for completion of a future allows simple overlap of
communication and computation operations. However, it introduces
the need for synchronization, and this requirement can diminish
the benefits of overlap. To this end, many programs can benefit from the use
of callbacks. Callbacks avoid the need
for an explicit wait and enable reactive control flow:
future completion  triggers a callback.
Callbacks allow operations to occur as
soon as they are capable of executing, rather than artificially waiting
for an unrelated operation to complete before being initiated.

\pnum
Futures are the core abstraction for obtaining asynchronous results,
and an API that supports asynchronous behavior can work with futures rather than
values directly. Such an API can also work with immediately available
values by having the caller wrap the values into a ready future using
the \code{make_future} function template, as in this example that creates a future for an ordered pair of a \code{double} and an \code{int}:

\begin{numlisting}
void consume(future<int, double> fut);
consume(make_future(3, 4.1));
\end{numlisting}

\pnum
Given a future, we can attach a callback to be executed at some
subsequent point when the future is ready using the \lstinline{then}
member function:

\begin{numlisting}
future<int, double> source = /* obtain a future */;
future<double> result = source.then(
  [](int x, double y) {
    return x + y;
  }
);
\end{numlisting}

\pnum
\commentout{
The receiver of the member function is a future encapsulating some
values that will be ready at some subsequent point.
}
In this example,
\code{source} is a future representing an \code{int} and a
\code{double} value. The argument of the call to \code{then} must be a
function object that can be called on these values. Here, we use a
lambda function that takes in an \code{int} and a \code{double}. The
call to \code{then} returns a future that represents the result of
calling the argument of \code{then} on the values contained in \code{source}.
Since
the lambda function above returns a \code{double}, the result of
\code{then} is a \code{future<double>} that will hold the double's value when
it is ready.

\pnum
In the example just shown, the result of \code{then()} is obtained by wrapping
the return type inside a future.
However,
there is also another case, when the
callback function returns a future rather than a non-future type (\code{double} in the previous example)
In this case, the result of \code{then()} does not include the step
of wrapping the return value in a future, since
we are already returning a future.
Thus, the result of the call
to \code{then} has the same type as the return type of the callback.
However, there is an important difference: the result is a future, which may or may not be ready. In the first case, it is the returned  non-future  value 
that may or may or may not be ready. This subtle difference, allows
the \upcxx programmer to chain the results of one asynchronous operation into
the inputs of the next, to arbitrary degree of nesting.

% the resulting future is not necessarily in the ready state, since the callback depends on a future value that may not itself be
% ready:

\begin{numlisting}
future<int, double> source = /* obtain a future */;
future<double> result = source.then(
  [](int x, double y) {
    // return a future<double> that is ready
    return make_future(x + y);
  }
);
// result may not be ready, since the callback will not be executed
// until source is ready
\end{numlisting}

\pnum
A callback may also initiate new asynchronous work and return a future
representing the completion of that work:

\begin{numlisting}
global_ptr<int> remote_array = /* some remote array */;

// retrieve remote_array[0]
future<int> elt0 = rget(remote_array);

// retrieve remote_array[remote_array[0]]
future<int> elt_indirect = elt0.then(
  [=](int index) {
    return rget(remote_array + index);
  }
);
\end{numlisting}

\pnum
The \code{then} member function is a combinator for constructing
pipelines of transformations over futures. Given a future and a
function that transforms that future's value into another value,
\code{then} produces a future representing the transformed
value. For example, we can transform, via a future, the value of
\code{elt_indirect} above as follows:

\begin{numlisting}
future<int> elt_indirect_squared = elt_indirect.then(
  [](int value) {
    return value * value;
  }
);
\end{numlisting}

\pnum
As the examples above demonstrate, the \code{then} member function
allows a callback to depend on the result of another future. A more
general pattern is for an operation to depend on the results of
multiple futures. The \code{when_all} function template enables this
by constructing a single future that combines the results of multiple
futures. We can then register a callback on the combined future:

\begin{numlisting}
future<int> value1 = /* ... */;
future<double> value2 = /* ... */;

future<int, double> combined = when_all(value1, value2);
future<double> result = combined.then(
  [](int x, double y) {
    return x + y;
  }
);
\end{numlisting}

\pnum
In the more general case, we may need to combine heterogeneous
mixtures of future and non-future types. The \code{to_future} function
template wraps a non-future value in a future while leaving future
values unchanged. Thus, we can use \code{when_all} along with
\code{to_future} to construct a single future that represents the
combination of both future and non-future values:

\begin{numlisting}
future<int> value1 = /* ... */;
double      value2 = /* ... */;

future<int, double> combined = when_all(to_future(value1),
                                        to_future(value2));
future<double> result = combined.then(
  [](int x, double y) {
    return x + y;
  }
);
\end{numlisting}

\pnum
The results of a ready future can be obtained as a
\code{std::tuple} using the \code{result_tuple} member function.
Individual components can be retrieved by value with the
\code{result} member function template or by r-value reference with
\code{result_moved}. Unlike with \code{std::get}, it is not a
compile-time error to use an invalid index with \code{result} or
\code{result_moved}; instead, the return type is \code{void} for an
invalid index.
This simplifies writing generic functions on futures,
such as the following definition of \code{wait}:

\begin{numlisting}
template<typename ...T>
template<int I=-1>
auto future<T...>::wait() {   // C++14-style decl for brevity
  while (!ready()) {
    progress();
  }
  return result<I>();
}
\end{numlisting}

\section{Execution Model}

\pnum
While 
some software frameworks provide thread-level
parallelism by considering each callback to be a task that can be run
in an arbitrary worker thread, this is not the case in \upcxx.
In order to maximize performance, our approach to futures is purposefully
ambivalent to issues of concurrency.
A \upcxx implementation is
allowed to take action as if the current thread is the only one that
needs to be accounted for. This restriction gives rise to a natural execution
policy: callbacks registered against futures are always executed as
soon as possible by the thread that discovers them. There are exactly
two scenarios in which this may happen:

\begin{penumerate}
\item{When a promise is fulfilled.}
\item{A callback is registered onto a ready future using the
  \code{then} member function.}
\end{penumerate}

\pnum
Fulfilling a promise 
(via \code{fulfill_result}, \code{fulfill_anonymous} or \code{finalize}) 
is the only operation that can change an associated future
from a non-ready to a ready state, enabling callbacks that depend on
it to execute. Thus, promise fulfillment is an obvious place for
discovering and executing such callbacks. Whenever a thread
calls a fulfillment function on a promise, the user must anticipate
that any newly available callbacks will be executed by the current
thread before the fulfillment call returns.

\pnum
The other place in which a callback will execute immediately is during the invocation
of \code{then} on a future that is already in its ready state. In this
case, the callback provided will fire immediately during the call to
\code{then}.

\pnum
There are some common programming contexts where it is not safe for a
callback to execute during fulfillment of a promise. For example, it
is generally unsafe to execute a callback that modifies a data
structure while a thread is traversing the data structure. In such
a situation, it is the user's responsibility to ensure that a
conflicting callback will not execute. One solution is create a
promise that represents a thread reaching its {\em safe-to-execute}
context, and then adding it to the dependency list of any conflicting
callback.

\begin{numlisting}
future<int> value = /* ... */;
// create a promise representing a safe-to-execute state
// dependency count is initially 1
promise<> safe_state;
// create a future that depends on both value and safe_state
future<int> combined = when_all(value, safe_state.get_future());
auto fut = // register a callback on the combined future
combined.then(/* some callback that requires a safe state */);
// do some work, potentially fulfilling value's promise...
// signify a safe state
safe_state.finalize();
// callback can now execute
\end{numlisting}

\pnum
As demonstrated above, the user can wait to fulfill the promise until
it is safe to execute the callback, which will then allow it to
execute.

\section{Fulfilling Promises}

\pnum
As demonstrated previously, promises can be used to both supply values
as well as signal completion of events that do not produce a value. As
such, a promise is a unified abstraction for tracking the completion
of asynchronous operations, whether the operations produce a value or
not. A promise represents at most one dependency that produces a
value, but it can track any number of anonymous dependencies that do
not result in a value.

\pnum
When created, a promise starts with an initial dependency count of 1.
For an empty promise (\code{promise<>}), this is necessarily an
anonymous dependency, since an empty promise does not hold a value.
For a non-empty promise, the initial count represents the sole
dependency that produces a value. Further anonymous dependencies can
be explicitly registered on a promise with the
\code{require_anonymous} member function:

\begin{numlisting}
promise<int, double> pro; // initial dependency count is 1
pro.require_anonymous(10); // dependency count is now 11
\end{numlisting}

\pnum
The argument to \code{require_anonymous} 
must be nonnegative and the promise's current dependency count must be greater than zero, so that a call to
\code{require_anonymous} never causes the dependency count to reach
zero, which would put the promise in the fulfilled state.
In the example
above, the argument must be greater than -1, and the given argument of 10
is valid.

\pnum
Anonymous dependencies can be fulfilled by calling the
\code{fulfill_anonymous} member function:

\begin{numlisting}
for (int k = 0; k < 5; i++) {
  pro.fulfill_anonymous(k);
} // dependency count is now 1
\end{numlisting}

\pnum
A non-anonymous dependency is fulfilled by calling
\code{fulfill_result} with the produced values:

\begin{numlisting}
pro.fulfill_result(3, 4.1); // dependency count is now 0
assert(pro.get_future().ready());
\end{numlisting}

\pnum
Both empty and non-empty promises can be used to track anonymous
dependencies. A \upcxx operation that operates on a promise
\emph{always} increments its dependency count upon invocation, as if
by calling \code{require_anonymous(1)} on the promise. After the
operation completes\footnote{The notification will occur during
  user-level progress of the persona that initiates the operation. See
  Ch. \ref{ch:progress} for more details.}, if the completion produces
values of type
\code{T...}, then the values are supplied to the promise through a
call to \code{fulfill_result}. Otherwise, the completion is signaled
by fulfilling an anonymous dependency through a call to
\code{fulfill_anonymous(1)}.

\pnum
The rationale for this behavior is to free the user from having to
manually increment the dependency count before calling an operation on
a promise; instead, \upcxx will implicitly perform this increment.
This leads to the pattern, shown at the beginning of this chapter, of
registering operations on a promise and then finalizing the promise to
take it out of registration mode:

\begin{numlisting}
global_ptr<int> ptrs[10] = /* some remote pointers */;
promise<> prom; // dependency count is 1

for (int i = 0; i < 10; i++) {
  rput(i, ptrs[i],
       operation_cx::as_promise(prom)); // increment count
} // dependency count is now 11

future<> fut = prom.finalize(); // decrement count, making it 10

// wait for the 10 rput operations to complete
fut.wait();
\end{numlisting}

\pnum
A user familiar with \upcxx V0.1 will observe that empty promises
subsume the capabilities of \code{event}s in \upcxx V0.1. In addition,
they can take part in all the machinery of promises, futures, and
callbacks, providing a much richer set of capabilities than were
available in V0.1.

\section{Lifetime and Thread Safety}

\pnum
Understanding the lifetime of objects in the presence of asynchronous
control flow can be tricky. Objects must outlive the last callback
that references them, which in general does not follow the scoped
lifetimes of the call stack. For this reason, \upcxx automatically
manages the state represented by futures and promises, and the state
persists for as long as there is a future, promise, or dependent
callback that references it. Thus, a user can construct intricate webs
of callbacks over futures without worrying about explicitly managing
the state representing the callbacks' dependencies or results.

\pnum
Though \upcxx does not prescribe a specific management strategy, the
semantics of futures and promises are analogous to those of standard
C++11 smart pointers. As with \code{std::shared_ptr}, a future may be
freely copied, and both the original and the copy represent the same
state and are associated with the same promise. Thus, if one copy of a
future becomes ready, then so will the other copies. On the other
hand, a promise can be mutated by the user through its member
functions, so allowing a promise to be copied would introduce the issue
of aliasing. Instead, we adopt the same non-copyable, yet movable,
semantics for a promise as \code{std::unique_ptr}.

\pnum
Given that \upcxx futures and promises are already thread-unaware to
allow the execution strategy to be straightforward and efficient,
\upcxx also makes no thread safety guarantees about
internal state management. This enables creation of
copies of a future to be a very cheap operation. For example, a
future can be captured by value by a lambda function or passed by
value without any performance penalties. On the other hand, the lack
of thread safety means that sharing a future between threads must be
handled with great caution. Even a simple operation such as making a
copy of a future, as when passing it by value to a function, is unsafe
if another thread is concurrently accessing an identical future, since
the act of copying it can modify the internal management state. Thus,
a mutex or other synchronization is required to ensure exclusive
access to a future when performing any operation on it.

\pnum
Fulfilling a promise gives rise to an even more stringent demand,
since it can set off a cascade of callback execution. Before
fulfilling a promise, the user must ensure that the thread has the
exclusive right to mutate not just the future associated with the
promise, but all other futures that are directly or indirectly
dependent on fulfillment of the promise. Thus, when crafting their
code, the user must properly manage exclusivity for {\em islands} of
disjoint futures. We say that two futures are in {\em disjoint islands} if
there is no dependency, direct or indirect, between them.

\pnum
A reader having previous experience with futures will note that
\upcxx's formulation is a significant departure from many other
software packages. Futures are commonly used to pass data between
threads, like a channel that a producing thread can supply a value
into, notifying a consuming thread of its availability. \upcxx,
however, is intended for high-performance computing, and supporting
concurrently shareable futures would require synchronization that
would significantly degrade performance. As such, futures in \upcxx
are not intended to {\em directly} facilitate communication between
threads. Rather, they are designed for a single thread to manage the
non-determinism of reacting to the events delivered by concurrently
executing agents, be they other threads or the network hardware.

%Until now, futures have been described independently of \upcxx
%operations. This is with good cause. Futures are fundamentally a lower-level
%abstraction than the communication operations of \upcxx, so \upcxx 
%strives to pitch itself as a consumer of the asynchrony primitives of
%futures. ...
\section{API Reference}
\pnum
{\em UPC++ progress level for all functions in this chapter (unless otherwise noted) is:} {\tt none}

\subsection{future}

\begin{plstlisting}
template<typename ...T>
class future;
\end{plstlisting}
\Indx{future @{\tt future}}
\begin{quote}
\concepts DefaultConstructible, CopyConstructible, CopyAssignable,
Destructible

\pnum
The types in \code{T...} must not be \code{void}.
\end{quote}

% I think we were wrong, we cannot use the future constructor to build
% ready futures as it creates ambiguity with the copy constructor:
% does constructing a future<T> with a future<T> produce a copy of that
% future or a future<future<T>>? If you said future<T>, then there's an
% insconsistency with future(future<A>, future<B>) which would deduce
% future<future<A>,future<B>> instead of future<A,B>.
%\begin{plstlisting}
%template<typename ...T>
%template<typename ...U>
%future::future(U ...results);
%\end{plstlisting}
%\begin{quote}
%Constructs a trivially ready future from the given values.
%\end{quote}

\begin{plstlisting}
template<typename ...T>
future<T...>::future();
\end{plstlisting}
\Indx{future @{\tt future}!default constructor}
\begin{quote}
\pnum
Constructs a future that will never become ready.

\permituninit
\end{quote}

\begin{plstlisting}
template<typename ...T>
future<T...>::~future();
\end{plstlisting}
\Indx{future @{\tt future}!destructor}
\begin{quote}
\pnum
Destructs this future object.

\permituninit
\end{quote}

\begin{plstlisting}
template<typename ...T>
future<T...> make_future(T ...results);
\end{plstlisting}
\Indx{make\_future@{\tt make\_future}}
\begin{quote}
\pnum
Constructs a trivially ready future from the given values.
\end{quote}

\begin{plstlisting}
template<typename ...T>
bool future<T...>::ready() const;
\end{plstlisting}
\Indx{future @{\tt future}!ready@{\tt ready}}
\begin{quote}
\pnum
Returns true if the future's result values have been supplied to it.
\end{quote}

\begin{plstlisting}
template<typename ...T>
std::tuple<T...> future<T...>::result_tuple() const;
\end{plstlisting}
\Indx{future @{\tt future}!result\_tuple@{\tt result\_tuple}}
\begin{quote}
\precondition \lstinline{this->ready()}

\pnum
Retrieves the tuple of result values for this future.
\end{quote}

\begin{plstlisting}
template<typename ...T>
template<int I=-1>
future_element_t<I, future<T...>>
  future<T...>::result() const;
\end{plstlisting}
\Indx{future @{\tt future}!result@{\tt result}}
\begin{quote}
\precondition \lstinline{this->ready()}

\pnum
If {\tt I} is in the range \code{[0, sizeof...(T))}, retrieves the
  \ith component from the future's results tuple. The return type is
  \lstinline{U}, where \lstinline{U} is the \ith component of
  \lstinline{T}.

\pnum
If {\tt I} is -1, returns the following:
\begin{itemize}
\pitem \code{void} if \code{T} is empty
\pitem if \code{T} has one element, the single component of the
  future's results tuple; the return type is \code{T}
\pitem if \code{T} has multiple elements, the tuple of result values
  for the future; the return type is \code{std::tuple<T...>}
\end{itemize}

\pnum
The return type is \lstinline{void} if {\tt I} is outside the range
\code{[-1, sizeof...(T))}.
\end{quote}

\begin{plstlisting}
template<typename ...T>
template<int I=-1>
future_element_moved_t<I, future<T...>>
  future<T...>::result_moved();
\end{plstlisting}
\Indx{future @{\tt future}!result\_moved@{\tt result\_moved}}
\begin{quote}
\precondition \lstinline{this->ready()}

\pnum
If {\tt I} is in the range \code{[0, sizeof...(T))}, retrieves the
  \ith component from the future's results tuple as an r-value
  reference. The return type is \lstinline{U&&}, where \lstinline{U}
  is the \ith component of \lstinline{T}.

\pnum
If {\tt I} is -1, returns the following:
\begin{itemize}
\pitem \code{void} if \code{T} is empty
\pitem if \code{T} has one element, the single component of the
  future's results tuple as an r-value reference; the return type is
  \code{T&&}
\pitem if \code{T} has multiple elements, the tuple of result values
  as r-value references for the future; the return type is
  \code{std::tuple<T&&...>}
\end{itemize}

\pnum
The return type is \lstinline{void} if {\tt I} is outside the range
\code{[-1, sizeof...(T))}.

\pnum
{\em Caution: this operation permits mutation of the values via
  r-value references, which could be observed by further calls that
  return the result(s) of a future.}
\end{quote}

\begin{plstlisting}
template<typename ...T>
template<typename Func>
future_invoke_result_t<Func, T...>
  future<T...>::then(Func func);
\end{plstlisting}
\Indx{future @{\tt future}!then@{\tt then}}
\begin{quote}
\precondition The call \code{func()} must not throw an
exception.

\pnum
Returns a new future representing the return value of the given
function object \code{func} when invoked on the results of this future
as its argument list. If \code{func} returns a future, then the result
of \code{then} will be a semantically equivalent future, except that
it will be in a non-ready state before \code{func} executes. If
\code{func} does not return a future, then the return value of
\code{then} is a future that encapsulates the result of \code{func},
and this future will also be in a non-ready state before \code{func}
executes. If the return type of \code{func} is \code{void}, then the
return type of \code{then} is \code{future<>}.

\pnum
The function object will be invoked in one of two situations:
\begin{pitemize}
\item{Immediately before \code{then} returns if this future is in
the ready state}.
\item{During a promise fulfillment which would directly or indirectly
make this future transition to the ready state}.
\end{pitemize}
\end{quote}

\begin{plstlisting}
template<typename ...T>
std::tuple<T...> future<T...>::wait_tuple();
\end{plstlisting}
\Indx{future @{\tt future}!wait\_tuple@{\tt wait\_tuple}}
\begin{quote}
\pnum
Blocks until the future is ready, while making \upcxx user-level
progress.
See Ch. \ref{ch:progress} for a
discussion of progress. The return value is the same as that produced
by calling \code{result_tuple()} on the future.

\restricted

\progresslevel{user}
\end{quote}

\begin{plstlisting}
template<typename ...T>
template<int I=-1>
future_element_t<I, future <T...>>
  future<T...>::wait();
\end{plstlisting}
\Indx{future @{\tt future}!wait@{\tt wait}}
\begin{quote}
\pnum
Blocks until the future is ready, while making \upcxx user-level
progress.
See Ch. \ref{ch:progress} for a
discussion of progress. The return value is the same as that produced
by calling \code{result()} on the future.

\restricted

\progresslevel{user}
\end{quote}

\begin{plstlisting}
template<typename ...T>
template<int I=-1>
future_element_moved_t<I, future <T...>>
  future<T...>::wait_moved();
\end{plstlisting}
\Indx{future @{\tt future}!wait\_moved@{\tt wait\_moved}}
\begin{quote}
\pnum
Blocks until the future is ready, while making \upcxx user-level
progress.
See Ch. \ref{ch:progress} for a
discussion of progress. The return value is the same as that produced
by calling \code{result_moved()} on the future.

\restricted

\progresslevel{user}
\end{quote}

\begin{plstlisting}
template<typename ...Futures>
future<CTypes...> when_all(Futures ...fs);
\end{plstlisting}
\Indx{when\_all@{\tt when\_all}}
\begin{quote}
\pnum
Given a variadic list of futures as arguments, constructs a future
representing the readiness of all arguments. The results tuple of this
future will be the concatenated results tuples of the arguments. The
type parameters of the returned object (\code{CTypes...}) is the
ordered concatenation of the type parameter lists of the types in
\code{Futures...}. If \code{Futures...} is empty, then the result is a
trivially ready \code{future<>}.
\end{quote}

\begin{plstlisting}
template<typename T>
future<CTypes...> to_future(T future_or_value);
\end{plstlisting}
\Indx{to\_future@{\tt to\_future}}
\begin{quote}
\pnum
Constructs a future that encapsulates the value represented by
\code{future_or_}\allowbreak\code{value}. If \code{T} is of type \code{future<U...>},
then \code{CTypes...} is the same as \code{U...}, and the returned
future is a copy of \code{future_or_value}. If \code{T} is not a
future, then \code{CTypes...} is \code{T}, and the function returns a
ready future whose encapsulated value is \code{future_or_value}.
\end{quote}

\subsection{promise}

\begin{plstlisting}
template<typename ...T>
class promise;
\end{plstlisting}
\Indx{promise@{\tt promise}}
\begin{quote}
\concepts DefaultConstructible, MoveConstructible, MoveAssignable, Destructible

\pnum
The types in \code{T...} must not be \code{void}.
\end{quote}

\begin{plstlisting}
template<typename ...T>
promise<T...>::promise(std::intptr_t dependency_count=1);
\end{plstlisting}
\Indx{promise@{\tt promise}!constructor}
\begin{quote}
\precondition \lstinline{dependency_count >= 1}

\pnum
Constructs a promise with its results uninitialized and the given
initial dependency count.

\permituninit
\end{quote}

\begin{plstlisting}
template<typename ...T>
promise<T...>::~promise();
\end{plstlisting}
\Indx{promise@{\tt promise}!destructor}
\begin{quote}
\pnum
Destructs this promise object.

\permituninit
\end{quote}

\begin{plstlisting}
template<typename ...T>
void promise<T...>::require_anonymous(std::intptr_t count);
\end{plstlisting}
\Indx{promise@{\tt promise}!require\_anonymous@{\tt require\_anonymous}}
\begin{quote}
\precondition \code{count} is nonnegative. The dependency count
of this promise is greater than 0.

Adds {\tt count} to this promise's dependency count.
\end{quote}

\begin{plstlisting}
template<typename ...T>
template<typename ...U>
void promise<T...>::fulfill_result(U &&...results);
\end{plstlisting}
\Indx{promise@{\tt promise}!fulfill\_result@{\tt fulfill\_result}}
\begin{quote}
\precondition \code{fulfill_result} has not been called on this
promise before, and the dependency count of this promise is greater than
zero.

\pnum
Initializes the promise's result tuple with the given values and
decrements the dependency counter by 1. Requires that \lstinline{T}
and \lstinline{U} have the same number of components, and that each
component of \lstinline{U} is implicitly convertible to the
corresponding component of \lstinline{T}. If the dependency counter
reaches zero as a result of this call, the associated future is set to
ready, and callbacks that are waiting on the future are executed on
the calling thread before this function returns.
\end{quote}

\begin{plstlisting}
template<typename ...T>
void promise<T...>::fulfill_anonymous(std::intptr_t count);
\end{plstlisting}
\Indx{promise@{\tt promise}!fulfill\_anonymous@{\tt fulfill\_anonymous}}
\begin{quote}
\precondition \code{count} is nonnegative. The dependency count
of this promise is greater than zero and greater than or equal to
\code{count}. If the dependency count is equal to
\code{count} and \code{T} is not empty, then the results of this
promise must have been previously supplied by a call to
\code{fulfill_result}.

\pnum
Subtracts \code{count} from the dependency counter. If this produces a
zero counter value, the associated future is set to ready, and
callbacks that are waiting on the future are executed on the calling
thread before this function returns.
\end{quote}

\begin{plstlisting}
template<typename ...T>
future<T...> promise<T...>::get_future() const;
\end{plstlisting}
\Indx{promise@{\tt promise}!get\_future@{\tt get\_future}}
\begin{quote}
\pnum
Returns the future representing this promise being fulfilled. Repeated
calls to \code{get_future} return equivalent futures with the
guarantee that no additional memory allocation is performed.
\end{quote}

\begin{plstlisting}
template<typename ...T>
future<T...> promise<T...>::finalize();
\end{plstlisting}
\Indx{promise@{\tt promise}!finalize@{\tt finalize}}
\begin{quote}
\pnum
Equivalent to calling \code{this->fulfill_anonymous(1)} and then
returning the result of \code{this->get_future()}.
\end{quote}
