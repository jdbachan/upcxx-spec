\chapter{Atomics}
\label{ch:atomics}

\section{Overview}
\pnum
\upcxx supports atomic operations on shared memory locations. Atomicity 
entails that a read-modify-write sequence on a memory location will 
happen without interference or interleaving with other concurrently 
executing atomic operations. Atomicity is not guaranteed if a memory 
location is concurrently targeted by both atomic and non-atomic 
operations. The order in which concurrent atomics update the 
same memory is not guaranteed, not even for successively issued 
operations by a single process. Ordering of atomics with respect to other 
asynchronous operations is also not guaranteed. The only means to 
ensure such ordering is by waiting for one operation to complete before 
initiating its successor. Note that \upcxx atomics do not interoperate with \code{std::atomic}. 

\pnum
At this time, it is unclear how \upcxx will support mixing of atomic
and non-atomic accesses to the same memory location. Until this is
resolved, users must assume that for the duration of the program, once
a memory location is accessed via a \upcxx atomic, only further atomic
operations to that location will have meaningful results (note that even
global barrier synchronization does not grant an exception to this rule).
This
unfortunately implies that deallocation of such memory is unsafe, as
that would allow the memory to be reallocated to a context unaware of
its constrained condition.

\pnum
All atomic operations are associated with an {\em atomic domain}. An atomic domain is defined for an
integer or floating-point type and a set of operations. Currently, the
allowed types are \code{std::int32_t},
\code{std::uint32_t}, \code{std::int64_t}, \code{std::uint64_t},
\code{float}, and \code{double}. The list of operations is detailed
in the API section below. The atomic domain is an
instance of a \code{atomic\_domain} class, and the operations are defined as methods on that
class. 

\pnum
The use of atomic domains permits selection (at construction) of the most efficient available implementation
which can provide correct results for the given set of operations on the given data type.  This is
important because the best possible implementation of a operation "X" may not be compatible with
operation "Y".  So, this best "X" can only be used when it is known that "Y" will not be used.  This
issue arises because a NIC may offload "X" (but not "Y") and use of a CPU-based implementation of
"Y" would not be coherent with the NIC performing a concurrent "X" operation.

\pnum
Similar to a mutex, an atomic domain exists independent of the data it applies
to. User code is responsible for ensuring that data accessed via a given atomic
domain is only accessed via that domain, never via a different domain or
without use of a domain.

\pnum
Users may create as many domains as needed to describe their uses of atomic
operations, so long as there is at most one domain per atomic datum. If
distinct data of the same type are accessed using differing sets of operations,
then creation of distinct domains for each operation set is recommended to
achieve the best performance on each set.

\pnum
For example, to use atomic fetch-and-add, load and store operations on an \code{int64_t}, a user must first define a
domain as follows:

\begin{plstlisting}
  atomic_domain<int64_t> ad_i64({atomic_op::load,
                                 atomic_op::store,
                                 atomic_op::fetch_add});
\end{plstlisting}

\pnum
Each atomic operation works on a {\em global pointer} to the type given when the domain was
constructed.

\pnum
All atomic operations are non-blocking and provide an operation-completion event to indicate
completion of the atomic. By default, all operations return futures. So, for example, this is the
way to call an atomic operation for the previous example's domain:

\begin{numlisting}
  global_ptr<int64_t> x = new_<int64_t>(0);  
  future<int64_t> f = ad_i64.fetch_add(x, 2,
                                       std::memory_order_relaxed);
  int64_t res = f.wait();
\end{numlisting}

\pnum
Atomic domains enable a user to select a subset of operations that are supported in hardware on a
given platform, and hence more performant. 

\section{Deviations from IEEE 754}
\pnum
\upcxx atomics on \code{float} and \code{double} are permitted to
deviate from the IEEE 754 standard~\cite{ieee-754},
even where \code{float} and
\code{double} otherwise conform to the standard in the underlying C++
implementation.
For example, a \upcxx atomic may perform a \code{compare_exchange}
operation on floating-point values as if they were integers of the
same width, and it may compare floating-point values as if they were
sign-and-magnitude-representation integers of the same width.
This can lead to non-conforming behavior with respect to NaNs and
negative zero.

\section{API Reference}

\begin{plstlisting}
enum class atomic_op : int {
  load, store,
  compare_exchange,
  add, fetch_add,
  sub, fetch_sub,
  mul, fetch_mul,
  min, fetch_min,
  max, fetch_max,
  bit_and, fetch_bit_and,
  bit_or, fetch_bit_or,
  bit_xor, fetch_bit_xor,
  inc, fetch_inc,
  dec, fetch_dec
};
\end{plstlisting}
\Indx{atomic\_op@{\tt atomic\_op}}

\begin{plstlisting}
template<typename T>
class atomic_domain;
\end{plstlisting}
\Indx{atomic\_domain@{\tt atomic\_domain}}
\begin{quote}
\concepts MoveConstructible, Destructible
\end{quote}

\begin{plstlisting}
template<typename T>
atomic_domain<T>::atomic_domain(
    std::vector<atomic_op> const &ops,
    team &team = world());
\end{plstlisting}
\Indx{atomic\_domain@{\tt atomic\_domain}!constructor}
\begin{quote}
\collective{the given team}

\precondition \code{T} must be one of the approved atomic types: \code{std::int32_t},
\code{std::uint32_t}, \code{std::int64_t}, \code{std::uint64_t},
\code{float}, or \code{double}.
\code{T} must be a permitted type for each of the operations in
\code{ops}.

\pnum
Constructs an atomic domain for type \code{T}, with supported operations
\code{ops}.

\progresslevel{internal}
\end{quote}

\begin{plstlisting}
template<typename T>
atomic_domain<T>::atomic_domain(atomic_domain &&other);
\end{plstlisting}
\Indx{atomic\_domain@{\tt atomic\_domain}!move constructor}
\begin{quote}
\precondition Calling thread must have the master persona.

\pnum
Makes this instance the calling process's representative of the atomic
domain associated with \code{other}, transferring all state from
\code{other}. Invalidates \code{other}, and any subsequent operations
on \code{other}, except for destruction, produce undefined behavior.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
template<typename T>
void atomic_domain<T>::destroy(entry_barrier lev =
                                 entry_barrier::user);
\end{plstlisting}
\Indx{atomic\_domain@{\tt atomic\_domain}!destroy@{\tt destroy}}
\begin{quote}
\collective{the team associated with this atomic domain}

\precondition This instance must not have been invalidated by being
passed to the move constructor.
\code{lev} must be single-valued (Ch. \ref{ch:collectives}).
After the entry barrier specified by \code{lev} completes, or upon
entry if \code{lev == entry_barrier::none},
all operations on this atomic domain must have signaled operation
completion.

\pnum
Destroys the calling process's state associated with the atomic
domain.

\ordering If \code{lev != entry_barrier::none}, with respect to all
threads participating in this collective, all evaluations which are
{\em sequenced-before} their respective thread's invocation of this
call will have a {\em happens-before} relationship with all
evaluations sequenced after the call.

\progresslevel{user {\rm if \code{lev == entry_barrier::user};}
  internal {\rm otherwise}}
\end{quote}

\begin{plstlisting}
template<typename T>
atomic_domain<T>::~atomic_domain();
\end{plstlisting}
\Indx{atomic\_domain@{\tt atomic\_domain}!destructor}
\begin{quote}
\precondition Either \upcxx must have been uninitialized since the
atomic domain's creation, or
the atomic domain must either have had \code{destroy()}
called on it or been invalidated by being passed to the move
constructor.

\pnum
Destructs this atomic domain object.

\permituninit

\progresslevel{none}
\end{quote}

\noindent
\begin{plstlisting}
template<typename T,
    typename Completions=decltype(operation_cx::as_future())>
RType atomic_domain<T>::load(global_ptr<T> p,
                          std::memory_order order,
                          Completions cxs=Completions{});
\end{plstlisting}
\Indx{atomic\_domain@{\tt atomic\_domain}!load@{\tt load}}
\begin{quote}

\precondition
\code{T} must
be the only type used by any atomic referencing any part of \code{p}'s
target memory for the entire lifetime of \upcxx.
\code{order} must be \memorder{relaxed} or \memorder{acquire}.
The \code{atomic_op::}\allowbreak\code{load} operation must have been included in the \code{ops} used to construct
this \code{atomic_domain}.
The team associated with this domain must not have been destroyed.

\pnum
Initiates an atomic read of the object at location \code{p} and
produces its value as part of operation completion.

\begin{completions}
\item{Operation} Indicates completion of all aspects of the
  operation: the remote atomic read and transfer of the result are
  complete. This completion produces a value of type \code{T}.
\end{completions}

\ordering If \code{order} is
\memorder{acquire} then the read performed will have a
{\em happens-before} relationship with the operation-completion
notification actions (future readying, promise fulfillment, or persona
LPC enlistment).

\progresslevel{internal}
\end{quote}

\noindent
\begin{plstlisting}
template<typename T,
    typename Completions=decltype(operation_cx::as_future())>
RType atomic_domain<T>::store(global_ptr<T> p,
                           T val,
                           std::memory_order order,
                           Completions cxs=Completions{});
\end{plstlisting}
\Indx{atomic\_domain@{\tt atomic\_domain}!store@{\tt store}}
\begin{quote}

\precondition
\code{T} must
be the only type used by any atomic referencing any part of \code{p}'s
target memory for the entire lifetime of \upcxx. 
\code{order} must be \memorder{relaxed} or \memorder{release}.
The \code{atomic_op::}\allowbreak\code{store} operation must have been included in the \code{ops} used to construct
this \code{atomic_domain}.
The team associated with this domain must not have been destroyed.

\pnum
Initiates an atomic write of \code{val} to the location \code{p}.
Completion of the write is indicated by operation completion.

\begin{completions}
\item{Operation} Indicates completion of all aspects of the
  operation: the transfer of the value and remote atomic write are
  complete.
\end{completions}

\ordering If \code{order} is
\memorder{release} then all evaluations
{\em sequenced-before} this call will have a {\em happens-before}
relationship with the write performed. The write performed will have a
{\em happens-before} relationship with the operation-completion
notification actions (future readying, promise fulfillment, or persona
LPC enlistment).

\progresslevel{internal}
\end{quote}

\noindent
\begin{plstlisting}
template<typename T,
    typename Completions=decltype(operation_cx::as_future())>
RType atomic_domain<T>::compare_exchange(
    global_ptr<T> p,
    T val1,
    T val2,
    std::memory_order order,
    Completions cxs=Completions{});
\end{plstlisting}
\Indx{atomic\_domain@{\tt atomic\_domain}!compare\_exchange@{\tt compare\_exchange}}
\begin{quote}

\precondition
\code{T} must
be the only type used by any atomic referencing any part of \code{p}'s
target memory for the entire lifetime of \upcxx. 
\code{order} must be \memorder{relaxed}, \memorder{acquire}, \memorder{release}, or \memorder{acq_rel}.
The \code{ops} used to construct this \code{atomic_domain} must have included the
\code{atomic_op::compare_exchange} operation.
The team associated with this domain must not have been destroyed.

\pnum
Initiates the atomic read-modify-write operation consisting of: reading
the value of the object located at \code{p}, and if it is equal to \code{val1},
writing \code{val2} back. The value produced by operation completion is the one
initially read.

\begin{completions}
\item{Operation} Indicates completion of all aspects of the
  operation: the transfer of the given value to the recipient, remote
  atomic update, and transfer of the old value to the initiator are
  complete. This completion produces a value of type \code{T}.
\end{completions}

\ordering If \code{order} is
either \memorder{release} or \memorder{acq_rel}
then all evaluations {\em sequenced-before} this call will have a
{\em happens-before} relationship with the atomic action.
If \code{order} is \memorder{acquire} or \memorder{acq_rel}
then the atomic action will have a
{\em happens-before} relationship with the operation-completion
notification actions (future readying, promise fulfillment, or persona
LPC enlistment).

\progresslevel{internal}
\end{quote}

\noindent
\begin{plstlisting}
template<typename T,
    typename Completions=decltype(operation_cx::as_future())>
RType atomic_domain<T>::<@\emph{binary\_key}@>(global_ptr<T> p,
                        T val,
                        std::memory_order order,
                        Completions cxs=Completions{});
template<typename T,
    typename Completions=decltype(operation_cx::as_future())>
RType atomic_domain<T>::fetch_<@\emph{binary\_key}@>(global_ptr<T> p,
                        T val,
                        std::memory_order order,
                        Completions cxs=Completions{});
template<typename T,
    typename Completions=decltype(operation_cx::as_future())>
RType atomic_domain<T>::<@\emph{unary\_key}@>(global_ptr<T> p,
                        std::memory_order order,
                        Completions cxs=Completions{});
template<typename T,
    typename Completions=decltype(operation_cx::as_future())>
RType atomic_domain<T>::fetch_<@\emph{unary\_key}@>(global_ptr<T> p,
                        std::memory_order order,
                        Completions cxs=Completions{});
\end{plstlisting}
\Indx{atomic\_domain@{\tt atomic\_domain}!add@{\tt add}}
\Indx{atomic\_domain@{\tt atomic\_domain}!fetch\_add@{\tt fetch\_add}}
\Indx{atomic\_domain@{\tt atomic\_domain}!sub@{\tt sub}}
\Indx{atomic\_domain@{\tt atomic\_domain}!fetch\_sub@{\tt fetch\_sub}}
\Indx{atomic\_domain@{\tt atomic\_domain}!mul@{\tt mul}}
\Indx{atomic\_domain@{\tt atomic\_domain}!fetch\_mul@{\tt fetch\_mul}}
\Indx{atomic\_domain@{\tt atomic\_domain}!min@{\tt min}}
\Indx{atomic\_domain@{\tt atomic\_domain}!fetch\_min@{\tt fetch\_min}}
\Indx{atomic\_domain@{\tt atomic\_domain}!max@{\tt max}}
\Indx{atomic\_domain@{\tt atomic\_domain}!fetch\_max@{\tt fetch\_max}}
\Indx{atomic\_domain@{\tt atomic\_domain}!bit\_and@{\tt bit\_and}}
\Indx{atomic\_domain@{\tt atomic\_domain}!fetch\_bit\_and@{\tt fetch\_bit\_and}}
\Indx{atomic\_domain@{\tt atomic\_domain}!bit\_or@{\tt bit\_or}}
\Indx{atomic\_domain@{\tt atomic\_domain}!fetch\_bit\_or@{\tt fetch\_bit\_or}}
\Indx{atomic\_domain@{\tt atomic\_domain}!bit\_xor@{\tt bit\_xor}}
\Indx{atomic\_domain@{\tt atomic\_domain}!fetch\_bit\_xor@{\tt fetch\_bit\_xor}}
\Indx{atomic\_domain@{\tt atomic\_domain}!inc@{\tt inc}}
\Indx{atomic\_domain@{\tt atomic\_domain}!fetch\_inc@{\tt fetch\_inc}}
\Indx{atomic\_domain@{\tt atomic\_domain}!dec@{\tt dec}}
\Indx{atomic\_domain@{\tt atomic\_domain}!fetch\_dec@{\tt fetch\_dec}}
\begin{quote}

\begin{sloppypar}
\precondition
\code{T} must
be the only type used by any atomic referencing any part of \code{p}'s
target memory for the entire lifetime of \upcxx,
and it must be one of the permitted types for the operation.
\code{order} must be \memorder{relaxed}, \memorder{acquire}, \memorder{release}, or \memorder{acq_rel}.
The \code{atomic_op::}\emph{op} operation must have been included in
the \code{ops} used to construct this \code{atomic_domain}, where
\emph{op} is the following for each variant, respectively:
\emph{binary\_key}, \code{fetch_}\emph{binary\_key},
\emph{unary\_key}, \code{fetch_}\emph{unary\_key}.
The team associated with this domain must not have been destroyed.
\end{sloppypar}

\pnum
Initiates the atomic read-modify-write operation consisting of:
reading the value of the object located at \code{p}, performing the
operation corresponding to \emph{binary\_key} or \emph{unary\_key},
and writing the new value back. For binary operations, the operation
is performed on the value initially read and the \code{val} argument.
For unary operations, the operation is performed on the value
initially read. In the fetch variants, the value initially read is
produced by operation completion.

\pnum
The correspondence between \emph{binary\_key}, its respective
arithmetic operation, and the permitted types is as in Table
\ref{tab:binary_atomics}. All operations support the integral types
\code{std::int32_t}, \code{std::uint32_t}, \code{std::int64_t}, and
\code{std::uint64_t}.

\begin{table}[h]
  \centering
  \begin{tabular}{|ccc|}
    \hline
    \textbf{\it binary\_key} & \textbf{Computation} & Supports \texttt{float} and \texttt{double} \\\hline
    \texttt{add} & \texttt{+} & yes \\
    \texttt{sub} & \texttt{-} & yes \\
    \texttt{mul} & \texttt{*} & yes \\
    \texttt{min} & \texttt{std::min} & yes \\
    \texttt{max} & \texttt{std::max} & yes \\
    \texttt{bit\_and} & \texttt{\&} & no \\
    \texttt{bit\_or} & \texttt{|} & no \\
    \texttt{bit\_xor} & \texttt{\^} & no \\
    \hline
  \end{tabular}
  \caption{Binary atomic arithmetic computations}
  \label{tab:binary_atomics}
\end{table}

\pnum
The correspondence between \emph{binary\_key}, its respective
arithmetic operation, and the permitted types is as in Table
\ref{tab:unary_atomics}. All operations support the integral types
\code{std::int32_t}, \code{std::uint32_t}, \code{std::int64_t}, and
\code{std::uint64_t}.

\begin{table}[!h]
  \centering
  \begin{tabular}{|ccc|}
    \hline
    \textbf{\it unary\_key} & \textbf{Computation} & Supports \texttt{float} and \texttt{double} \\\hline
    \texttt{inc} & \texttt{++} & yes \\
    \texttt{dec} & \texttt{-{}-} & yes \\
    \hline
  \end{tabular}
  \caption{Unary atomic arithmetic computations}
  \label{tab:unary_atomics}
\end{table}

\begin{completions}
\item{Operation} Indicates completion of all aspects of the operation:
  the transfer of the given value to the recipient and remote atomic
  update, and transfer of the old value to the initiator in the fetch
  variants, are complete. This completion does not produce a value in
  the non-fetch variants and produces a value of type \code{T} in the
  fetch variants.
\end{completions}

\ordering If \code{order} is
either \memorder{release} or \memorder{acq_rel}
then all evaluations {\em sequenced-before} this call will have a
{\em happens-before} relationship with the atomic action.
If \code{order} is \memorder{acquire} or \memorder{acq_rel}
then the atomic action will have a
{\em happens-before} relationship with the operation-completion
notification actions (future readying, promise fulfillment, or persona
LPC enlistment).

\progresslevel{internal}
\end{quote}
