\chapter{Global Pointers}
\label{ch:globalptr}

\commentout{
\noindent
\begin{plstlisting}[frame=single,style=upcxx] 
namespace upcxx {

 using intrank_t = /* implementation-defined */;

 template<typename T>
 struct global_ptr {
   typedef T element_type;
   explicit global_ptr(T *ptr);
   global_ptr(std::nullptr_t);
   ~global_ptr() = default;
   bool is_local() const;
   bool is_null() const;
   T* local() const;
   intrank_t where() const;
   global_ptr operator+(std::ptrdiff_t diff) const;
   std::ptrdiff_t operator-(global_ptr rhs) const;
   global_ptr& operator++();
   global_ptr operator++(int);
   global_ptr& operator--();
   global_ptr operator--(int);
   global_ptr& operator+=(std::ptrdiff_t diff);
   global_ptr& operator-=(std::ptrditt_t diff);
   bool operator==(global_ptr rhs) const;
   bool operator!=(global_ptr rhs) const;
   bool operator<(global_ptr rhs) const; 
   bool operator<=(global_ptr rhs) const; 
   bool operator>(global_ptr rhs) const; 
   bool operator>=(global_ptr rhs) const; 
 };

 template<typename T>
 global_ptr<T> operator+(std::ptrdiff_t diff, global_ptr<T> ptr);
 
 template<typename T, typename U>
 global_ptr<T> reinterpret_pointer_cast(global_ptr<U> ptr);

} // namespace upcxx

namespace std {

  template<typename T>
  struct less<global_ptr<T>>;
  template<typename T>
  struct less_equal<global_ptr<T>>;
  template<typename T>
  struct greater<global_ptr<T>>;
  template<typename T>
  struct greater_equal<global_ptr<T>>;
  template<typename T>
  struct hash<global_ptr<T>>;

} // namespace std
\end{plstlisting} 
}

\section{Overview}
\pnum
The \upcxx \  \globalptr is the primary way to address memory in a remote shared memory
segment of a \upcxx program.  The next chapter discusses how memory in
the shared segment is allocated to the user.

\pnum
As mentioned in Chapter \ref{ch:overview}, a global pointer is a
handle that may not be dereferenced. This restriction follows from
the design decision to prohibit implicit communication. Logically, a
global pointer has two parts: a raw C++ pointer and an associated {\em
  affinity}, which is a binding of each location in a shared segment
to a particular process (generally the process which allocated that shared object).
In cases where the use of a \globalptr executes in a process that has
direct load/store access to the memory of the \globalptr (i.e.
\islocal is {\tt true}), we may extract the raw pointer component, and
benefit from the reduced cost of employing a local reference rather
than a global one. To this end, \upcxx provides the {\tt local()}
function, which returns a raw C++ pointer. Calling {\tt local()} on a
\globalptr that references an address in a remote shared segment
results in undefined behavior.

\pnum
Global pointers have the following guarantees:
\begin{penumerate}
\item{ % do not replace below with \globalptr, the <T> matters for this statement
A \code{global\_ptr<T>} is only valid if it is the null global
pointer, it references a valid object, or it represents one element
past the end of a valid array or non-array object.
}
\item{
Two global pointers compare equal if and only if they reference the
same object, one past the end of the same array or non-array object,
or are both null.
}
\item{
Equality of global pointers corresponds to observational equality, meaning
that two global pointers which compare equal will produce equivalent
behavior when interchanged.
}
\end{penumerate}

\pnum
These facts become important given that \upcxx allows two processes
which are local to each other to map the same memory into their own
virtual address spaces but possibly with different virtual addresses.
They also ensure that a global pointer can be viewed from any process to
mean the same thing without need for translation.

\section{API Reference}

\begin{plstlisting}
using intrank_t = /* implementation-defined */;
\end{plstlisting}
\Indx{intrank\_t@{\tt intrank\_t}}

\begin{quote}
\pnum
An implementation-defined signed integer type that represents a \upcxx
rank ID.
\end{quote}

\begin{plstlisting}
template<typename T>
struct global_ptr;
\end{plstlisting}
\Indx{global\_ptr@{\tt global\_ptr}}
\begin{quote}
\concepts DefaultConstructible, TriviallyCopyable,
TriviallyDestructible, EqualityComparable, LessThanComparable,
hashable

\uconcepts DefinitelyTriviallySerializable

\pnum
\code{T} must not have any cv qualifiers:
\code{std::is_const<T>::value} and \\ \code{std::is_volatile<T>::value}
must both be false.
\end{quote}

\begin{plstlisting}
template<typename T>
struct global_ptr {
  using element_type = T;
  // ...
};
\end{plstlisting}
\Indx{global\_ptr@{\tt global\_ptr}!element\_type@{\tt element\_type}}
\begin{quote}
\pnum
Member type that is an alias for the template parameter \code{T}.
\end{quote}

\begin{plstlisting}
template<typename T>
global_ptr<T>::global_ptr(std::nullptr_t = nullptr);
\end{plstlisting}
\Indx{global\_ptr@{\tt global\_ptr}!null constructor}
\begin{quote}
\pnum
Constructs a global pointer corresponding to a null pointer.

\permituninit

\progresslevel{none}
\end{quote}

\begin{plstlisting}
template<typename T>
global_ptr<T>::~global_ptr();
\end{plstlisting}
\Indx{global\_ptr@{\tt global\_ptr}!destructor}
\begin{quote}
\pnum
Trivial destructor. Does not delete or otherwise reclaim the raw
pointer that this global pointer is referencing.

\permituninit

\progresslevel{none}
\end{quote}

\begin{plstlisting}
template<typename T>
global_ptr<T> to_global_ptr(T* ptr);
\end{plstlisting}
\Indx{to\_global\_ptr@{\tt to\_global\_ptr}}
\begin{quote}
\precondition \code{ptr} is a null pointer, or a valid pointer such
that the expression \code{*ptr} on the calling process yields an object
of type \code{T} that resides within the shared segment of a process
in the local team (\S\ref{sec:localTeam}) of the caller

\pnum
Constructs a global pointer corresponding to the given raw pointer.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
template<typename T>
global_ptr<T> try_global_ptr(T* ptr);
\end{plstlisting}
\Indx{try\_global\_ptr@{\tt try\_global\_ptr}}
\begin{quote}
\precondition \code{ptr} is a null pointer, or a valid pointer such
that the expression \code{*ptr} on the calling process yields an object
of type \code{T}

\pnum
If the object referenced by \code{*ptr} resides within the shared
segment of a process in the local team (\S\ref{sec:localTeam}) of the
caller, returns a global pointer referencing that object. Otherwise
returns a null pointer.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
template<typename T>
bool global_ptr<T>::is_local() const;
\end{plstlisting}
\Indx{global\_ptr@{\tt global\_ptr}!is\_local@{\tt is\_local}}
\begin{quote}
\pnum
Returns whether or not the calling process has load/store access to the
memory referenced by this pointer. Returns true if this is a null
pointer, regardless of the context in which this query is called.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
template<typename T>
bool global_ptr<T>::is_null() const;
\end{plstlisting}
\Indx{global\_ptr@{\tt global\_ptr}!is\_null@{\tt is\_null}}
\begin{quote}
\pnum
Returns whether or not this global pointer corresponds to the null
value, meaning that it references no memory. This query is purely a
function of the global pointer instance, it is not affected by the context
in which it is called.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
template<typename T>
T* global_ptr<T>::local() const;
\end{plstlisting}
\Indx{global\_ptr@{\tt global\_ptr}!local@{\tt local}}
\begin{quote}
\precondition \code{this->is_local()}

\pnum
Converts this global pointer into a raw pointer.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
template<typename T>
intrank_t global_ptr<T>::where() const;
\end{plstlisting}
\Indx{global\_ptr@{\tt global\_ptr}!where@{\tt where}}
\begin{quote}
\pnum
Returns the rank in team \code{world()} of the process with affinity to 
the T object pointed-to by this global pointer.
The return value for \code{where()} on a null global pointer is
an implementation-defined value. 
This query is purely a function of the global pointer instance,
it is not affected by the context in which it is called.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
template<typename T>
global_ptr<T> global_ptr<T>::operator+(std::ptrdiff_t diff) const;
template<typename T>
global_ptr<T> operator+(std::ptrdiff_t diff, global_ptr<T> ptr);
template<typename T>
global_ptr<T>& global_ptr<T>::operator+=(std::ptrdiff_t diff);
\end{plstlisting}
\Indx{global\_ptr@{\tt global\_ptr}!operator+=@{\tt operator+=}}
\Indx{global\_ptr@{\tt global\_ptr}!operator+@{\tt operator+}}
\begin{quote}
\precondition Either \code{diff == 0}, or the global pointer is
pointing to the \code{i}th element of an array of \code{N} elements,
where \code{i} may be equal to \code{N}, representing a
one-past-the-end pointer. At least one of the indices \code{i+diff} or
\code{i+diff-1} must be a valid element of the same array. A pointer
to a non-array object is treated as a pointer to an array of size 1.

\pnum
If \code{diff == 0}, returns a copy of the global pointer. Otherwise
produces a pointer that references the element that is at \code{diff}
positions greater than the current element, or a one-past-the-end
pointer if the last element of the array is at \code{diff-1} positions
greater than the current.

\pnum
\code{operator+=} modifies the \code{global\_ptr} in-place and
return a reference to itself after the operation.

\pnum
These routines are purely functions of their arguments, they are not
affected by the context in which they are called.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
template<typename T>
global_ptr<T> global_ptr<T>::operator-(std::ptrdiff_t diff) const;
template<typename T>
global_ptr<T>& global_ptr<T>::operator-=(std::ptrdiff_t diff);
\end{plstlisting}
\Indx{global\_ptr@{\tt global\_ptr}!operator-@{\tt operator-}}
\Indx{global\_ptr@{\tt global\_ptr}!operator-=@{\tt operator-=}}
\begin{quote}
\precondition Either \code{diff == 0}, or the global pointer is
pointing to the \code{i}th element of an array of \code{N} elements,
where \code{i} may be equal to \code{N}, representing a
one-past-the-end pointer. At least one of the indices \code{i-diff} or
\code{i-diff-1} must be a valid element of the same array. A pointer
to a non-array object is treated as a pointer to an array of size 1.

\pnum
If \code{diff == 0}, returns a copy of the global pointer. Otherwise
produces a pointer that references the element that is at \code{diff}
positions less than the current element, or a one-past-the-end pointer
if the last element of the array is at \code{diff+1} positions less
than the current.

\pnum
\code{operator-=} modifies the \code{global\_ptr} in-place and
return a reference to itself after the operation.

\pnum
These routines are purely a function of their arguments, they are not
affected by the context in which they are called.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
template<typename T>
std::ptrdiff_t global_ptr<T>::operator-(global_ptr<T> rhs) const;
\end{plstlisting}
\Indx{global\_ptr@{\tt global\_ptr}!operator-@{\tt operator-}}
\begin{quote}
\precondition Either \code{*this == rhs}, or this global pointer
is pointing to the \code{i}th element of an array of \code{N}
elements, and \code{rhs} is pointing at the \code{j}th element of the
same array. Either pointer may also point one past the end of the
array, so that \code{i} or \code{j} is equal to \code{N}. A pointer to
a non-array object is treated as a pointer to an array of size 1.

\pnum
If \code{*this == rhs}, results in 0. Otherwise, returns \code{i-j}.

\pnum
This routine is purely a function of its arguments, it is not
affected by the context in which it is called.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
template<typename T>
global_ptr<T>& global_ptr<T>::operator++();
\end{plstlisting}
\Indx{global\_ptr@{\tt global\_ptr}!operator++@{\tt operator++}}
\begin{quote}
\precondition the global pointer must be pointing to an element
of an array or to a non-array object

\pnum
Modifies this pointer to have the value \code{*this + 1} and returns a
reference to this pointer.

\pnum
This routine is purely a function of its instance, it is not
affected by the context in which it is called.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
template<typename T>
global_ptr<T> global_ptr<T>::operator++(int);
\end{plstlisting}
\Indx{global\_ptr@{\tt global\_ptr}!operator++@{\tt operator++}}
\begin{quote}
\precondition the global pointer must be pointing to an element
of an array or to a non-array object

\pnum
Modifies this pointer to have the value \code{*this + 1} and returns a
copy of the original pointer.

\pnum
This routine is purely a function of its instance, it is not
affected by the context in which it is called.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
template<typename T>
global_ptr<T>& global_ptr<T>::operator--();
\end{plstlisting}
\Indx{global\_ptr@{\tt global\_ptr}!operator--@{\tt operator--}}
\begin{quote}
\precondition the global pointer must either be pointing to the
\code{i}th element of an array, where \code{i >= 1}, or one element
past the end of an array or a non-array object

\pnum
Modifies this pointer to have the value \code{*this - 1} and returns a
reference to this pointer.

\pnum
This routine is purely a function of its instance, it is not
affected by the context in which it is called.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
template<typename T>
global_ptr<T> global_ptr<T>::operator--(int);
\end{plstlisting}
\Indx{global\_ptr@{\tt global\_ptr}!operator--@{\tt operator--}}
\begin{quote}
\precondition the global pointer must either be pointing to the
\code{i}th element of an array, where \code{i >= 1}, or one element
past the end of an array or a non-array object

\pnum
Modifies this pointer to have the value \code{*this - 1} and returns a
copy of the original pointer.

\pnum
This routine is purely a function of its instance, it is not
affected by the context in which it is called.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
template<typename T>
bool global_ptr<T>::operator==(global_ptr<T> rhs) const;
template<typename T>
bool global_ptr<T>::operator!=(global_ptr<T> rhs) const;
template<typename T>
bool global_ptr<T>::operator<(global_ptr<T> rhs) const;
template<typename T>
bool global_ptr<T>::operator<=(global_ptr<T> rhs) const;
template<typename T>
bool global_ptr<T>::operator>(global_ptr<T> rhs) const;
template<typename T>
bool global_ptr<T>::operator>=(global_ptr<T> rhs) const;
\end{plstlisting}
\Indx{global\_ptr@{\tt global\_ptr}!comparison operators}
\begin{quote}
\pnum
Returns the result of comparing two global pointers. Two global
pointers compare equal if they both represent null pointers, or if
they represent the same memory address with affinity to the same process. All other
global pointers compare unequal.

\pnum
A pointer to a non-array object is treated as a pointer to an array of
size one. If two global pointers point to different elements of the
same array, or to subobjects of two different elements of the same
array, then the pointer to the element at the higher index compares
greater than the pointer to the element at the lower index. If one
pointer points to an element of an array or to a subobject of an
element of an array, and the other pointer points one past the end of
the array, then the latter compares greater than the former.

\pnum
If global pointers \code{p} and \code{q} compare equal, then \code{p
  == q}, \code{p <= q}, and \code{p >= q} all result in true while
\code{p != q}, \code{p < q}, and \code{p > q} all result in false. If
\code{p} and \code{q} do not compare equal, then \code{p != q} is true
while \code{p == q} is false.

\pnum
If \code{p} compares greater than \code{q}, then \code{p > q}, \code{p
  >= q}, \code{q < p}, and \code{q <= p} all result in true while
\code{p < q}, \code{p <= q}, \code{q > p}, and \code{q >= p} all
result in false.

\pnum
All other comparisons result in an unspecified value.

\pnum
These routines are purely functions of their arguments, they are not
affected by the context in which they are called.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
namespace std {
  template<typename T>
  struct less<global_ptr<T>>;
  template<typename T>
  struct less_equal<global_ptr<T>>;
  template<typename T>
  struct greater<global_ptr<T>>;
  template<typename T>
  struct greater_equal<global_ptr<T>>;
  template<typename T>
  struct hash<global_ptr<T>>;
}
\end{plstlisting}
\Indx{global\_ptr@{\tt global\_ptr}!comparison operators (STL specializations)}
\begin{quote}
\begin{sloppypar}
\pnum
Specializations of STL function objects for performing comparisons and
computing hash values on global pointers. The specializations of
\code{std::less}, \code{std::less_equal}, \code{std::greater}, and
\code{std::greater_equal} all produce a strict total order over global
pointers, even if the comparison operators do not. This strict total
order is consistent with the partial order defined by the comparison
operators.
\end{sloppypar}

\progresslevel{none}
\end{quote}

\begin{plstlisting}
template<typename T>
std::ostream& operator<<(std::ostream &os, global_ptr<T> ptr);
\end{plstlisting}
\Indx{global\_ptr@{\tt global\_ptr}!operator<<@{\tt operator<<}}
\begin{quote}
\pnum
Inserts an implementation-defined character representation of
\code{ptr} into the output stream \code{os}. The textual representation of
two objects of type \code{global_ptr<T>} is identical if and only if
the two global pointers compare equal.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
template<typename T, typename U>
global_ptr<T> static_pointer_cast(global_ptr<U> ptr);
template<typename T, typename U>
global_ptr<T> reinterpret_pointer_cast(global_ptr<U> ptr);
\end{plstlisting}
\Indx{global\_ptr@{\tt global\_ptr}!static\_pointer\_cast@{\tt static\_pointer\_cast}}
\Indx{global\_ptr@{\tt global\_ptr}!reinterpret\_pointer\_cast@{\tt reinterpret\_pointer\_cast}}
\begin{quote}
\precondition The expression \code{static_cast<T*>((U*)nullptr)} must
be well formed for the first variant, and
\code{reinterpret_cast<T*>((U*)nullptr)} must be well formed for the
second variant.

\pnum
Constructs a global pointer whose underlying raw pointer is obtained
by using a cast expression on that of \code{ptr}. The affinity of the
result is the same as that of \code{ptr}.

\pnum
If \code{rp} is the raw pointer of \code{ptr}, then the raw pointer of
the result is constructed by \code{static_cast<T*>(rp)} for the first
variant and \code{reinterpret_cast<T*>(rp)} for the second.

\progresslevel{none}
\end{quote}
