\chapter{Progress}
\label{ch:progress}
\commentout{
\noindent
\begin{plstlisting}[frame=single,style=upcxx] 
namespace upcxx {
 enum class progress_level {
   /*none, -- not an actual member, conceptual only*/
   internal,
   user
 };

 void progress(progress_level lev = progress_level::user);

 bool progress_required();

 void discharge();
} // namespace upcxx
\end{plstlisting}
% //Thread personas
% struct /*upcxx::*/persona_scope {
%   // Make `p` the new current persona for this OS thread.
%   persona_scope(persona &p);
%   
%   // Acquire `lock`, then make `p` the new current persona for
%   // this OS thread.
%   template<typename Lock>
%   persona_scope(Lock &lock, persona &p);
%
%   // Pop `p` from persona stack, release `lock` if any.
%   // Calling thread must be same for constructor and destructor.
%   ~persona_scope();
% };
%} // namespace upcxx
%\end{plstlisting}
}

\section{Overview}

%The use of internal operating system threads within the \upcxx runtime
%has been ruled out by design.
%\upcxx has been designed with a policy against the use of internal 
%operating system threads. 
\pnum
UPC++ presents a highly-asynchronous interface, but guarantees that user-provided
callbacks will only ever run on user threads during calls to the library.
This guarantees a good user-visibility of
the resource requirements of \upcxx, while providing a better interoperability 
with other software packages which may have restrictive threading requirements.
However, such a design choice requires the application developer to be conscientious
about providing \upcxx access to CPU cycles.

\pnum
Progress in \upcxx refers to how the calling application allows the 
\upcxx internal runtime to advance the state of its outstanding asynchronous 
operations. Any asynchronous operation initiated by the user may 
require the application to give \upcxx access to the execution thread 
periodically until the operation reports its completion. Such access is 
granted by simply making calls into \upcxx. Each \upcxx function's contract
to the user contains its {\em progress guarantee} level. This is described by the
members of the \code{upcxx::progress_level} enumerated type:
\Indx{progress\_level@{\tt progress\_level}}

%As part of each \upcxx 
%function's contract to the user is its {\em progress guarantee} level 
%(members of the \lstinline{upcxx::progress_level} enumerated type):

\begin{pdescription}

\item[{\tt progress\_level::user}] \upcxx may advance its internal state as 
well as signal completion of user-initiated operations. This may entail 
the firing of remotely injected procedure calls (\RPCs), or 
readying/fulfillment of futures/promises and the ensuing callback cascade.
\Indx{progress\_level@{\tt progress\_level}!progress\_level::user@{\tt progress\_level::user}}
\Indx{progress\_level@{\tt progress\_level}}

\item[{\tt progress\_level::internal}] \upcxx may advance its internal state, 
but no notifications will be delivered to the application. Thus, an
application has very limited ways to ``observe" the effects of such 
progress.
\Indx{progress\_level@{\tt progress\_level}!progress\_level::internal@{\tt progress\_level::internal}}

\item[{\it Progress level: none}] \upcxx will not attempt to advance the 
progress of asynchronous operations. (Note this level does not have an explicit
entry in the \code{progress_level} enumerated type).
\Indx{progress\_level@{\tt progress\_level}!progress\_level::none@{\tt progress\_level::none}}

\end{pdescription}

\pnum
The most common progress guarantee made by \upcxx functions is
\code{progress\_level::}\allowbreak\code{internal}. This ensures the delivery of notifications
to remote processes (or other threads) making {\em user}-level progress
in a timely manner.
%will receive application modifying events if they are making 
%{\em user}-level progress in a timely manner. 
In order to avoid having the user contend with the cost associated with
callbacks and \RPCs being run anytime a \upcxx function is entered,
\code{progress\_level::user} is purposefully not the common case.

%{\em progress\_level::user} is purposefully not the common case so the user 
%is freed from having to contend with callbacks and \RPCs running anytime 
%\upcxx is entered.

\pnum
\code{progress} is the notable function enabling the application 
to make {\em user}-level progress. Its sole purpose is to look for 
ready operations involving this process or thread and run the 
associated \RPC/callback code.

\begin{plstlisting}
upcxx::progress(progress_level lev = progress_level::user)
\end{plstlisting} 
\Indx{progress@{\tt progress}}

\pnum
%It becomes idiomatic then that 
\upcxx execution phases which leverage 
asynchrony heavily tend to follow a particular program structure. 
First, initial communications are launched. Their completion callbacks 
might then perform a mixture of compute or further \upcxx communication 
with similar, cascading completion callbacks. 
Then, the application spins on \code{upcxx::progress()},
checking some designated application state which monitors the amount of 
pending outgoing/incoming/local work to be done. 
For the user, understanding 
which functions perform these progress spins becomes crucial, since any 
invocation of user-level progress may execute \RPCs or 
callbacks.
\commentout{
Hiding a progress spin within a subprocedure could be 
disastrous if the calling procedure is not prepared to have callbacks 
executing in its context. % What must it be prepared to do?
We do not explore this issue deeper as it is 
a matter of good software engineering and outside the purview of this 
document.
}

\section{Restricted Context}
\label{sec:restricted_context}

\pnum
During user-level progress made by \upcxx, callbacks may be executed.
Such callbacks are subject to restrictions on how they may further invoke 
\upcxx themselves.
%Callbacks that execute in the course of \upcxx making user-level 
%progress are subject to restrictions on how they may further invoke 
%\upcxx themselves. 
We designate such restricted execution of callbacks
as being in the {\em restricted 
context}. The general restriction is stated as:

\pnumlst
\begin{quote}
\em User code running in the restricted context must assume that for 
the duration of the context all other attempts at making user-level 
progress, from any thread on any process, may result in a no-op every time.
\end{quote}

\pnum
The immediate implication is that a thread which is already in the 
restricted context should assume no-op behavior from further 
attempts at making progress. This makes it pointless to try and wait 
for \upcxx notifications from within restricted context since there is no viable 
mechanism to make the notifications visible to the user. Thus, calling any 
routine which spins on user-level progress until some notification occurs will 
likely hang the thread.

\section{Attentiveness}

\pnum
Many \upcxx operations have a mechanism to signal completion to the application.
However, a performance-oriented application will need to be aware of an additional
asynchronous operation status indicator called {\em progress-required}.
%But besides an operation's status of 
%being completed, there is another fundamental status indicator of 
%asynchronous operations we call {\em progress-required} which a well 
%performing application will need to be aware of. 
This status indicates that for a particular operation further advancements of the current
process or thread's {\em internal}-level progress are necessary so that completion
regarding remote entities (e.g. notification of delivery) can be reached.
%
%When a particular operation requires progress, it means that further advancements of the local process 
%or thread's {\em internal}-progress will be necessary before completion 
%regarding other parties can be reached remotely (e.g. notification of 
%delivery).
Once an operation has left the progress-required 
state, \upcxx guarantees that remote entities will see 
their side of the operations' completion without any further progress by the 
current compute resource. Applications will need to leverage this 
information for performance, as it is inadvisable for a compute resource 
to become inattentive to \upcxx progress (e.g. long bouts of 
arithmetic-heavy computation) while other entities depend on operations that 
require further servicing. 

\commentout{
Deadlock issues could also arise if an 
operation requires progress but the thread responsible goes to sleep on 
a condition variable or mutex which can only be met if other threads 
receive the event's completion.
}

\pnum
As said previously, nearly all \upcxx operations track their completion 
individually. However, it is not possible for the programmer to query \upcxx
if individual operations no longer require further progress.
% of the of the transition out of 
% requiring further progress is not something that \upcxx provides on a 
% per-operation basis.
Instead, the user may ask \upcxx when all operations initiated by this process
have reached a state at which they no longer require progress. 
This is achieved by using the following functions:

\begin{plstlisting}
bool upcxx::progress_required();
void upcxx::discharge();
\end{plstlisting}
\Indx{progress\_required@{\tt progress\_required}}

\pnum
The \code{progress_required} function reports whether this process
requires progress, allowing the application to know that there are 
still pending operations that will not achieve remote completion without
further advancements to internal progress.
This is of particular importance before an application enters a lapse of
inattentiveness (for instance, performing expensive computations) in order
to prevent slowing down remote entities.

\pnum
The \code{discharge} function allows an application to ensure that \upcxx does
not require progress anymore. It is equivalent to the following: 

\begin{plstlisting}
void upcxx::discharge() {
  while(upcxx::progress_required())
    upcxx::progress(upcxx::progress_level::internal);
}
\end{plstlisting}
\Indx{discharge@{\tt discharge}}

\pnum
A well-behaved \upcxx application is encouraged to call \code{discharge}
before any long lapse of attentiveness to progress.

\section{Thread Personas/Notification Affinity}
\label{sec:personas}

\pnum
As explained in Chapter \ref{ch:futures} {\em Futures and Promises}, 
futures require careful consideration when used in the presence of 
thread concurrency. It is crucial that \upcxx is very explicit about how a 
multi-threaded application can safely use futures returned by \upcxx calls.

\pnum
The most important thing an application has to be aware of is which thread 
\upcxx will use to signal completion of a given future.
%The biggest concern the application should have is 
%this: what thread will \upcxx use to signal the future it has given me? 
%The answer is simple: 
It is therefore extremely important to know that \upcxx will use the
same thread to which the future was returned by the \upcxx operation 
(i.e. the thread which invoked the operation in the first place).
This means that the thread which invoked a future-returning 
operation will be the only one able to see that operation's completion.
As \upcxx triggers futures only during a call which makes 
user-level progress, the invoking thread must continue 
to make such progress calls until the future is satisfied. This 
requirement has the drawback of banning the application from doing the 
following: initiating a future-returning operation on one thread, 
allowing that thread to terminate or become permanently inattentive 
(e.g. sleeping in a thread pool), and expecting a different thread to 
receive the future's completion. This section will focus on two ways 
the application can still attain this use-case.

\pnum
The notion of ``thread" has been used in a loose fashion throughout this 
document, the natural interpretation being an operating system (OS) thread.
More precisely, this document uses the notion of ``thread" to denote a \upcxx device referred to as 
{\em thread persona} which generalizes the notion of operating system threads.

\pnum
A \upcxx thread persona is a collection of \upcxx-internal state usually 
attributed to a single thread. By making it a proper construct, \upcxx allows
a single OS thread to switch between multiple application-defined roles for
processing notifications. Personas act as the receivers for notifications generated by
the \upcxx runtime.

\pnum
Values of type \code{upcxx::persona} are non-copyable, 
non-moveable objects which the application can instantiate as desired. 
For each OS thread, \upcxx internally maintains a stack of {\em active} 
persona references. The top of this stack is the {\em current} persona. 
All asynchronous \upcxx operations will have their notification events 
(signaling of futures or promises) sent to the current persona of the 
OS thread invoking the operation. Calls that make user-level 
progress will process notifications destined to any of the active 
personas of the invoking thread. The initial state of the persona stack 
consists of a single entry pointing to a persona created by \upcxx 
which is dedicated to the current OS thread. Therefore, if the application 
never makes any use of the persona API, notifications will be processed 
solely by the OS thread that initiates the operation.

\commentout{
\begin{plstlisting}
namespace upcxx {

  struct persona_scope {
    // Make `p` the new current persona for this OS thread.
    persona_scope(persona &p);
    
    // Acquire `lock`, then make `p` the new current persona for
    // this OS thread.
    template<typename Lock>
    persona_scope(Lock &lock, persona &p);
    
    // Pop `p` from persona stack, release `lock` if any.
    // Calling thread must be same for constructor and destructor.
    ~persona_scope();
  };
  
  persona_scope& top_persona_scope();

  persona_scope& default_persona_scope();
  
  bool progress_required(persona_scope &ps = top_persona_scope());
  
  void discharge(persona_scope &ps = top_persona_scope());
  
} // namespace upcxx
\end{plstlisting}
\Indx{persona\_scope@{\tt persona\_scope}}
}

\pnum
Pushing and popping personas from the persona stack (hence changing the
current persona) is done with the \code{upcxx::persona_scope} type.
For example:

\begin{numlisting}
persona scheduler_persona;
std::mutex scheduler_lock;

{ // Scope block delimits domain of persona_scope instance.
  auto scope = persona_scope(scheduler_lock, scheduler_persona);
  
  // All following upcxx actions will use `scheduler_persona`
  // as current.
  
  // ...
  
  // `scope` destructs:
  // - `scheduler_persona` dropped from active set if it
  //   wasn't active before the scope's construction.
  // - Previously current persona revived.
  // - Lock released.
}
\end{numlisting}

\pnum
Since \upcxx will assume an OS thread has exclusive access to all of 
its active personas, it is the user's responsibility to ensure that no
OS threads share an active persona concurrently. The use of the 
\code{persona_scope} constructor, which takes a lock-like 
synchronization primitive, is strongly encouraged to facilitate
in enforcing this invariant.

\pnum
There are two ways that asynchronous operations can be initiated by a given
OS thread but retired in another. The first solution is simple:

\begin{penumerate}
\item{The user defines a persona \code{P}.}

\item{Thread 1 activates \code{P}, initiates the asynchronous operation,
and releases \code{P}.}

\item{Thread 1 synchronizes with Thread 2, indicating the operation has
been initiated.}

\item{Thread 2 activates \code{P}, spins on \code{progress} until the
operation completes.}
\end{penumerate}

\pnum
Care must be taken that any futures created by phase 2 are never 
altered (uttered) concurrently. The same synchronization that was used 
to enforce exclusivity of persona acquisition can be leveraged to 
protect the future as well.

\pnum
While this technique achieves our goal of different threads initiating 
and resolving asynchronous operations, it fails a different but also 
desirable property. It is often desirable to allow multiple threads to issue 
communication {\em concurrently} while delegating a separate thread to 
handle the notifications. To achieve this, it is clear that multiple 
personas are needed. Indeed, the exclusivity of a persona being current to only 
one OS thread prevents the application from concurrent initiation of communication.

\pnum
In order to issue operations and concurrently retire them in
a different thread, the user is strongly encouraged
to use the LPC completion mechanism 
described in Chapter~\ref{ch:completion}, as opposed to the future or promise variants. 
An example of such a call is:

\begin{plstlisting}
rget(gptr_src, operation_cx::as_lpc(some_persona, callback_func));
\end{plstlisting}

\pnum
In addition to the arguments necessary for the particular operation, the \code{as\_lpc}
completion mechanism
takes a persona reference and a {\tt C++} function object (lambda, etc.)
such that upon completion of the operation, the designated persona shall 
execute the function object during its user-level progress. 
Using this mechanism, it is simple to have multiple threads initiating 
communication concurrently with a designated thread receiving the completion notifications.
To achieve this, each operation is initiated by a thread using the agreed-upon persona of
the receiver thread together with a callback that will incorporate knowledge of completion
into the receiver's state.

\section{API Reference}

\begin{plstlisting}
enum class progress_level {
  /*none, -- not an actual member, conceptual only*/
  internal,
  user
};
\end{plstlisting}
\Indx{progress\_level@{\tt progress\_level}}

\begin{plstlisting}
void progress(progress_level lev = progress_level::user);
\end{plstlisting}
\Indx{progress@{\tt progress}}
\begin{quote}
\pnum
This call will always attempt to advance internal progress.

\pnum
If \code{lev == progress_level::user} then this thread is also used
to execute any available user actions for the personas currently active.
Actions include:
\begin{penumerate}
\item{Either future-readying or promise-fulfilling completion notifications for
asynchronous operations initiated by one of the active personas. By the execution
model of futures and promises this can induce callback cascade.}
\item{Continuation-style completion notifications from operations initiated by any
persona but designating one of the active personas as the completion recipient.}
\item{\RPCs destined for this process but only if the master
persona is among the active set.}
\item{{\tt lpc}'s destined for any of the active personas.}
\end{penumerate}

\progresslevel{internal {\rm or} user}
\end{quote}

%\begin{plstlisting}
%template<typename ...T>
%future_element_t<0, future <T...>> wait(future<T...> f);
%\end{plstlisting}
%\begin{quote}
%Waits for the given future by repeatedly attempting \upcxx user-level
%progress and testing for readiness. The return value is the same as
%that produced by \code{f.result()}.
%\end{quote}


\subsection{persona}
\label{sec:persona}

\begin{plstlisting}
class persona;
\end{plstlisting}
\Indx{persona@{\tt persona}}
\begin{quote}
\concepts DefaultConstructible, Destructible
\end{quote}

\begin{plstlisting}
persona::persona();
\end{plstlisting}
\Indx{persona@{\tt persona}!default constructor}
\begin{quote}
\pnum
Constructs a persona object with no enqueued operations.

\permituninit

\progresslevel{none}
\end{quote}

\begin{plstlisting}
persona::~persona();
\end{plstlisting}
\Indx{persona@{\tt persona}!destructor}
\begin{quote}
\pnum
Destructs this persona object.
If this persona is a member of any
thread's persona stack, the result of this call is undefined. If any
operations are currently enqueued on this persona, or if any
operations initiated by this persona require further progress, the
result of this call is undefined.

\permituninit

\progresslevel{none}
\end{quote}

\begin{plstlisting}
template<typename Func>
void persona::lpc_ff(Func func);
\end{plstlisting}
\Indx{persona@{\tt persona}!lpc\_ff@{\tt lpc\_ff}}
\begin{quote}
\precondition \code{Func} must be a function-object type that
can be invoked on zero arguments, and the call \code{func()} must not
throw an exception.

\pnum
\code{std::move}'s \code{func} into an unordered collection of 
type-erased function objects to be executed during user-level progress 
of the targeted (this) persona. This function is thread-safe, so it
may be called from any thread to enqueue work for this persona.

\pnum
The execution of \code{func} is never performed synchronously, even if
the target persona is a member of the caller's persona stack and this
function is invoked during user-level progress.

\ordering All evaluations {\em sequenced-before} this
call will have a {\em happens-before} relationship with the invocation
of \code{func}.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
template<typename Func>
future_invoke_result_t<Func> persona::lpc(Func func);
\end{plstlisting}
\Indx{persona@{\tt persona}!lpc@{\tt lpc}}
\begin{quote}
\precondition \code{Func} must be a function-object type that
can be invoked on zero arguments, and the call \code{func()} must not
throw an exception.

\pnum
\code{std::move}'s \code{func} into an unordered collection of 
type-erased function objects to be executed during user-level progress 
of the targeted (this) persona. The return value of \code{func} is 
asynchronously returned to the currently active persona in a future. If 
the return value of \code{func} is a future, then the targeted persona 
will wait for that future before signaling the future returned by 
\code{lpc} with its value. This function is thread-safe, so
it may be called from any thread to enqueue work for this
persona. Note that the future returned by \code{lpc} is considered to 
be owned by the currently active persona, the future returned by 
\code{func} (if any) will be considered owned by the target (this) 
persona.

\pnum
The execution of \code{func} is never performed synchronously, even if
the target persona is a member of the caller's persona stack and this
function is invoked during user-level progress.

\ordering All evaluations {\em sequenced-before} this
call will have a {\em happens-before} relationship with the invocation
of \code{func}, and the invocation of \code{func} will have a
{\em happens-before} relationship with evaluations sequenced after the
signaling of the final future.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
persona& master_persona();
\end{plstlisting}
\Indx{master\_persona@{\tt master\_persona}}
\begin{quote}
\pnum
Returns a reference to the master persona automatically instantiated
by the \upcxx runtime. The thread that executes \code{upcxx::init}
implicitly acquires this persona as its current persona. The master
persona is special in that it is the only one which will execute
\RPCs destined for this process. Additionally, some \upcxx functions
may only be called by a thread with the master persona in its active stack.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
persona& current_persona();
\end{plstlisting}
\Indx{current\_persona@{\tt current\_persona}}
\begin{quote}
\pnum
Returns a reference to the persona on the top of the thread's active persona stack.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
persona& default_persona();
\end{plstlisting}
\Indx{default\_persona@{\tt default\_persona}}
\begin{quote}
\pnum
Returns a reference to the persona instantiated automatically and uniquely for
this OS thread. The default persona is always the bottom of and can
never be removed from its designated OS thread's active stack.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
void liberate_master_persona()
\end{plstlisting}
\Indx{liberate\_master\_persona@{\tt liberate\_master\_persona}}
\begin{quote}
\precondition This thread must be the one which called
\code{upcxx::init}, it must have not altered its persona stack since
calling \code{init}, and it must not have called this function already
since calling \code{init}.

\pnum
The thread which invokes \code{upcxx::init} implicitly has the master 
persona at the top of its active stack, yet the user has no 
\code{persona_scope} to drop to allow other threads to acquire the 
persona. Thus, if the user intends for other threads to acquire the 
master persona, they should have the init-calling thread release the 
persona with this function so that it can be claimed by 
\code{persona_scope}'s. Generally, if this function is ever called, it 
is done soon after \code{init} and then the master persona should be
reacquired by a \code{persona_scope}.

\progresslevel{none}
\end{quote}

\subsection{persona\_scope}

\begin{plstlisting}
class persona_scope;
\end{plstlisting}
\Indx{persona\_scope@{\tt persona\_scope}}
\begin{quote}
\concepts Destructible, MoveConstructible
\end{quote}
\Indx{persona\_scope@{\tt persona\_scope}!constructor}

\begin{plstlisting}
persona_scope::persona_scope(persona &p);
\end{plstlisting}
\Indx{persona\_scope@{\tt persona\_scope}!constructor}
\begin{quote}
\precondition Excluding this thread, \code{p} is not a member of any
other thread's active stack.

\pnum
Pushes \code{p} onto the top of the calling OS thread's active persona stack.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
template<typename Mutex>
persona_scope::persona_scope(Mutex &mutex, persona &p);
\end{plstlisting}
\Indx{persona\_scope@{\tt persona\_scope}!constructor (with mutex)}
\begin{quote}
\pnum
{\em C++ Concepts of \code{Mutex}}: Mutex

\precondition \code{p} will only be a member of some thread's active
stack if that thread holds \code{mutex} in a locked state.

\pnum
Invokes \code{mutex.lock()}, then pushes \code{p} onto the OS thread's
active persona stack.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
persona_scope::~persona_scope();
\end{plstlisting}
\Indx{persona\_scope@{\tt persona\_scope}!destructor}
\begin{quote}
\precondition All \code{persona_scope}'s constructed on this thread
since the construction of this instance have since destructed.

\pnum
The persona supplied to this instance's constructor is popped from this
thread's active stack. If this instance was constructed with the mutex
constructor, then that mutex is unlocked.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
persona_scope& top_persona_scope();
\end{plstlisting}
\Indx{top\_persona\_scope@{\tt top\_persona\_scope}}
\begin{quote}
\pnum
Reference to the most recently constructed but not destructed
\code{persona_scope} for this thread. Every thread begins with an
implicitly instantiated scope pointing to its default persona that
survives for the duration of the thread's lifetime.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
persona_scope& default_persona_scope();
\end{plstlisting}
\Indx{default\_persona\_scope@{\tt default\_persona\_scope}}
\begin{quote}
\pnum
Every thread begins with an implicitly instantiated scope pointing to its
default persona that survives for the duration of the thread's lifetime.
This function returns a reference to that bottommost \code{persona\_scope} for the
calling thread, which points at the calling thread's \code{default\_persona()}.

\progresslevel{none}
\end{quote}

\subsection{Outgoing Progress}

\label{api:progress_required}
\begin{plstlisting}
bool progress_required(persona_scope &ps = top_persona_scope());
\end{plstlisting}
\Indx{progress\_required@{\tt progress\_required}}
\begin{quote}
\precondition \code{ps} has been constructed by this thread.

\pnum
For the set of personas included in this thread's active stack section 
bounded inclusively between \code{ps} and the current top, {\em nearly}
answers if any \upcxx operations initiated by those personas require
further advancement of internal-progress of their respective personas
before their completion events will be eventually available to
user-level progress on the destined processes. The exact meaning of the return
value depends on which personas are selected by \code{ps}:

\begin{pitemize}
% jb: This allows us to funnel things from other threads to the master
% allowing the thread to be shutdown.
\item{If \code{ps} {\em does not} include the master persona:} 
A return value of \code{true} means that one or more of the personas
indicated by \code{ps} requires further internal-progress to achieve completion 
of its outgoing operations. A value of \code{false} means that none of the 
personas indicated by \code{ps} require internal-progress, but internal-progress
of the master persona might still be required.

% jb: This implies the user can never be indefinitely inattentive to
% the master.
\item{If \code{ps} {\em does} include the master persona:} 
A return value of \code{true} means that one or more of the personas
indicated by \code{ps} requires further internal-progress to achieve completion
of its outgoing operations.
A return value of \code{false} means that none of the non-master personas
indicated by \code{ps} requires further internal-progress, but the
master persona may or may not require further internal-progress.

\end{pitemize}

\progresslevel{none}
\end{quote}

\begin{plstlisting}
void discharge(persona_scope &ps = top_persona_scope());
\end{plstlisting}
\Indx{discharge@{\tt discharge}}
\begin{quote}
\pnum
Advances internal-progress enough to ensure that
\code{progress_required(ps)} returns \code{false}.

\progresslevel{internal}
\end{quote}

