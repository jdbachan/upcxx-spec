\chapter{One-Sided Communication}
\label{ch:rputrget}

\section{Overview}
\pnum
The main one-sided communication functions for \upcxx are \rput and
\rget. Where possible, the underlying transport layer will use RDMA
techniques to provide the lowest-latency transport possible. The type
{\tt T} used by \rput or \rget
needs to be {\bf DefinitelyTriviallySerializable},
as described in Chapter \ref{ch:serialization} ({\em Serialization}).

\section{API Reference}

\subsection{Remote Puts}

\begin{plstlisting}
template<typename T,
         typename Completions=decltype(operation_cx::as_future())>
RType rput(T value, global_ptr<T> dest,
           Completions cxs=Completions{});
\end{plstlisting}
\Indx{rput @{\tt rput}}
\begin{quote}
\precondition \code{T} must be DefinitelyTriviallySerializable.

\pnum
Initiates a transfer of \code{value} that will store it in the memory
referenced by \code{dest}.

\begin{completions}
\item{Remote} Indicates completion of the transfer
  of \code{value}.
\item{Operation} Indicates completion of all aspects of the
  operation: the transfer and remote stores are complete.
\end{completions}

\ordering The writes to \code{dest} will have a {\em
  happens-before} relationship with the operation-completion
notification actions (future readying, promise fulfillment, or persona
LPC enlistment) and remote-completion actions (RPC enlistment). For
LPC and RPC completions, all
evaluations {\em sequenced-before} this call will have a {\em
  happens-before} relationship with the execution of the completion
function.

\progresslevel{internal}
\end{quote}

\begin{plstlisting}
template<typename T,
         typename Completions=decltype(operation_cx::as_future())>
RType rput(T const *src, global_ptr<T> dest, std::size_t count,
           Completions cxs=Completions{});
\end{plstlisting}
\Indx{rput @{\tt rput}!bulk {\tt rput}}
\begin{quote}
\begin{sloppypar}
\precondition \code{T} must be DefinitelyTriviallySerializable.
The addresses in \code{[src,src+count)} and \code{[dest,dest+count)}
must not overlap.
\end{sloppypar}

\pnum
Initiates an operation to transfer and store
the \code{count} items of type \code{T} beginning
at \code{src} to the memory beginning at \code{dest}. The values
referenced in the \code{[src,src+count)} interval must not be modified
until either source or operation completion is indicated.

\begin{completions}
\item{Source} Indicates completion of injection or internal
  buffering of the
  source values, signifying that the \code{src} buffer may be
  modified.
\item{Remote} Indicates completion of the transfer of the
  values, implying readiness of the target
  buffer \code{[dest,dest+count)}.
\item{Operation} Indicates completion of all aspects of the
  operation: the transfer and remote stores are complete.
\end{completions}

\ordering The reads of \code{src} will have a {\em
  happens-before} relationship with the source-completion notification
actions (future readying, promise fulfillment, or persona LPC
enlistment). The writes to \code{dest} will have a {\em
  happens-before} relationship with the operation-completion
notification actions (future readying, promise fulfillment, or persona
LPC enlistment) and remote-comple\-tion actions (RPC enlistment). For
LPC and RPC completions, all
evaluations {\em sequenced-before} this call will have a {\em
  happens-before} relationship with the execution of the completion
function.

\progresslevel{internal}
\end{quote}


\subsection{Remote Gets}

\begin{plstlisting}
template<typename T,
         typename Completions=decltype(operation_cx::as_future())>
RType rget(global_ptr<T> src,
           Completions cxs=Completions{});
\end{plstlisting}
\Indx{rget @{\tt rget}}
\begin{quote}
\precondition \code{T} must be DefinitelyTriviallySerializable.

\pnum
Initiates a transfer to this process of a single value of type \code{T}
located at \code{src}. The value will be transferred to the calling
process and delivered in
the operation-completion notification.

\begin{completions}
\item{Operation} Indicates completion of all aspects of the
  operation, including transfer
  and readiness of the resulting value. This completion produces a
  value of type \code{T}.
\end{completions}

\ordering The read of \code{src} will have a {\em
  happens-before} relationship with the operation-completion
notification actions (future readying, promise fulfillment, or persona
LPC enlistment). All evaluations {\em sequenced-before} this
call will have a {\em happens-before} relationship with the invocation
of any LPC associated with operation completion.

\progresslevel{internal}
\end{quote}

\begin{plstlisting}
template<typename T,
         typename Completions=decltype(operation_cx::as_future())>
RType rget(global_ptr<T> src, T *dest, std::size_t count,
           Completions cxs=Completions{});
\end{plstlisting}
\Indx{rget @{\tt rget}!bulk {\tt rget}}
\begin{quote}
\begin{sloppypar}
\precondition \code{T} must be DefinitelyTriviallySerializable. 
The addresses in \code{[src,src+count)} and \code{[dest,dest+count)}
must not overlap.
\end{sloppypar}

\pnum
Initiates a transfer of \code{count} values of type \code{T} beginning
at \code{src} and stores them in the locations beginning at
\code{dest}. The source values must not be modified until operation
completion is notified.

\begin{completions}
\item{Operation} Indicates completion of all aspects of the
  operation, including transfer
  and readiness of the resulting values.
\end{completions}

\ordering The reads of \code{src} and writes to
\code{dest} will have a {\em happens-before} relationship with the
operation-completion notification actions (future readying, promise
fulfillment, or persona LPC enlistment). All evaluations {\em
  sequenced-before} this
call will have a {\em happens-before} relationship with the invocation
of any LPC associated with operation completion.

\progresslevel{internal}
\end{quote}
