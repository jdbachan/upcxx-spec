\chapter{Overview and Scope}
\label{ch:overview}
\indent
\section{Preliminaries}

\pnum
\upcxx is a C++11 library providing classes and functions that support Partitioned Global Address Space (PGAS) programming.
The project began in 2012 with a prototype AKA V0.1, described in
the IPDPS14 paper by {\em Zheng et al.} \cite{zheng:ipdps14}.
This document describes a production version, V1.0, with the addition
of several features and a new asynchronous API.

\pnum 
Under the PGAS model, a distributed memory parallel computer is
viewed abstractly as a collection of {\em processing elements}, an individual
computing resource, each with {\em local memory} (see Fig. \ref{fig:pgas}).
A processing element is called a {\em process} in \upcxx.
The execution model of \upcxx is SPMD and the
number of \upcxx processes is fixed during program execution.

\pnum
As with conventional C++ threads programming, processes can access
their respective local memory via a pointer.
However,  the PGAS
abstraction supports a global address space,
which is allocated in {\em shared segments} distributed over the processes.
A {\em global pointer} \Indx{Global Pointer}
enables the programmer to
move data in the shared segments between processes as shown in Fig. \ref{fig:pgas}.
As with threads programming,
references made via global pointers are subject to race conditions,
and appropriate synchronization must be employed.
\begin{figure}[ht]
\begin{center}
\includegraphics[scale=0.4]{figures/pgas.png}
\end{center}
\caption{Abstract Machine Model of a PGAS program memory
   }
\label{fig:pgas}
\end{figure}


\pnum
\upcxx global pointers are fundamentally different from  conventional 
C-style pointers. A global pointer refers to a location in a shared 
segment. It cannot be dereferenced using the \code{*} operator, and it does
not support conversions between pointers to base and derived types. 
It also cannot be constructed by the address-of operator.  On the other hand,
 \upcxx global pointers {\em do} support some  properties of a regular 
C pointer, such as pointer arithmetic and passing a pointer by value.

\pnum
Notably, global pointers are used in {\em one-sided} communication:
bulk copying operations (RMA) similar to {\em memcpy} but across processes
(Ch. \ref{ch:rputrget}), and in Remote Procedure Calls (RPC,
Ch. \ref{ch:rpc}).
% In contrast to a message-passing style of programming, \upcxx focuses
% on {\em one-sided} communication via RMA and Remote Procedure Call (RPC).
RPC enables the programmer to
ship functions to other processes,
which is useful in managing irregular distributed data structures.
These processes can push or pull data via global pointers.
%in the same way as the C++11 standard {\tt async} library for
%shared-memory systems.
{\em Futures} and {\em Promises} (Ch. \ref{ch:futures}) are 
used to determine completion of communication or to provide handlers that 
respond to completion.  Wherever possible, \upcxx will engage
low-level hardware support for communication and
this capability is crucial to \upcxx's support of
{\em lightweight communication}. 


\pnum
\upcxx's design philosophy is to provide ``close to the metal
performance.'' To meet this requirement, \upcxx imposes certain
restrictions. In particular, non-blocking communication is the default
for nearly all operations defined in the API, and all communication is
explicit. These two restrictions encourage the programmer to write
code that is performant and make it more difficult to write code that
is not. Conversely, \upcxx relaxes some restrictions found in models
such as MPI; in particular, it does not impose an in-order delivery requirement between
separate communication operations.
The added flexibility increases the possibility of
overlapping communication and scheduling it appropriately.

\pnum
\upcxx also avoids non-scalable constructs found in models such as
UPC. For example, it does not support shared distributed arrays or
shared scalars. Instead, it provides distributed objects, which can be
used to similar ends (Ch. \ref{ch:distobject}). Distributed objects are
useful in solving the {\em bootstrapping problem}, whereby processes need
to distribute their local copies of global pointers to other processes.
Though \upcxx does not directly provide multidimensional arrays,
applications that use \upcxx may define them. To this end, \upcxx
supports non-contiguous data transfers: vector, indexed,
and strided data (Ch. \ref{ch:vis}).

\pnum
Because \upcxx does not provide separate concurrent threads to manage progress,
\upcxx must manage all progress inside active calls to the library.
\upcxx has been designed with a policy against the use of internal 
operating system threads. The strengths of this approach are improved 
user-visibility into the resource requirements of \upcxx and better 
interoperability with software packages and their possibly restrictive 
threading requirements. The consequence, however, is that the user must be
conscientious to balance the need for making progress against
the application's need for CPU cycles.
Chapter \ref{ch:progress} discusses subtleties of managing progress
and how an application can arrange for \upcxx to advance the state of
asynchronous communication.

\pnum
Processes may be grouped into teams (Ch. \ref{ch:teams}). A team can
participate in collective operations. Teams are also the interface
that \upcxx uses to propagate the shared memory capabilities of the
underlying hardware and operating system and can let a programmer
reason about hierarchical processor-memory organization, allowing an
application to reduce its memory footprint. \upcxx supports atomic
operations, currently on remote 32-bit and 64-bit integers. Atomics
are useful in managing distributed queues, hash tables, and so on.
However, as explained in the discussion below on \upcxx's memory
model, atomics are split phased and not handled the same way as they
are in C++11 and other libraries.

\pnum
\upcxx will support memory kinds (Ch. \ref{ch:kinds}), whereby the
programmer can identify regions of memory requiring different access
methods or having different performance properties, such as device
memory. Since memory kinds will be implemented in Year 2, we will
defer their detailed discussion until next year.

\section{Execution Model}
\Indx{Execution Model}
 
\pnum
The \upcxx internal state contains, for each process, internal unordered
queues that are managed for the user. The \upcxx progress engine scans
these queues for operations initiated by this process, as well as externally
generated operations that target this process. The progress engine is
active inside \upcxx calls only and is quiescent at other times, as
there are no threads or background processes executing inside
\upcxx. 
This
passive stance permits \upcxx to be driven by any other execution
model a user might choose. This universality does place a small burden
on the user: calling into the \progress function. \upcxx relies on the
user to make periodic calls into the \progress function to ensure that
\upcxx operations are completed. \progress is the mechanism by which
the user loans \upcxx a thread of execution to perform operations that
target the given process. The user can determine that a specific
operation completes by checking the status of its associated \future,
or by attaching a completion handler to the operation.
 
\pnum
\upcxx presents a {\em thread-aware} programming model. It assumes
that only one thread of execution is interacting with any \upcxx
object. The abstraction for thread-awareness in \upcxx is the {\em
  persona}. A \future produced by a thread of execution is associated
with its persona, and transferring the \future to another thread must
be accompanied by transferring the underlying persona. Each process has a
{\em master persona}, initially attached to the thread that calls
\init. Some \upcxx operations, such as \barrier, require a thread to
have exclusive access to the master persona to call them. Thus, the
programmer is responsible for ensuring synchronized access to both
personas and memory, and that access to shared data does not interfere
with the internal operation of \upcxx.

\section{Memory Model}

\pnum
The \upcxx memory model differs from that of C++11 (and beyond)
in that all updates are split-phased: every communication operation
has a distinct initiate and wait step. Thus, atomic operations execute over
a time interval, and the time intervals of successive operations that target
the same datum must not overlap, or a data race will result.

\pnum
\upcxx differs from MPI in that it doesn't guarantee in-order
delivery. For example, if we overlap two successive \RPC operations
involving the same source and destination process, we cannot say which
one completes first.
% ak: commenting this out until we come up with a more accurate statement
% The only way to ensure the order is to place a
% control-flow dependency between the wait of the first and the initiate
% of the second operation.

%\section{Benefits of \upcxx}
%
%\upcxx is intended for challenging applications that employ fine grained or irregular communication. \upcxx's support for communication is lightweight,
%and will deliver hardware offload performance when available.\footnote{\upcxx is currently implemented on top of GASNet, see \url{http://gasnet.lbl.gov}.}
%Thus, if the hardware provides special support for communication, e.g. collectives or atomics, then \upcxx will utilize that support.

%\upcxx's benefit is 
%due to four attributes:
%\begin{itemize}
%\item Ease in employing distributed irregular data structures in algorithms that move data at a fine granularity, and that have a low computational intensity.
%\item The ability to move data to code or code to the data as needed
% without incurring high programming overheads and
%\item The ability to do both of those things asynchronously and autonomously;
%\item It leverages modern C++ features and interoperates with MPI, OpenMP and other parallel and distributed programming frameworks.
%\end{itemize} 

\section{Common Requirements}
\pnum
Unless explicitly stated otherwise, the requirements in
[res.on.arguments] in the C++ standard apply to \upcxx functions as
well. In particular, if a local or global pointer passed to a \upcxx
function is invalid for its intended use, the behavior of the function
is undefined.

\pnum
For \upcxx functions with a \emph{Precondition(s)} clause, violation
of the preconditions results in undefined behavior.

\section{Organization of this Document}
\pnum
This specification is intended to be a normative reference - a Programmer's Manual is
forthcoming. For the purposes of understanding the key ideas in
\upcxx, we recommend that the novice reader skip Chapter
\ref{ch:progress} (Progress) and the advanced topics related to
futures, personas, and continuation-based communication.

\pnum
The organization for the rest of the document is as follows.
Chapter \ref{ch:initfin} discusses the process of starting up and
closing down \upcxx.
Global pointers (Ch. \ref{ch:globalptr}) are fundamental
to the PGAS model, and Chapter \ref{ch:storage} discusses storage
allocation.
Since \upcxx supports asynchronous communication only, \upcxx provides
futures and promises (Ch. \ref{ch:futures}) to manage control flow
and completion.
Chapters \ref{ch:rputrget} and \ref{ch:rpc} describe the two forms of
asynchronous one-sided communication, \rput/\rget and RPC,
respectively.
Chapter \ref{ch:progress} discusses progress.
Chapter \ref{ch:atomics} discusses atomic operations.
Chapter \ref{ch:teams} discusses teams, which are a means of
organizing \upcxx processes.
Chapter \ref{ch:distobject} discusses distributed objects.
Chapter \ref{ch:vis} discusses non-contiguous data transfers.
Chapter \ref{ch:kinds} discusses memory kinds.

\section{Conventions}
\Indx{Conventions}
\begin{enumerate}
\item {\tt C++} language keywords are in the color {\textcolor{mocha}{mocha}}.

\item \upcxx terms are set in the color {\textcolor{blue}{bright blue}}
except when they appear  in a synopsis framebox.

\item 
All functions are declared {\textcolor{mocha}{noexcept}}
unless specifically called out.

\item All entities are in the \code{upcxx} namespace unless otherwise qualified.
\end{enumerate}

\input{Glossary}
