
\chapter{Non-Contiguous One-Sided Communication}
\label{ch:vis}

\section{Overview}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\pnum
\upcxx provides functions to perform one-sided communications
similar to \code{rget} and \code{rput} which are dedicated to handle
data stored in non-contiguous locations. These functions are denoted
with a suffix added to the type of operation, in increasing order of specialization:

\pnum
\code{\{rput,rget\}\_\{irregular,regular,strided\}}

\pnum
The most general variant of the API, \code{\{rput,rget\}\_irregular},
accept iterators over an array or collection
of \code{std::pair} (or \code{std::tuple}) that contain
a local or global pointer to a memory location in the first member while the
second member contains the size of the contiguous chunk of memory to be transferred.
This variant is capable of expressing non-contiguous RMA of arbitrary shape,
but pays the highest overhead in metadata to payload ratio.

\pnum
The next set of functions, \code{\{rput,rget\}\_regular},
operates over contiguous elements of identical size on each side of the transfer, 
and only requires the caller to provide an array or collection of base pointers to each element.

\begin{figure}[ht]
  \centering
  \includegraphics[width=.7\linewidth]{figures/vis_strided.png}
  \caption{Example of a 3-D strided translational copy, with associated metadata \label{fig.vis_strided}} 
\end{figure}

\pnum
Finally, the most specialized set of functions, \code{\{rput,rget\}\_strided},
provide an interface for expressing 
translational and transposing copies between
arbitrary rectangular sections of densely stored N-dimensional arrays.
This specialized pattern requires the least metadata, which is constant in size for a given dimensionality.
An example of such a transfer is depicted in Figure~\ref{fig.vis_strided}.

\section{API Reference}

\subsection{Requirements on Iterators}

\pnum
An iterator used with a \upcxx operation in this section must adhere
to the following requirements:
\begin{pitemize}
\item It must satisfy the Iterator and EqualityComparable C++
  concepts.
\item Calling \code{std::distance} on the iterator must not invalidate
  it.
\end{pitemize}

\subsection{Irregular Put}

\begin{plstlisting}
template<typename SrcIter, typename DestIter,
         typename Completions=decltype(operation_cx::as_future())>
RType rput_irregular(
  SrcIter src_runs_begin, SrcIter src_runs_end,
  DestIter dest_runs_begin, DestIter dest_runs_end,
  Completions cxs=Completions{});
\end{plstlisting}
\Indx{rput\_irregular @{\tt rput\_irregular}}
\begin{quote}

\preconditionlst

\begin{pitemize}
\item
\code{SrcIter} and \code{DestIter} both satisfy the
iterator requirements above.

\item
\code{std::get<0>(*std::declval<SrcIter>())} has a return type
convertible to \code{T const*}, for some
DefinitelyTriviallySerializable type \code{T}.

\item
\code{std::get<1>(*std::declval<SrcIter>())} has a return type
convertible to \code{std::size_t}.

\item
\code{std::get<0>(*std::declval<DestIter>())} has the return type\\
\code{global_ptr<T>}, for the same type \code{T} as with
\code{SrcIter}.

\item
\code{std::get<1>(*std::declval<DestIter>())} has a return type
convertible to \code{std::size_t}.

\item
All destination addresses must be \code{global_ptr<T>}'s referencing
memory with affinity to the same process.

\item
The length of the expanded address sequence (the sum over the run
lengths) must be the same for the source and destination sequences.

\end{pitemize}

\pnum
For some type \code{T}, takes a sequence of source addresses of
\code{T const*} and a sequence of destination addresses of
\code{global_ptr<T>} and does the corresponding puts from each source
address to the destination address of the same sequence position.

\pnum
Address sequences are encoded in run-length form as sequences of runs,
where each run is a pair consisting of a starting address plus the
number of consecutive elements of type \code{T} beginning at that address.

\pnum
As an example of valid types for individual runs, \code{SrcIter} could
be an iterator over elements of type
\code{std::pair<T const*, std::size_t>}, and \code{DestIter} an
iterator over \code{std::pair<global_ptr<T>, std::size_t>}. Variations
replacing \code{std::pair} with \code{std::tuple} or \code{size_t} with
other primitive integral types are also valid.

\pnum
The source sequence iterators must remain valid, and the underlying
addresses and source memory contents must not be modified until source
completion is signaled. Only after source completion is signaled can
the source address sequences and memory be reclaimed by the
application.

\pnum
The destination sequence iterators must remain valid until source
completion is signaled.

\pnum
The destination memory regions must be completely disjoint and must not overlap
with any source memory regions, otherwise behavior is undefined. 
Source regions are permitted to overlap with each other.

\begin{completions}
\item{Source} Indicates that the source sequence iterators and
  underlying memory, as well as the destination sequence iterators,
  are no longer in use by \upcxx and may be reclaimed by the user.
\item{Remote} Indicates completion of the transfer
  of all values.
\item{Operation} Indicates completion of all aspects of the
  operation: the transfer and remote stores are complete.
\end{completions}

\ordering The reads of the sources will have a {\em
  happens-before} relationship with the source-completion notification
actions (future readying, promise fulfillment, or persona LPC
enlistment). The writes to the destinations will have a
{\em happens-before} relationship with the operation-completion
notification actions (future readying, promise fulfillment, or persona
LPC enlistment) and remote-completion actions (RPC enlistment). For
LPC and RPC completions, all
evaluations {\em sequenced-before} this call will have a {\em
  happens-before} relationship with the execution of the completion
function.

\progresslevel{internal}
\end{quote}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Irregular Get}

\begin{plstlisting}
template<typename SrcIter, typename DestIter,
         typename Completions=decltype(operation_cx::as_future())>
RType rget_irregular(
  SrcIter src_runs_begin, SrcIter src_runs_end,
  DestIter dest_runs_begin, DestIter dest_runs_end,
  Completions cxs=Completions{});
\end{plstlisting}
\Indx{rget\_irregular @{\tt rget\_irregular}}
\begin{quote}

\preconditionlst

\begin{pitemize}

\item
\code{SrcIter} and \code{DestIter} both satisfy the
iterator requirements above.

\item
\code{std::get<0>(*std::declval<SrcIter>())} has the type
\code{global_ptr<T>} for some DefinitelyTriviallySerializable type
\code{T}.

\item
\code{std::get<1>(*std::declval<SrcIter>())} has a type that is
convertible to \code{std::size_t}.

\item
\code{std::get<0>(*std::declval<DestIter>())} has the type
\code{T*}, for the same type \code{T} as with \code{SrcIter}.

\item
\code{std::get<1>(*std::declval<DestIter>())} has a type that is
convertible to \code{std::size_t}.

\item
All source addresses must be \code{global_ptr<T>}'s referencing memory
with affinity to the same process.

\item
The length of the expanded address sequence (the sum over the run
lengths) must be the same for the source and destination sequences.

\end{pitemize}

\pnum
For some type \code{T}, takes a sequence of source addresses of
\code{global_ptr<T>} and a sequence of destination addresses of
\code{T*} and does the corresponding gets from each source
address to the destination address of the same sequence position.

\pnum
Address sequences are encoded in run-length form as sequences of runs,
where each run is a pair consisting of a starting address plus the
number of consecutive elements of type \code{T} beginning at that address.

\pnum
As an example of valid types for individual runs, \code{DestIter} could
be an iterator over elements of type
\code{std::pair<T*, std::size_t>}, and \code{SrcIter} an
iterator over \code{std::pair<global_ptr<T>, std::size_t>}. Variations
replacing \code{std::pair} with \code{std::tuple} or \code{size_t} with
other primitive integral types are also valid.

\pnum
The source sequence iterators must remain valid, and the underlying
addresses and memory contents must not be modified until operation
completion is signaled. Only after operation completion is signaled
can the address sequences and source memory be reclaimed by the
application.

\pnum
The destination sequence iterators must remain valid until operation
completion is signaled.

\pnum
The destination memory regions must be completely disjoint and must not overlap
with any source memory regions, otherwise behavior is undefined. 
Source regions are permitted to overlap with each other.

\begin{completions}
\item{Operation} Indicates completion of all aspects of the
  operation: the transfer and local stores are complete.
\end{completions}

\ordering The reads of the sources and writes to the
destinations will have a
{\em happens-before} relationship with the operation-completion
notification actions (future readying, promise fulfillment, or persona
LPC enlistment). For LPC completions, all evaluations {\em
  sequenced-before} this call will have a {\em happens-before}
relationship with the execution of the completion function.

\progresslevel{internal}
\end{quote}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Regular Put}

\begin{plstlisting}
template<typename SrcIter, typename DestIter,
         typename Completions=decltype(operation_cx::as_future())>
RType rput_regular(
  SrcIter src_runs_begin, SrcIter src_runs_end,
  std::size_t src_run_length,
  DestIter dest_runs_begin, DestIter dest_runs_end,
  std::size_t dest_run_length,
  Completions cxs=Completions{});
\end{plstlisting}
\Indx{rput\_regular @{\tt rput\_regular}}

\begin{quote}

\preconditionlst

\begin{pitemize}

\item
\code{SrcIter} and \code{DestIter} both satisfy the iterator
requirements above.

\item
\code{*std::declval<SrcIter>()} has a type convertible to
\code{T const*}, for some DefinitelyTriviallySerializable type
\code{T}.

\item
\code{*std::declval<DestIter>())} has the type \code{global_ptr<T>},
for the same type \code{T} as with \code{SrcIter}.

\item
All destination addresses must be \code{global_ptr<T>}'s referencing
memory with affinity to the same process.

\item
\begin{sloppypar}
The length of the two sequences delimited by
(\code{src_runs_begin}, \code{src_runs_end}) and
(\code{dest_runs_begin}, \code{dest_runs_end}) multiplied by
\code{src_run_length} and \code{dest_run_length}, respectively, must be
the same.
\end{sloppypar}

\end{pitemize}

\pnum
This call has the same semantics as \code{rput_irregular}
with the exception that, for each sequence, all run
lengths are the same and are factored out of the sequences into two
extra parameters \code{src_run_length} and \code{dest_run_length},
which express the number of consecutive elements of type \code{T} in units of element count.
Thus the iterated elements are no longer pairs, but just pointers.

\pnum
The source sequence iterators must remain valid, and the underlying
addresses and source memory contents must not be modified until source
completion is signaled. Only after source completion is signaled can
the source address sequences and memory be reclaimed by the
application.

\pnum
The destination sequence iterators must remain valid until source
completion is signaled.

\begin{completions}
\item{Source} Indicates that the source sequence iterators and
  underlying memory, as well as the destination sequence iterators,
  are no longer in use by \upcxx and may be reclaimed by the user.
\item{Remote} Indicates completion of the transfer
  of all values.
\item{Operation} Indicates completion of all aspects of the
  operation: the transfer and remote stores are complete.
\end{completions}

\ordering The reads of the sources will have a {\em
  happens-before} relationship with the source-completion notification
actions (future readying, promise fulfillment, or persona LPC
enlistment). The writes to the destinations will have a
{\em happens-before} relationship with the operation-completion
notification actions (future readying, promise fulfillment, or persona
LPC enlistment) and remote-completion actions (RPC enlistment). For
LPC and RPC completions, all
evaluations {\em sequenced-before} this call will have a {\em
  happens-before} relationship with the execution of the completion
function.

\progresslevel{internal}
\end{quote}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Regular Get}

\begin{plstlisting}
template<typename SrcIter, typename DestIter,
         typename Completions=decltype(operation_cx::as_future())>
RType rget_regular(
  SrcIter src_runs_begin, SrcIter src_runs_end,
  std::size_t src_run_length,
  DestIter dest_runs_begin, DestIter dest_runs_end,
  std::size_t dest_run_length,
  Completions cxs=Completions{});
\end{plstlisting}
\Indx{rget\_regular @{\tt rget\_regular}}

\begin{quote}

\preconditionlst

\begin{pitemize}

\item
\code{SrcIter} and \code{DestIter} both satisfy the iterator
requirements above.

\item
\code{*std::declval<DestIter>()} has a type convertible to
\code{T*}, for some DefinitelyTriviallySerializable type \code{T}.

\item
\code{*std::declval<SrcIter>())} has the type \code{global_ptr<T>},
for the same type \code{T} as with \code{DestIter}.

\item
All source addresses must be \code{global_ptr<T>}'s referencing memory
with affinity to the same process.

\item
\begin{sloppypar}
The length of the two sequences delimited by
(\code{src_runs_begin}, \code{src_runs_end}) and
(\code{dest_runs_begin}, \code{dest_runs_end}) multiplied by
\code{src_run_length} and \code{dest_run_length}, respectively, must be
the same.
\end{sloppypar}

\end{pitemize}

\pnum
This call has the same semantics as \code{rget_irregular}
with the exception that, for each sequence, all run
lengths are the same and are factored out of the sequences into two
extra parameters \code{src_run_length} and \code{dest_run_length},
which express the number of consecutive elements of type \code{T} in units of element count.
Thus, the iterated elements are no longer pairs, but just pointers.

\pnum
The source sequence iterators must remain valid, and the underlying
addresses and memory contents must not be modified until operation
completion is signaled. Only after operation completion is signaled
can the address sequences and source memory be reclaimed by the
application.

\pnum
The destination sequence iterators must remain valid until operation
completion is signaled.

\begin{completions}
\item{Operation} Indicates completion of all aspects of the
  operation: the transfer and local stores are complete.
\end{completions}

\ordering The reads of the sources and writes to the
destinations will have a
{\em happens-before} relationship with the operation-completion
notification actions (future readying, promise fulfillment, or persona
LPC enlistment). For LPC completions, all evaluations {\em
  sequenced-before} this call will have a {\em happens-before}
relationship with the execution of the completion function.

\progresslevel{internal}
\end{quote}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Strided Put}

\begin{plstlisting}
template<std::size_t Dim, typename T,
         typename Completions=decltype(operation_cx::as_future())>
RType rput_strided(
  T const *src_base,
  std::ptrdiff_t const *src_strides,
  global_ptr<T> dest_base,
  std::ptrdiff_t const *dest_strides,
  std::size_t const *extents,
  Completions cxs=Completions{});

template<std::size_t Dim, typename T,
         typename Completions=decltype(operation_cx::as_future())>
RType rput_strided(
  T const *src_base,
  std::array<std::ptrdiff_t,Dim> const &src_strides,
  global_ptr<T> dest_base,
  std::array<std::ptrdiff_t,Dim> const &dest_strides,
  std::array<std::size_t,Dim> const &extents,
  Completions cxs=Completions{});
\end{plstlisting}
\Indx{rput\_strided @{\tt rput\_strided}}

\begin{quote}

\precondition \code{T} must be a DefinitelyTriviallySerializable type.

\pnum
If Dim == 0, \code{src_strides}, \code{dest_strides},
and \code{extents} are ignored, and the data movement performed is
equivalent to \code{rput(src_base, dest_base, 1)}.

\pnum
Otherwise, performs the semantic equivalent of many put's of type \code{T}.
Let the {\em index space} be the set of integer vectors of dimension
\code{Dim} contained in the bounding box with the inclusive lower bound at the
all-zero origin, and the exclusive upper bound equal to \code{extents}.
For each index vector \code{index} in this index space, a put will be 
executed with source and destination addresses computed according to the following pseudo-code,
where {\tt dotprod} is the vector dot product and
pointer arithmetic is done in units of bytes (not elements of {\t T}):

\begin{plstlisting}
src_address  = src_base  + dotprod(index, src_strides)
dest_address = dest_base + dotprod(index, dest_strides)
\end{plstlisting}

\pnum
Note this implies the elements of the \code{src_strides} and \code{dest_strides}
arrays are expressed in units of bytes.

\pnum
The destination memory regions must be completely disjoint and must not overlap
with any source memory regions, otherwise behavior is undefined. 
Source regions are permitted to overlap with each other.

\pnum
The elements of type \code{T} residing in the 
source addresses must remain valid and unmodified until source
completion is signaled.

\pnum
The contents of the \code{src_strides}, \code{dest_strides}, and \code{extents} arrays
are consumed synchronously before the call returns.

\begin{completions}
\item{Source} Indicates that the source memory is no longer in
  use by \upcxx and may be reclaimed by the user.
\item{Remote} Indicates completion of the transfer
  of all values.
\item{Operation} Indicates completion of all aspects of the
  operation: the transfer and remote stores are complete.
\end{completions}

\ordering The reads of the sources will have a {\em
  happens-before} relationship with the source-completion notification
actions (future readying, promise fulfillment, or persona LPC
enlistment). The writes to the destinations will have a
{\em happens-before} relationship with the operation-completion
notification actions (future readying, promise fulfillment, or persona
LPC enlistment) and remote-completion actions (RPC enlistment). For
LPC and RPC completions, all
evaluations {\em sequenced-before} this call will have a {\em
  happens-before} relationship with the execution of the completion
function.

\progresslevel{internal}
\end{quote}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Strided Get}

\begin{plstlisting}
template<std::size_t Dim, typename T,
         typename Completions=decltype(operation_cx::as_future())>
RType rget_strided(
  global_ptr<T> src_base,
  std::ptrdiff_t const *src_strides,
  T *dest_base,
  std::ptrdiff_t const *dest_strides,
  std::size_t const *extents,
  Completions cxs=Completions{});

template<std::size_t Dim, typename T,
         typename Completions=decltype(operation_cx::as_future())>
RType rget_strided(
  global_ptr<T> src_base,
  std::array<std::ptrdiff_t,Dim> const &src_strides,
  T *dest_base,
  std::array<std::ptrdiff_t,Dim> const &dest_strides,
  std::array<std::size_t,Dim> const &extents,
  Completions cxs=Completions{});
\end{plstlisting}
\Indx{rget\_strided @{\tt rget\_strided}}

\begin{quote}

\precondition \code{T} must be a DefinitelyTriviallySerializable type.

\pnum
If Dim == 0, \code{src_strides}, \code{dest_strides},
and \code{extents} are ignored, and the data movement performed is
equivalent to \code{rget(src_base, dest_base, 1)}.

\pnum
Otherwise, performs the reverse direction of \code{rput_strided} where now the
source memory is remote and the destination is local.

\pnum
The destination memory regions must be completely disjoint and must not overlap
with any source memory regions, otherwise behavior is undefined. 
Source regions are permitted to overlap with each other.

\pnum
The elements of type \code{T} residing in the 
source addresses must remain valid and unmodified
until operation completion is signaled.

\pnum
The contents of the \code{src_strides}, \code{dest_strides}, and \code{extents} arrays
are consumed synchronously before the call returns.

\begin{completions}
\item{Operation} Indicates completion of all aspects of the
  operation: the transfer and local stores are complete.
\end{completions}

\ordering The reads of the sources and writes to the
destinations will have a
{\em happens-before} relationship with the operation-completion
notification actions (future readying, promise fulfillment, or persona
LPC enlistment). For LPC completions, all evaluations {\em
  sequenced-before} this call will have a {\em happens-before}
relationship with the execution of the completion function.

\progresslevel{internal}
\end{quote}
