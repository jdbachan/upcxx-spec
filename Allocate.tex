\chapter{Storage Management}
\label{ch:storage}

\commentout{
\noindent
\begin{plstlisting}[frame=single,style=upcxx] 
namespace upcxx {

 void* allocate(size_t size,
                size_t alignment = alignof(std::max_align_t));

 template<typename T, size_t alignment = alignof(T)>
 global_ptr<T> allocate(size_t n=1);

 void deallocate(void* p);

 template<typename T>
 void deallocate(global_ptr<T> g);

 template<typename T, typename ...Args>
 global_ptr<T> new_(Args &&...args);

 template<typename T, typename ...Args>
 global_ptr<T> new_(const std::nothrow_t &tag, Args &&...args);

 template<typename T>
 global_ptr<T> new_array(size_t n);

 template<typename T>
 global_ptr<T> new_array(size_t n, const std::nothrow_t &tag);

 template<typename T>
 void delete_(global_ptr<T> g);

 template<typename T>
 void delete_array(global_ptr<T> g);

} // namespace upcxx
\end{plstlisting}
}

\section{Overview}
\pnum
\upcxx provides two flavors of storage allocation involving the shared segement.
% the first  calls a constructor or destructor, the second does not.
The pair of functions \nw and \delete will call the class
constructors and destructors, respectively, as well as allocate and
deallocate memory from the shared segment.
The pair
\allocate and \deallocate allocate and deallocate dynamic memory from the shared segment,
% and are analogous to C99 {\tt malloc} and {\tt free}.
but do not call C++ constructors or destructors.
A user may call these functions directly, or use placement new, or other memory management practices.

\commentout{
\begin{figure}
\begin{plstlisting}
class Bob
{
private:
  int m;
public:
  Bob():m(0){ }
  Bob(int a, int b):m(b+a) { }
};

using namespace upcxx;

int main(int argc, char* argv[])
{
  init();
  global_ptr<Bob> bob1 = allocate<Bob>();
  global_ptr<Bob> bob2 = allocate<Bob>(5);
  global_ptr<Bob> bob3 = new_<Bob>(5,2);
  global_ptr<Bob> bob12 = new_<Bob>(1,2);
  global_ptr<Bob> bob13 = new_<Bob>(1,3);
  Bob* b4 = (Bob*)allocate(sizeof(Bob));  // codes with old-school malloc
  global_ptr<Bob> bob5(b4); //valid promotion
  Bob barray[5];
  global_ptr<Bob> garray(barray);  // runtime error. not in shared segment
  global_ptr<Bob> garray2 = new_array<Bob,6>();
  deallocate(bob1);
  deallocate(bob2);
  delete bob12.local(); // user calls C++ delete on insides for us.
  bob13.local()->~Bob(); // OK. user calls their destructor
  deallocate(bob13.local()); //OK. .... then puts memory back to shared
  delete_(bob3);
  delete_(bob5); //valid, destructors will be called though
  delete_(garray2);
  finalize();
  return 0;
}
\end{plstlisting}
\caption{Example usage of UPC++ allocation}
\end{figure}
}

\section{API Reference}
% \subsection{C++ variants}
\begin{plstlisting}
template<typename T, typename ...Args>
global_ptr<T> new_(Args &&...args);
\end{plstlisting}
\Indx{new\_@{\tt new\_}}
\begin{quote}
\precondition \code{T(args...)} must be a valid call to a
constructor for \code{T}.

\pnum
Allocates space for an object of type \code{T} from the shared segment
of the calling process. If the allocation succeeds, returns a pointer to
the start of the allocated memory, and the object is initialized by
invoking the constructor \code{T(args...)}. If the allocation fails,
throws \code{std::bad_alloc}.

\exceptions May throw \code{std::bad_alloc} or any exception
thrown by the call \code{T(args...)}.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
template<typename T, typename ...Args>
global_ptr<T> new_(const std::nothrow_t &tag, Args &&...args);
\end{plstlisting}
\Indx{new\_@{\tt new\_}}
\begin{quote}
\precondition \code{T(args...)} must be a valid call to a
constructor for \code{T}.

\pnum
Allocates space for an object of type \code{T} from the shared segment
of the calling process. If the allocation succeeds, returns a pointer to
the start of the allocated memory, and the object is initialized by
invoking the constructor \code{T(args...)}. If the allocation fails,
returns a null pointer.

\exceptions May throw any exception thrown by the call
\code{T(args...)}.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
template<typename T>
global_ptr<T> new_array(size_t n);
\end{plstlisting}
\Indx{new\_array@{\tt new\_array}}
\begin{quote}
\precondition \code{T} must be DefaultConstructible.

\pnum
Allocates space for an array of \code{n} objects of type \code{T} from
the shared segment of the calling process. If the allocation succeeds,
returns a pointer to the start of the allocated memory, and the
objects are initialized by invoking their default constructors. If the
allocation fails, throws \code{std::bad_alloc}.

\exceptions May throw \code{std::bad_alloc} or any exception
thrown by the call \code{T()}. If an exception is thrown by the
constructor for \code{T}, then previously initialized elements are
destroyed in reverse order of construction.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
template<typename T>
global_ptr<T> new_array(size_t n, const std::nothrow_t &tag);
\end{plstlisting}
\Indx{new\_array@{\tt new\_array}}
\begin{quote}
\precondition \code{T} must be DefaultConstructible.

\pnum
Allocates space for an array of \code{n} objects of type \code{T} from
the shared segment of the calling process. If the allocation succeeds,
returns a pointer to the start of the allocated memory, and the
objects are initialized by invoking their default constructors. If the
allocation fails, returns a null pointer.

\exceptions May throw any exception thrown by the call
\code{T()}. If an exception is thrown by the constructor for \code{T},
then previously initialized elements are destroyed in reverse order of
construction.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
template<typename T>
void delete_(global_ptr<T> g);
\end{plstlisting}
\Indx{delete\_@{\tt delete\_}}
\begin{quote}
\precondition 
\code{T} must be Destructible.
\code{g} must be either a null pointer or a non-deallocated pointer that
resulted from a call to \code{new_<T, Args...>} on the calling process,
for some value of \code{Args...}. 

\pnum
If \code{g} is not a null pointer, invokes the destructor on the given
object and deallocates the storage allocated to it. Does nothing if
\code{g} is a null pointer.

\exceptions May throw any exception thrown by the the destructor
for \code{T}.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
template<typename T>
void delete_array(global_ptr<T> g);
\end{plstlisting}
\Indx{delete\_array@{\tt delete\_array}}
\begin{quote}
\precondition 
\code{T} must be Destructible.
\code{g} must be either a null pointer or a non-deallocated pointer that
resulted from a call to \code{new_array<T>} on the calling process.

\pnum
If \code{g} is not a null pointer, invokes the destructor on each
object in the given array and deallocates the storage allocated to it.
Does nothing if \code{g} is a null pointer.

\exceptions May throw any exception thrown by the the destructor
for \code{T}.

\progresslevel{none}
\end{quote}

% \subsection{C99 variants}

\begin{plstlisting}
void* allocate(size_t size,
               size_t alignment = alignof(std::max_align_t));
\end{plstlisting}
\Indx{allocate@{\tt allocate}}
\begin{quote}
\precondition \code{alignment} is a valid alignment. \code{size}
must be an integral multiple of \code{alignment}.

\pnum
Allocates \code{size} bytes of memory from the shared segment of the
calling process, with alignment as specified by \code{alignment}. If the
allocation succeeds, returns a pointer to the start of the allocated
memory, and the allocated memory is uninitialized. If the allocation
fails, returns a null pointer.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
template<typename T, size_t alignment = alignof(T)>
global_ptr<T> allocate(size_t n=1);
\end{plstlisting}
\Indx{allocate@{\tt allocate}}
\begin{quote}
\precondition \code{alignment} is a valid alignment.

\pnum
Allocates enough space for \code{n} objects of type \code{T} from the
shared segment of the calling process, with the memory aligned as
specified by \code{alignment}. If the allocation succeeds, returns a
pointer to the start of the allocated memory, and the allocated memory
is uninitialized. If the allocation fails, returns a null pointer.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
void deallocate(void* p);
\end{plstlisting}
\Indx{deallocate@{\tt deallocate}}
\begin{quote}
\precondition \code{p} must be either a null pointer or a
non-deallocated pointer that resulted from a call to the first form of
\code{allocate} on the calling process.

\pnum
Deallocates the storage previously allocated by a call to
\code{allocate}. Does nothing if \code{p} is a null pointer.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
template<typename T>
void deallocate(global_ptr<T> g);
\end{plstlisting}
\Indx{deallocate@{\tt deallocate}}
\begin{quote}
\precondition \code{g} must be either a null pointer or a
non-deallocated pointer that resulted from a call to \code{allocate<T,
  alignment>} on the calling process, for some value of \code{alignment}.

\pnum
Deallocates the storage previously allocated by a call to
\code{allocate}. Does nothing if \code{g} is a null pointer. Does not
invoke the destructor for \code{T}.

\progresslevel{none}
\end{quote}
