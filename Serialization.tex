\chapter{Serialization}
\label{ch:serialization}

\pnum
  As a communication library, \upcxx needs to send C++ types between
  processes that might be separated by a network interface.  The underlying
  GASNet networking interface sends and receives bytes, thus, \upcxx
  needs to be able to convert C++ types to and from bytes.

\section{Class Serialization Interface}
\Indx{Serialization!User-defined}

\pnum
  For standard TriviallyCopyable data types, \upcxx can serialize and deserialize
  these objects for the user without extra intervention on their
  part. For user data types that have more involved serialization requirements, the
  user needs to take two steps to inform \upcxx about how to serialize
  the object.
\begin{penumerate}
\item Declare their type to be a friend of \access
\item Implement the visitor function {\tt serialize}
\end{penumerate}
\Indx{serialize@{\tt serialize}}
\pnum
The type must also satisfy the C++ CopyConstructible concept.

\pnum
Figure \ref{fig:serialize} provides an example of this process.
The definition of the \code{&} operator for the
\code{Archive} class depends on whether \upcxx is serializing or
deserializing an object instance.  

\begin{figure}[ht]
\begin{numlisting}
class UserType {
  // The user's fields and member declarations as usual.
  int member1, member2;
  // ...

  // To enable the serializer to visit the member fields,
  // the user provides this...
  friend class upcxx::access;

  // ...and this
  template<typename Archive>
  void serialize(Archive &ar, unsigned) {
    ar & this->member1;
    ar & this->member2;
    // ...
  }
};
\end{numlisting}
\caption{An example of using \access in a user-defined class}
\label{fig:serialize}
\end{figure}

\pnum
\upcxx provides implementations of \code{operator&} for the C++ built-in types.
\upcxx serialization is compatible with a subset of the {\tt Boost}
serialization interface. This does not imply that \upcxx includes or
requires Boost as a dependency. The reference implementation of \upcxx
does neither of these, it comes with its own implementation of
serialization that simply adheres to the interface set by Boost.
It is acceptable to have \code{friend boost::serialization::access} in place
of \code{friend upcxx::access}.  \upcxx will use your Boost serialization 
in that case.

\pnum
There are restrictions on which actions serialization/deserialization 
routines may perform. They are:

\begin{penumerate}

\item{Serialization/deserialization may not call any \upcxx routine
with a progress level other than {\tt none}.}

\item{\upcxx must perceive these routines as referentially transparent.
Loosely, this means that the routines should be ``pure" functions
between the native representation and a flat sequence of bytes.}

\item{The routines must be thread-safe and permit concurrent invocation
from multiple threads, even when serializing the same object.}

\end{penumerate}

\section{Serialization Concepts}
\Indx{Serialization!Concepts}

\begin{figure}[htbp]
  \centering
  \includegraphics[width=.9\linewidth]{figures/serializable_concepts.png}
  \caption{Serializable UPC++ concepts type hierarchy.
           \upcxx concepts with a corresponding trait query are indicated with a solid line.
           \label{fig.serializable_concepts}}
\end{figure}

\pnum
\upcxx defines the concepts \emph{DefinitelyTriviallySerializable},
\emph{TriviallySerializable}, \emph{DefinitelySerializable},
and \emph{Serializable} that describe what form of
serialization a C++ type supports.
Figure~\ref{fig.serializable_concepts} helps summarize the relationship
of these concepts.
\Indx{DefinitelyTriviallySerializable}
\Indx{TriviallySerializable}
\Indx{DefinitelySerializable}
\Indx{Serializable}

\pnum
A type \code{T} is DefinitelyTriviallySerializable if either of the
following holds:
\begin{pitemize}
\item \code{T} is TriviallyCopyable (i.e.
  \code{std::is_trivially_copyable<T>::value} is \code{true}), and if
  \code{T} is of class type, \code{T} does not implement the \upcxx
  serialization interface described above
\item \code{upcxx::is_definitely_trivially_serializable<T>} is
  specialized to provide a member constant \code{value} that is
  \code{true}
\end{pitemize}
\pnum
In the latter case, \upcxx treats the type \code{T} as if it were
TriviallyCopyable for the purposes of serialization. Thus, \upcxx will
serialize an object of type \code{T} by making a byte copy, and it
will assume \code{T} is TriviallyDestructible when destroying a
deserialized object of type \code{T}.

\pnum
A type \code{T} is TriviallySerializable if it is
semantically valid to copy an object by copying its underlying bytes,
and \upcxx serializes such types by making a byte copy. A type
\code{T} that is DefinitelyTriviallySerializable is also
TriviallySerializable.

\pnum
A type \code{T} is DefinitelySerializable if one of the following
holds:
\begin{pitemize}
\item \code{T} is DefinitelyTriviallySerializable
\item \code{T} is of class type and implements the \upcxx
  serialization interface
\item \code{T} is explicitly described as DefinitelySerializable by
  this specification
\end{pitemize}

\pnum
A type \code{T} is Serializable if it is either
TriviallySerializable or DefinitelySerializable.

\pnum
The type trait \code{upcxx::is_definitely_trivially_serializable<T>}
provides a member constant \code{value} that is \code{true} if
\code{T} is DefinitelyTriviallySerializable and \code{false}
otherwise. This trait may be specialized for user types (types that
are not defined by the C++ or \upcxx standards).

\pnum
The type trait \code{upcxx::is_definitely_serializable<T>} provides a
member constant \code{value} that is \code{true} if \code{T} is
DefinitelySerializable and \code{false} otherwise. This trait may not
be specialized by the user for any types.

\pnum
The set of standard-library container types that are DefinitelySerializable
is implementation-defined. If an implementation defines a container type
\code{T} to be DefinitelySerializable, then
\code{upcxx::is_definitely_serializable<T>::value} must be
\code{true}.

\pnum
Several \upcxx communication operations require that the objects to be
transferred are of DefinitelyTriviallySerializable type. The C++ standard allows
implementations to determine whether or not lambda functions are
TriviallyCopyable, so whether or not such objects are
DefinitelyTriviallySerializable is implementation-dependent.

\pnum
Serializability of a type \code{T} does not imply that objects of type
\code{T} are meaningful on another process. In particular, C++
pointer-to-object and pointer-to-function types are
DefinitelyTriviallySerializable, but it is generally invalid to
dereference a local pointer that originated from another process. More
generally, objects that represent local resources are usually not
meaningful on other processes, whether their types are Serializable or
not.

\section{Functions}
\Indx{Serialization!Function objects}

\pnum
In Chapter \ref{ch:completion} ({\em Completion}) and Chapter
\ref{ch:rpc} ({\em Remote Procedure Calls}) there are several cases
where a C++ {\em FunctionObject} is expected to execute on a destination
process. In these cases the function arguments are serialized as
described in this chapter. The FunctionObject itself (i.e. the
\code{func} argument to \code{rpc}, \code{rpc_ff}, or \code{as_rpc})
is converted to a
function pointer offset from a known {\em sentinel} in the
source program's {\em code segment}.  The details of the
implementation are not described here but typical allowed
FunctionObjects are
\begin{pitemize}
\item C functions
\item C++ global and file-scope functions
\item Class static functions
\item lambda functions
\end{pitemize}
 
\section{Special Handling in Remote Procedure Calls}
\label{sec:special_rpc_args}
\Indx{Serialization!Special cases}

\pnum
Remote procedure calls, whether standalone (\S\ref{ch:rpc}) or
completion based (\S\ref{ch:completion}), perform special handling on
certain non-DefinitelySerializable \upcxx data structures. Arguments
that are
either a reference to \code{dist_object} type (see
\S\ref{ch:distobject} Distributed Objects) or a \code{team} (see
\S\ref{ch:teams} Teams) are transferred by their \code{dist_id} or
\code{team_id} respectively. Execution of the RPC is deferred until
all of the id's have a corresponding instance
constructed on the recipient. When that occurs, \code{func} is
enlisted for execution during user-level progress of the recipent's
master persona (see \S\ref{ch:progress} Progress), and it will be called
with the recipient's instance references in place of those supplied at
the send site.
The behavior is undefined if the recipient's instance of a
\code{dist_object} or \code{team} argument is destroyed before the RPC
executes.

\section{View-Based Serialization}
\Indx{Serialization!View-based}

\pnum
\upcxx also provides a mechanism for serializing the elements of a
sequence. The following is an example of transferring a sequence with
\code{rpc}:

\begin{numlisting}
std::list<double> items = /* fill with elements */;
auto fut = rpc_ff(1, [](view<double> packedlist) {
  // target side gets object containing iterators
  for (double elem : packedlist) {  // traverse network buffer
    process(elem); // process each element
  }
}, make_view(items.begin(), items.end()));
\end{numlisting}
\Indx{view@{\tt view}}
\Indx{make\_view@{\tt make\_view}}

\pnum
In this example, a \code{std::list<double>} contains the elements to
be transferred. Calling \code{make_view} on its begin and end
iterators results in a \code{view}, which can then be
passed to a remote procedure call. The elements in the sequence are
serialized and transferred as part of the RPC, and the target
receives a \code{view} over the elements stored in the
network buffer. The RPC can then iterate over \code{view}
to obtain each element.

\begin{sloppypar}
\pnum
There is an asymmetry in the \code{view} types at the
initiator and target of an RPC, reflecting the difference in how the
underlying sequences are stored in memory. In the example above, the
type of the value
returned by \code{make_view} is \code{view<double,
  std::list<double>::iterator>}, since the initiator supplies
iterators associated with a list. The target of the RPC, however,
receives a \code{view<double,
  view_default_iterator_t<double>>}, with the
\code{view_default_iterator_t<T>} type representing an iterator over a
network buffer. The latter is the default argument for the second
template parameter of \code{view}, so that a user can
specify \code{view<T>} rather than
\code{view<T, view_default_iterator_t<T>>}.
\end{sloppypar}
\Indx{view\_default\_iterator\_t@{\tt view\_default\_iterator\_t}}

\begin{sloppypar}
\pnum
\upcxx provides different handling of \code{view<T>} based on whether
the element type \code{T} is DefinitelyTriviallySerializable or not.
For DefinitelyTriviallySerializable element type, deserialization is a no-op, and
the \code{view<T>} on the recipient is a direct view over a network
buffer, providing both random access and access to the buffer itself.
The corresponding \code{view_default_iterator_t<T>} is an alias for
\code{T*}. On the other hand, if the \code{view} element type is not
DefinitelyTriviallySerializable, then an element must be nontrivially
deserialized before it can be accessed by the user. In such a case, the
\code{view<T>} only provides access through an InputIterator, which
deserializes and returns elements by value, and
\code{view_default_iterator_t<T>} is an alias for
\code{deserializing_iterator<T>}.
\end{sloppypar}
\Indx{deserializing\_iterator@{\tt deserializing\_iterator}}

\pnum
As a non-owning interface, \code{view} only provides \code{const}
access to the elements in the underlying sequence, analogous to C++17
\code{string_view}. However, in the case of a \code{view<T>} that is
received by the target of an RPC, where \code{T} is
DefinitelyTriviallySerializable, the underlying elements are stored
directly in a network buffer as indicated above. There is no external
owning container, so \upcxx permits a user to perform a
\code{const_cast} conversion on an element and modify it.

\pnum
The lifetime of the underlying data buffer and all view iterators on
the target in both the DefinitelyTriviallySerializable and
non-DefinitelyTriviallySerializable cases is restricted by default to
the duration of the RPC. In this case, the elements must be
processed or copied elsewhere before the RPC returns. However, if the
RPC returns a future, then the lifetime of the buffer and view
iterators is extended
until that future is readied. This allows an RPC to initiate an
asynchronous operation to consume the elements, and as long as the
resulting future is returned from the RPC, the underlying buffer will
remain alive until the asynchronous operation is complete and the
future readied.

\pnum
While \upcxx manages the lifetime of the data underlying a \code{view}
when it is an argument to an RPC, the library does not support a
\code{view} as the return type of an RPC due to the lifetime issues it
raises. Thus, an RPC is prohibited from returning a \code{view} even
though it is classified as DefinitelySerializable.

\pnum
The behavior is unspecified when a \code{view<T, IterType>} is passed
to \code{rpc}, \code{rpc_ff}, or \code{as_rpc} if the type \code{T} is
itself a \code{view}.

\section{API Reference}

\begin{plstlisting}
template<typename T>
struct is_definitely_trivially_serializable;
\end{plstlisting}
\Indx{is\_definitely\_trivially\_serializable@{\tt is\_definitely\_trivially\_\\serializable}}
\begin{quote}
\pnum
Provides a member constant \code{value} that is \code{true} if
\code{T} is DefinitelyTriviallySerializable and \code{false}
otherwise. This trait may be specialized for user types.
\end{quote}

\begin{plstlisting}
template<typename T>
struct is_definitely_serializable;
\end{plstlisting}
\Indx{is\_definitely\_serializable@{\tt is\_definitely\_serializable}}
\begin{quote}
\begin{sloppypar}
\pnum
Provides a member constant \code{value} that is \code{true} if
\code{T} is DefinitelySerializable and \code{false} otherwise. This
trait may not be specialized.
However, its value may be indirectly influenced by specializing
\code{is_definitely_trivially_serializable<T>} or implementing the
class serialization interface for \code{T}, as appropriate.
\end{sloppypar}
\end{quote}

\begin{plstlisting}
template<typename T>
class deserializing_iterator {
public:
  // types
  using iterator_category = std::input_iterator_tag;
  using value_type        = T;
  using difference_type   = std::ptrdiff_t;
  using pointer           = T*;
  using reference         = T;

  deserializing_iterator();

  // accessors
  T operator*() const;

  // increment
  deserializing_iterator& operator++();
  deserializing_iterator  operator++(int);
};

// comparisons
template<typename T>
bool operator==(const deserializing_iterator& x,
                const deserializing_iterator& y);
template<typename T>
bool operator!=(const deserializing_iterator& x,
                const deserializing_iterator& y);
\end{plstlisting}
\Indx{deserializing\_iterator@{\tt deserializing\_iterator}}
\begin{quote}
\concepts InputIterator

\begin{sloppypar}
\pnum
An iterator over elements stored in a network buffer. Dereferencing
the iterator causes the element to be deserialized and returned by
value (i.e. \code{deserializing_iterator<T>::reference} is an alias
for \code{T}).

\pnum
While this iterator is classified as an InputIterator, it does not
support \code{operator->}, as the underlying element must be
materialized on demand and its lifetime would not extend beyond the
application of the operator.
\end{sloppypar}

\pnum
{\em UPC++ progress level for all functions above:} {\tt none}
\end{quote}

\begin{plstlisting}
template<typename T>
using view_default_iterator_t = /* ... */;
\end{plstlisting}
\Indx{view\_default\_iterator\_t@{\tt view\_default\_iterator\_t}}
\begin{quote}
\begin{sloppypar}
\pnum
A type alias that is equivalent to \code{T*} if \code{T} is
DefinitelyTriviallySerializable (i.e.
\code{upcxx::is_definitely_trivially_serializable<T>::value} is true),
and \code{deserializing_iterator<T>} otherwise.
\end{sloppypar}
\end{quote}

\begin{plstlisting}
template<typename T, 
         typename IterType = view_default_iterator_t<T>>
class view {
public:
  // types
  using iterator  = IterType;
  using size_type = std::size_t;

  // iterators
  iterator begin();
  iterator end();

  // capacity
  size_type size() const;
};
\end{plstlisting}
\Indx{view@{\tt view}!with general iterator}
\begin{quote}
\concepts DefaultConstructible, CopyConstructible, CopyAssignable,
Destructible

\uconcepts DefinitelySerializable

\pnum
A class template representing a view over an underlying sequence of
elements of type \code{T}, delimited by \code{begin()} and
\code{end()}.

\pnum
{\em UPC++ progress level for all member functions of \code{view}:} {\tt none}
\end{quote}

\begin{plstlisting}
template<typename T>
class view<T, T*> {
public:
  // types
  using value_type             = T;
  using pointer                = T*;
  using const_pointer          = const T*;
  using reference              = T&;
  using const_reference        = const T&;
  using const_iterator         = const T*;
  using iterator               = const_iterator;
  using const_reverse_iterator = 
          std::reverse_iterator<const_iterator>;
  using reverse_iterator       = const_reverse_iterator;
  using size_type              = std::size_t;
  using difference_type        = std::ptrdiff_t;

  // no explicit construct/copy/destroy for non-owning type

  // iterators
  const_iterator         begin() const;
  const_iterator         cbegin() const;

  const_iterator         end() const;
  const_iterator         cend() const;

  const_reverse_iterator rbegin() const;
  const_reverse_iterator crbegin() const;

  const_reverse_iterator rend() const;
  const_reverse_iterator crend() const;

  // capacity
  bool empty() const;
  size_type size() const;

  // element access
  const_reference operator[](size_type n) const;
  const_reference at(size_type n) const;
  const_reference front() const;
  const_reference back() const;

  const_pointer data() const;
};
\end{plstlisting}
\Indx{view@{\tt view}!T* iterator specialization@{\tt T*} iterator specialization}
\begin{quote}
\concepts DefaultConstructible, CopyConstructible, CopyAssignable,
Destructible

\uconcepts DefinitelySerializable

\pnum
A template specialization representing a view over a network buffer of
elements of type \code{T}, delimited by \code{begin()} and
\code{end()}.

\begin{sloppypar}
\exceptions \code{at(n)} throws \code{std::out_of_range} if
\code{n} is not in the range \code{[0, size())}.
\end{sloppypar}

\pnum
{\em UPC++ progress level for all member functions of \code{view}:} {\tt none}
\end{quote}

\begin{plstlisting}
template<typename T, typename IterType>
view<T, IterType>::view();
\end{plstlisting}
\Indx{view@{\tt view}!default constructor}
\begin{quote}
\precondition \code{IterType} must satisfy the ForwardIterator
C++ concept. The type
\code{std::iterator_traits<IterType>::value_type} must be the same as
\code{T}. \code{T} must be DefinitelySerializable.

\pnum
Initializes this \code{view} to represent an empty sequence.
\end{quote}

\begin{plstlisting}
template<typename IterType>
view<typename std::iterator_traits<IterType>::value_type, IterType>
  make_view(IterType begin, IterType end,
      typename std::iterator_traits<IterType>::difference_type 
        size = std::distance(begin, end));
\end{plstlisting}
\Indx{make\_view@{\tt make\_view}}
\begin{quote}
\precondition \code{IterType} must satisfy the ForwardIterator
C++ concept. The underlying element type
(\code{std::iterator_traits<IterType>::value_type})
must be DefinitelySerializable.
\code{size} must be equal to the number
of elements in \code{[begin, end)}.

\pnum
Constructs a \code{view} over the sequence delimited by \code{begin}
and \code{end}.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
template<typename Container>
view<typename Container::value_type,
     typename Container::const_iterator>
  make_view(const Container &container);
\end{plstlisting}
\Indx{make\_view@{\tt make\_view}}
\begin{quote}
\precondition \code{Container} must satisfy the Container
C++ concept. The underlying element type (\code{Container::value_type})
must be DefinitelySerializable.

\pnum
Constructs a \code{view} over the sequence delimited by
\code{container.cbegin()} and \code{container.cend()}.

\progresslevel{none}
\end{quote}
