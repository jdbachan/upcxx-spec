
\chapter{Teams}
\label{ch:teams}

\commentout{
\noindent
\begin{plstlisting}[frame=single,style=upcxx] 
namespace upcxx {

 class team;
 class team_id;

 intrank_t team::rank_n() const;
 intrank_t team::rank_me() const;

 // Converts peer_index to index in world()
 intrank_t team::operator[](intrank_t peer_index) const;

 // Converts index in world to peer in this team.
 intrank_t team::from_world(intrank_t world_index) const;
 intrank_t team::from_world(intrank_t world_index,
                            intrank_t otherwise) const;

 team team::split(intrank_t color, intrank_t key);

 team_id team::id() const;

 team& team_id::here() const;
 future<team &> team_id::when_here() const;

 team& world();
 
 intrank_t rank_n() const;
 intrank_t rank_me() const;

 team& local_team();
 bool local_team_contains(intrank_t world_index);

 void barrier(team &team = world());
 future<> barrier_async(team &team = world());

 template<typename T, typename BinaryOp>
 future<T> reduce(T &&value, BinaryOp &&op, team &team = world());


} // namespace upcxx
\end{plstlisting}
}

\section{Overview}
\pnum
\upcxx provides {\em teams} as a means of grouping processes. \upcxx uses 
\teams for collective operations (Ch. \ref{ch:collectives}). \team
construction is collective and should be
considered moderately expensive and done as part of the set-up phase
of a calculation. \teams are similar to {\tt MPI\_Group}s and the default
\team is \code{world()}.  \teams are considered special when it comes to 
serialization.  Each \team has a unique \code{team_id} that is equal
across the \team and acts as an opaque handle.  Any process that is a member of 
the \team can retrieve the \team
object with the \code{team_id::here()} function.  Hence, coordinating
processes can reference specific \teams by their \code{team_id}.

\pnum
While a process within a \upcxx SPMD program can have multiple \code{intrank_t} 
values that represent their relative placement in several \teams, it is the
\code{intrank_t} in the \code{world()} team that is used in most \upcxx functions,
unless otherwise specifically noted.  For example, \broadcast takes a rank
relative to the specific team over which it operates.


%  \upcxx
%uses this feature when a \team is used as an argument in a remote
%procedure call. The \RPCs are defined in terms of ranks within the 
%global \team  \code{world()}.  

\section{Local Teams}
\label{sec:localTeam}

\pnum
The local team is an ordered set of processes where heap storage in the shared
segment allocated by any process in the team is local to all members.  
Any process can obtain a reference to the local team by calling \localteam and
global pointers behave accordingly:

\begin{enumerate}
\item
\globalptr's referencing objects allocated in the shared segment of 
processes that are members of this \team will 
report \code{is_local() == true} and \code{local()} will return a valid 
\code{T*} referencing the corresponding object.

\item
The \globalptr \code{where()} function will report
the rank in team \code{world()} of the process that originally acquired the referenced object using the 
functions in chapter \ref{ch:storage}. 
\end{enumerate}

\pnum
It is {\it not} guaranteed that the \code{T*}'s 
obtained by different processes to the same shared object will have bit-wise 
identical pointer values. In the general case, peers may have different 
virtual addresses for the same physical memory.  


\section{API Reference}

\subsection{team}

\begin{plstlisting}
class team;
\end{plstlisting}
\Indx{team@{\tt team}}
\begin{quote}
\concepts MoveConstructible, Destructible
\end{quote}

\begin{plstlisting}
constexpr intrank_t team::color_none;
\end{plstlisting}
\Indx{team@{\tt team}!color\_none@{\tt color\_none}}
\begin{quote}
\pnum
A constant used to specify that the calling process of \code{split()}
will not be a member of any subteam. This constant is guaranteed to
have a negative value.
\end{quote}

\begin{plstlisting}
intrank_t team::rank_n() const;
\end{plstlisting}
\Indx{team@{\tt team}!rank\_n@{\tt rank\_n}}
\begin{quote}
\pnum
Returns the number of ranks in the given team.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
intrank_t team::rank_me() const;
\end{plstlisting}
\Indx{team@{\tt team}!rank\_me@{\tt rank\_me}}
\begin{quote}
\pnum
Returns the rank of the calling process in the given team.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
intrank_t team::operator[](intrank_t peer_index) const;
\end{plstlisting}
\Indx{team@{\tt team}!operator[]@{\tt operator[]}}
\begin{quote}
\precondition \code{peer_index >= 0} and
\code{peer_index < rank_n()}.

\pnum
Returns the rank in the \code{world()} team of the process
with rank \code{peer_index} in this team.

\pnum
{\em UPC++ progress level:} unspecified between {\tt none} and {\tt internal}
\end{quote}

\begin{plstlisting}
intrank_t team::from_world(intrank_t world_index) const;
intrank_t team::from_world(intrank_t world_index,
                           intrank_t otherwise) const;
\end{plstlisting}
\Indx{team@{\tt team}!from\_world@{\tt from\_world}}
\begin{quote}
\precondition \code{world_index >= 0} and
\code{world_index < world().rank_n()}. 
For the single-argument overload,
the process with rank \code{world_index} in the \code{world()} team
must be a member of this team.

\pnum
Returns the rank in this team of the process with rank
\code{world_index} in the \code{world()} team. For the two-argument
overload, if that process is not a member of this team then the value
of \code{otherwise} is returned.

\pnum
{\em UPC++ progress level:} unspecified between {\tt none} and {\tt internal}
\end{quote}

\begin{plstlisting}
team team::split(intrank_t color, intrank_t key);
\end{plstlisting}
\Indx{team@{\tt team}!split@{\tt split}}
\begin{quote}
\collective{this (i.e. the parent) team}

\precondition \code{color >= 0 || color == team::color_none}

\pnum
Splits the given team into subteams based on the \code{color} and \code{key}
arguments.
If \code{color == team::color_none}, the return value is an invalid
team that cannot be used in any \upcxx operation except \code{\~team}.
Otherwise, all processes that call the function with the same \code{color}
value will be separated into the same subteam. Ranks in the same
subteam will be numbered according to their position in the sequence
of sorted key values. If two callers specify the same combination of
\code{color} and \code{key}, their relative ordering in the subteam
will be the same as in the parent team. The return value is the team
representing the calling processes's new subteam.

\pnum
This call will invoke user-level progress,
so the caller may expect incoming \RPCs to fire before it returns.

\ordering With respect to all threads participating in
this collective, all evaluations which are {\em sequenced-before} their
respective thread's invocation of this call will have a
{\em happens-before} relationship with all evaluations sequenced after
the call.

\progresslevel{user}
\end{quote}

\begin{plstlisting}
team::team(team &&other);
\end{plstlisting}
\Indx{team@{\tt team}!move constructor}
\begin{quote}
\precondition Calling thread must have the master persona.
No operation on this team, nor any \upcxx operation with a progress
level other than \code{none}, may
have been invoked by the calling process between the creation of
\code{other} and this call.

\pnum
Makes this instance the calling process's representative of the team
associated with \code{other}, transferring all state from
\code{other}. Invalidates \code{other}, and any subsequent operations
on \code{other}, except for destruction, produce undefined behavior.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
void team::destroy(entry_barrier lev = entry_barrier::user);
\end{plstlisting}
\Indx{team@{\tt team}!destroy@{\tt destroy}}
\begin{quote}
\collective{this team}

\precondition This instance must not have
been invalidated by being passed to the move constructor, and it must
not be an invalid team that resulted from a call to \code{split()}.
\code{lev} must be single-valued (Ch. \ref{ch:collectives}).
After the entry barrier specified by \code{lev} completes, or upon
entry if \code{lev == entry_barrier::none},
the operations on this team must not
require internal-level or user-level progress from any persona
before they can complete.

\pnum
Destroys the calling process's state associated with the team. Further
lookups on this process using the \code{team_id} corresponding to this
team will have undefined behavior.

\ordering If \code{lev != entry_barrier::none}, with respect to all
threads participating in this collective, all evaluations which are
{\em sequenced-before} their respective thread's invocation of this
call will have a {\em happens-before} relationship with all
evaluations sequenced after the call.

\progresslevel{user {\rm if \code{lev == entry_barrier::user};}
  internal {\rm otherwise}}
\end{quote}

\begin{plstlisting}
team::~team();
\end{plstlisting}
\Indx{team@{\tt team}!destructor}
\begin{quote}
\precondition Either \upcxx must have been uninitialized since the
team's creation, or
the team must either have had \code{destroy()} called on
it, been invalidated by being passed to the move constructor, or be an
invalid team that resulted from a call to \code{split()}.

\pnum
Destructs this \code{team} object.

\permituninit

\progresslevel{none}
\end{quote}

\begin{plstlisting}
team_id team::id() const;
\end{plstlisting}
\Indx{team@{\tt team}!team\_id@{\tt team\_id}}
\begin{quote}
\pnum
Returns the universal name associated with this team.

\progresslevel{none}
\end{quote}

\subsection{team\_id}

\begin{plstlisting}
class team_id;
\end{plstlisting}
\Indx{team\_id@{\tt team\_id}}
\begin{quote}
\concepts DefaultConstructible, TriviallyCopyable,
StandardLayoutType, EqualityComparable, LessThanComparable, hashable

\uconcepts DefinitelyTriviallySerializable

\pnum
A universal name representing a team.
\end{quote}

\begin{plstlisting}
team_id::team_id();
\end{plstlisting}
\Indx{team\_id@{\tt team\_id}!default constructor}
\begin{quote}
\pnum
Initializes this name to be an invalid ID.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
team& team_id::here() const;
\end{plstlisting}
\Indx{team\_id@{\tt team\_id}!here@{\tt here}}
\begin{quote}
\precondition This name must be a valid ID. The calling process
must be a member of the \code{team}
associated with this name, and it must have completed creation of the
\code{team}. The \code{team} must not have been destroyed.

\pnum
Retrieves a reference to the \code{team} instance associated with this
name.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
future<team &> team_id::when_here() const;
\end{plstlisting}
\Indx{team\_id@{\tt team\_id}!when\_here@{\tt when\_here}}
\begin{quote}
\precondition This name must be a valid ID. The calling process
must be a member of the \code{team}
associated with this name. The calling thread must have the master
persona. The \code{team} must not have been destroyed.

\pnum
Retrieves a future which is readied after the calling process constructs the
\code{team} corresponding to this name.

\progresslevel{none}
\end{quote}

\subsection{Fundamental Teams}

\begin{plstlisting}
team& world();
\end{plstlisting}
\Indx{world@{\tt world}}
\begin{quote}
\pnum
Returns a reference to the team representing all the processes in the \upcxx
program. The result is undefined if a move is performed on the
returned team.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
intrank_t rank_n();
\end{plstlisting}
\Indx{rank\_n@{\tt rank\_n}}
\begin{quote}
\pnum
Returns the number of ranks in the \code{world()} team. 

\pnum
Equivalent to: \code{world().rank_n()}.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
intrank_t rank_me();
\end{plstlisting}
\Indx{rank\_me@{\tt rank\_me}}
\begin{quote}
\pnum
Returns the rank of the calling process in the \code{world()} team. 

\pnum
Equivalent to: \code{world().rank_me()}.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
team& local_team();
\end{plstlisting}
\Indx{local\_team@{\tt local\_team}}
\begin{quote}
\pnum
Returns a reference to the local team containing this process. 
The local team represents an ordered set of processes where memory
allocated from the shared segment of any member is local to all team members
(\S\ref{sec:localTeam}). 
The result is undefined if a move is
performed on the returned team.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
bool local_team_contains(intrank_t world_index);
\end{plstlisting}
\Indx{local\_team\_contains@{\tt local\_team\_contains}}
\begin{quote}
\precondition \code{world_index >= 0} and
\code{world_index < world().rank_n()}.

\pnum
Determines if the process whose rank is \code{world_index} in \code{world()}
is a member of the local team containing the calling process (\S\ref{sec:localTeam}).

\pnum
Equivalent to: \code{local_team().from_world(world_index,-1) >= 0}

\progresslevel{none}
\end{quote}
