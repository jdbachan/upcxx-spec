\chapter{Init and Finalize}
\label{ch:initfin}

\commentout{


\noindent
\begin{plstlisting}[frame=single,style=upcxx] 
namespace upcxx {
  void init();     // initialize UPC++ library state
  void finalize(); // destroy UPC++ library state
};
\end{plstlisting}
}

\section{Overview}
\pnum
The \init function must be called before any other \upcxx function can
be invoked. This can happen anywhere in the program, so long as it
appears before any \upcxx calls that require the library to be in an
initialized state. The call is {\em collective}, meaning
every process in the parallel job must enter this function if any are
to participate in \upcxx operations. While \init can be called more
than once by each process in a program, only the first invocation will
initialize \upcxx, and the rest will merely increment the internal
count of how many times \init has been called. For each \init call, a
matching \finalize call must eventually be made. \init and \finalize
are not re-entrant and must be called by only a single thread of
execution in each process. The thread that calls \init has the {\em
  master persona} attached to it (see section \ref{sec:persona} for
more details of threading behavior). After the number of calls to
\finalize matches the number of calls to \init, no \upcxx function
that requires the library to be in an initialized state can be invoked
until \upcxx is reinitialized by a subsequent call to \init.

\pnum
All \upcxx operations require the library to be in an initialized
state unless otherwise specified, and violating this requirement
results in undefined behavior. Member functions, constructors, and
destructors are included in the set of operations that require \upcxx
to be initialized, unless explicitly stated otherwise.

\section{Hello World}
\label{sec:hello}
\begin{figure}[ht]
\begin{numlisting}
#include <upcxx/upcxx.hpp>
#include <iostream>
int main(int argc, char *argv[])
{
  upcxx::init();                        // initialize UPC++

  std::cout << "Hello World" 
    << " ranks:" << upcxx::rank_n()     // how many processes?
    << " my rank: " << upcxx::rank_me() // which process am I?
    << std::endl;

  upcxx::finalize();                    // finalize UPC++
  return 0; 
}
\end{numlisting}
\caption{{\em HelloWorld.cpp} program}
\label{fig:hello}
\end{figure}

\pnum
A \upcxx installation should be able to compile and execute the simple
{\em Hello World} program shown in Figure \ref{fig:hello}.
The output of {\em Hello World}, however,
is platform-dependent and may vary between different runs, since there
is no synchronization to order the output between processes. Depending
on the nature of the buffering protocol of {\tt stdout}, output from
different processes may even be interleaved.

\section{API Reference}
\begin{plstlisting}
void init();
\end{plstlisting}
\Indx{init@{\tt init}}
\begin{quote}
\precondition Called collectively by all processes in the
parallel job. Calling thread must have the master persona
(\S\ref{sec:persona}) if \upcxx is in an already-initialized state.

\pnum
If there have been no previous calls to \code{init}, or if all previous
calls to \code{init} have had matching calls to \code{finalize}, then
this routine initializes the \upcxx library. Otherwise, leaves the
library's state as is. Upon return, the calling thread will be attached
to the master persona (\S\ref{sec:persona}).

\permituninit
\end{quote}

\begin{plstlisting}
bool initialized();
\end{plstlisting}
\Indx{initialized@{\tt initialized}}
\begin{quote}
\pnum
Returns whether or not \upcxx is in the initialized state. \upcxx is
initialized if there has been at least one previous call to
\code{init} that has not had a matching call to \code{finalize}.

\permituninit
\end{quote}

\begin{plstlisting}
void finalize();
\end{plstlisting}
\Indx{finalize@{\tt finalize}}
\begin{quote}
\precondition Called collectively by all processes in the
parallel job. Calling thread must have the master persona
(\S\ref{sec:persona}), and \upcxx must be in an already-initialized state. 

\pnum
If this call matches the call to \code{init} that placed \upcxx in
an initialized state, then this call uninitializes the \upcxx library.
Otherwise, this function does not alter the library's state. 

\pnum
Before uninitializing the \upcxx library,
\code{finalize} shall execute a (blocking) \code{barrier()} over 
team \code{world()}.
If this call uninitializes the \upcxx library while there are any
asynchronous operations still in-flight (after the barrier), behavior is undefined.
An operation is defined as in-flight if it was initiated but
still requires internal-level or user-level progress from any persona
on any process in the job before it can complete.
It is
left to the application to define and implement their own specific
approach to ensuring quiescence of in-flight operations.   
A potential quiescence API is being considered
for future versions and feedback is encouraged.

\progresslevel{user}
\end{quote}
