\chapter{Completion}
\label{ch:completion}

\section{Overview}
\label{sec:completion_overview}
\pnum
Data movement operations come with the concept of completion, meaning
that the effect of the operation is now visible on the source or
target process and that resources, such as memory on the source and
destination sides, are no longer in use by \upcxx. A single \upcxx
call may have several completion events associated with it, indicating
completion of different stages of a communication operation. These
events are categorized as follows:
\begin{pitemize}
\item {\em Source completion:} The source-side resources of a
  communication operation are no longer in use by \upcxx, and the
  application is now permitted to modify or reclaim them.
\item {\em Remote completion:} The data have been deposited on the
remote target process, and they can be consumed by the target.
\item {\em Operation completion:} The operation is complete from the
  viewpoint of the initiator. The transferred data can now be read by
  the initiator, resulting in the values that were written to the target
  locations.
\end{pitemize}

\pnum
A completion event may be associated with some values produced by
the communication operation, or it may merely signal completion of
an action. Each communication operation specifies the set of completion
events it provides, as well as the values that a completion event
produces. Unless otherwise indicated, a completion event does not
produce a value.

\pnum
\upcxx provides several alternatives for how completion can be
signaled to the program:
\begin{pitemize}
\item {\em Future:} The communication call returns a future, which
  will be readied when the completion event occurs. This is the
  default notification mode for communication operations. If the
  completion event is associated with some values of type \code{T...},
  then the returned future will have type \code{future<T...>}. If no
  value is associated with the completion, then the future will have
  type \code{future<>}.
\item {\em Promise:} The user provides a promise when requesting
  notification of a completion event, and that promise will have one
  its dependencies fulfilled when the event occurs. The promise must
  have a non-zero dependency count. If the completion event is
  associated with some values of type \code{T...}, then it must be
  valid to call \code{fulfill_result()} on the promise with values of
  type \code{T...}, and the promise must not have had
  \code{fulfill_result()} called on it. The promise will then have
  \code{fulfill_result()} called on it with the associated values when
  the completion event occurs. If no value is associated with the
  completion, then the promise may have any type. It will have an
  anonymous dependency fulfilled upon the completion event.
\item {\em Local-Procedure Call (LPC):} The user provides a target
  persona and a callback function object when requesting notification of a
  completion event. If the completion is associated with some values
  of type \code{T...}, then the callback must be invokable with
  arguments of type \code{T...}. Otherwise, it must be invokable with
  no arguments. The callback, together with the associated completion
  values if any, is enlisted for execution on the given persona when
  the completion event occurs.
\item {\em Remote-Procedure Call (RPC):} The user provides a
  Serializable function object when requesting notification
  of a completion
  event, as well as the arguments on which the function object should be
  invoked. Each argument must either be DefinitelySerializable, a
  \code{dist_object<T>&}, or \code{team&}. The function object and
  arguments
  are transferred as part of the communication operation, and the
  invocation is enlisted for execution on the master persona of the
  target process when the completion event occurs.
\item {\em Buffered:} The communication call consumes the source-side
  resources of the operation before the call returns, allowing the
  application to immediately modify or reclaim them. This delays the
  return of the call until after the source-completion event. The
  implementation may internally buffer the source-side resources or
  block until network resources are available to inject the data
  directly.
\item {\em Blocking:} This is similar to buffered completion, except
  that the implementation is required to block until network resources
  are available to inject the data directly.
\end{pitemize}
\pnum
Future, promise, and LPC completions are only valid for completion
events that occur at the initiator of a communication call, namely
source and operation completion. RPC completion is only valid for a
completion event that occurs at the target of a communication
operation, namely remote completion. Buffered and blocking completion
are only
valid for source completion. More details on futures and
promises are in Ch. \ref{ch:futures}, while LPC and RPC callbacks
are discussed in Ch. \ref{ch:progress}.

\pnum
Notification of completion only happens during user-level progress of
the initiator or target process. Even if an operation completes early,
including before the initiation operation returns, the application
cannot learn this fact without entering user progress. For futures and
promises, only when the initiating thread (persona actually) enters
user-level progress will the future or promise be eligible for taking
on a readied or fulfilled state. LPC callbacks will execute once a thread
enters user progress of the designated persona. See Ch.
\ref{ch:progress} for the full discussion on user progress and
personas.

\pnum
If buffered or blocking completion is requested, then the
source-completion event
occurs before the communication call returns. However,
source-completion notifications, such as triggering a future or
executing an LPC, are still delayed until the next user-level
progress.

\pnum
Operation completion implies both source and remote completion.
However, it does not imply that the actions associated with source and
remote completion have been executed.

\section{Completion Objects}

\pnum
The \upcxx mechanism for requesting notification of completion is
through opaque \emph{completion objects}, which associate notification
actions with completion events. Completion objects are
CopyConstructible, CopyAssignable, and Destructible, and the same
completion object may be passed to multiple communication calls. A
simple completion object
is constructed by a call to a static member function of the
\code{source_cx}, \code{remote_cx}, or \code{operation_cx} class,
providing notification for the corresponding event. The member
functions \code{as_future}, \code{as_promise}, \code{as_lpc}, and
\code{as_rpc} request notification through a future, promise, LPC, or
RPC, respectively. Only the member functions that correspond to valid
means of signaling notification of an event are defined in the class
associated with that event.

\pnum
The following is an example of a simple completion object:

\begin{numlisting}
global_ptr<int> gp1 = /* some global pointer */;
promise<int> pro1;
auto cxs = operation_cx::as_promise(pro1);
rget(gp1, cxs);
pro1.finalize(); // fulfill the initial anonymous dependency
\end{numlisting}

\pnum
The \code{rget} function, when provided just a \code{global_ptr<int>},
transfers a single \code{int} from the given location to the
initiator. Thus, operation completion is associated with an \code{int}
value, and the promise used for signaling that event must have type
compatible with an \code{int} value, e.g. \code{promise<int>}. The
user constructs a completion object that
requests operation notification on the promise \code{pro1} by calling
\code{operation_cx::as_promise(pro1)}. Since a completion object is
opaque, the \code{auto} keyword is used to deduce the type of the
completion object. The resulting completion object can then be passed
to \code{rget}, which fulfills the promise with the transferred value
upon operation completion.

\pnum
A user can request notification of multiple completion events, as well
as multiple notifications of a single completion event. The pipe
(\code{|}) operator can be used to combine completion objects to
construct a union of the operands. The following is an example:

\begin{numlisting}
int foo() {
  return 0;
}

int bar(int x) {
  return x;
}

void do_comm(double *src, size_t count) {
  global_ptr<double> dest = /* some global pointer */;
  promise<> pro1;
  persona &per1 = /* some persona */;
  auto cxs = (operation_cx::as_promise(pro1) |
              source_cx::as_future() |
              operation_cx::as_future() |
              operation_cx::as_future() |
              source_cx::as_lpc(per1, foo) |
              remote_cx::as_rpc(bar, 3)
              );
  std::tuple<future<>, future<>, future<>> result =
    rput(src, dest, count, cxs);
  pro1.finalize().wait(); // finalize promise, wait on its future
}
\end{numlisting}

\pnum
This code initiates an \code{rput} operation, which provides source-,
remote-, and operation-completion events. A unified completion object
is constructed by applying the pipe operator to individual completion
objects. When \code{rput} is invoked with the resulting unified
completion object, it returns a tuple of futures corresponding to the
individual future completions requested. The ordering of futures in
this tuple matches the order of application of the pipe operator (this
operator is associative but not commutative). In
the example above, the first future in the tuple would correspond to
source completion, and the second and third would be for operation
completion. If no future-based notification is requested, then the
return type of the communication call would be \code{void} rather than
a tuple.

\pnum
When multiple notifications are requested for a single event, the
order in which those notifications occur is unspecified. In the code
above, the order in which \code{pro1} is fulfilled and the two
futures for operation completion are readied is indeterminate. 
Similarly, if both source and operation completion occur before the
next user-level progress, the order in which the notifications occur
is unspecified, so that operation-completion requests may be notified
before source-completion requests.

\pnum
Unlike a direct call to the \code{rpc} function (Ch. \ref{ch:rpc}),
but like a call to \code{rpc_ff}, an
RPC completion callback does not return a result to the initiator.
Thus, the value returned by the RPC invocation of \code{bar} above is
discarded.

\pnum
Arguments to \code{remote_cx::as_rpc} are serialized at an unspecified
time between the invocation of \code{as_rpc} and the source completion
event of a communication operation that accepts the resulting
completion object. If multiple communication operations use a single
completion object resulting from \code{as_rpc}, then the arguments may
be serialized multiple times. For arguments that are not passed by
value, the user must ensure that they remain valid until source
completion of all communication operations that use the associated
completion object.

\subsection{Restrictions}

\pnum
The API reference for a \upcxx call that supports the completion
interface lists the completion events that the call provides, as well
as the types of values associated with each event, if any. The result
is undefined if a completion object is passed to a call and the object
contains a request for an event that the call does not support.
Passing a completion object that contains a request whose type does
not match the types provided by the corresponding completion event, as
described in \S\ref{sec:completion_overview}, also results in
undefined behavior.

\pnum
If a \upcxx call provides both operation and remote completion, then
at least one must be requested by the provided completion object. If a
call provides operation but not remote completion, then operation
completion must be requested. The behavior of the program is undefined
if neither operation nor remote completion is requested from a call
that supports one or both of operation or remote completion.

\pnum
A promise object associated with a promise-based completion request
must have a dependency count greater than zero when the completion
object is passed to a \upcxx operation. The result is undefined if the
same promise object is used in multiple requests for notifications
that produce values.

\subsection{Completion and Return Types}

\pnum
In subsequent API-reference sections, the opaque type of a completion
object is denoted by \code{CType}.  
\Indx{CType@{\tt CType}}
Similarly, \code{RType} denotes a
\Indx{RType@{\tt RType}}
return type that is dependent on the completion object passed to a
\upcxx call.
This return type is as follows:
\begin{pitemize}
\item \code{void}, if no future-based completions are requested
\item \code{future<T...>}, if a single future-based completion is
  requested, where \code{T...} is the sequence of types associated
  with the given completion event
\item \code{std::tuple<future<T...>...>}, if multiple future-based
  completions are requested, where each future's arguments \code{T...}
  is the sequence of types associated with the corresponding
  completion event
\end{pitemize}
\pnum
Type deduction, such as with \code{auto}, is recommended when working
with completion objects and return types.

\subsection{Default Completions}

\pnum
If a completion object is not explicitly provided to a communication
call, then a default completion object is used. For most calls, the
default is \code{operation_cx::as_future()}. However, for
\code{rpc_ff}, the default completion is
\code{source_cx::as_buffered()}, and for \code{rpc}, it is
\code{source_cx::as_buffered() | operation_cx::as_future()}. The
default completion of a \upcxx communication call is listed in its API
reference.

\section{API Reference}

\begin{plstlisting}
struct source_cx;

struct remote_cx;

struct operation_cx;
\end{plstlisting}
\Indx{source\_cx@{\tt source\_cx}}
\Indx{remote\_cx@{\tt remote\_cx}}
\Indx{operation\_cx@{\tt operation\_cx}}
\begin{quote}
\pnum
Types that contain static member functions for constructing completion
objects for source, remote, and operation completion.
\end{quote}

\begin{plstlisting}
[static] CType source_cx::as_future();

[static] CType operation_cx::as_future();
\end{plstlisting}
\Indx{source\_cx@{\tt source\_cx}!as\_future@{\tt as\_future}}
\Indx{operation\_cx@{\tt operation\_cx}!as\_future@{\tt as\_future}}
\begin{quote}
\pnum
Constructs a completion object that represents notification of source
or operation completion with a future.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
template<typename ...T>
[static] CType source_cx::as_promise(promise<T...> &pro);

template<typename ...T>
[static] CType operation_cx::as_promise(promise<T...> &pro);
\end{plstlisting}
\Indx{source\_cx@{\tt source\_cx}!as\_promise@{\tt as\_promise}}
\Indx{operation\_cx@{\tt operation\_cx}!as\_promise@{\tt as\_promise}}
\begin{quote}
\precondition \code{pro} must have a dependency count greater
than zero.

\pnum
Constructs a completion object that represents signaling the given
promise upon source or operation completion.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
template<typename Func>
[static] CType source_cx::as_lpc(persona &target, Func func);

template<typename Func>
[static] CType operation_cx::as_lpc(persona &target, Func func);
\end{plstlisting}
\Indx{source\_cx@{\tt source\_cx}!as\_lpc@{\tt as\_lpc}}
\Indx{operation\_cx@{\tt operation\_cx}!as\_lpc@{\tt as\_lpc}}
\begin{quote}
\precondition \code{Func} must be a function-object type and
CopyConstructible.
\code{func} must not throw an exception when invoked.

\pnum
Constructs a completion object that represents the enqueuing of
\code{func} on the given local persona upon source or operation
completion.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
template<typename Func, typename ...Args>
[static] CType remote_cx::as_rpc(Func func, Args... &&args);
\end{plstlisting}
\Indx{remote\_cx@{\tt remote\_cx}!as\_rpc@{\tt as\_rpc}}
\begin{quote}
\precondition \code{Func} must be Serializable and
CopyConstructible and a function-object type. Each of \code{Args...}
must either be a DefinitelySerializable and CopyConstructible type, or
\code{dist_object<T>&}, or \code{team&}.
The call \code{func(args...)} must not throw an exception.

\pnum
Constructs a completion object that represents the enqueuing of
\code{func} on a target process upon remote completion.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
[static] CType source_cx::as_buffered();
\end{plstlisting}
\Indx{source\_cx@{\tt source\_cx}!as\_buffered@{\tt as\_buffered}}
\begin{quote}
\pnum
Constructs a completion object that represents buffering source-side
resources or blocking until they are consumed before a communication
call returns, delaying the return
until the source-completion event occurs.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
[static] CType source_cx::as_blocking();
\end{plstlisting}
\Indx{source\_cx@{\tt source\_cx}!as\_blocking@{\tt as\_blocking}}
\begin{quote}
\pnum
Constructs a completion object that represents blocking until
source-side resources are consumed before a communication call
returns, delaying the return until the source-completion event occurs.

\progresslevel{none}
\end{quote}

\begin{plstlisting}
template<typename CTypeA, CTypeB>
CType operator|(CTypeA a, CTypeB b);
\end{plstlisting}
\Indx{CType@{\tt CType}!operator@{\tt operator\textbar}}
\begin{quote}
\precondition \code{CTypeA} and \code{CTypeB} must be completion
types.

\pnum
Constructs a completion object that is the union of the completions in
\code{a} and \code{b}. Future-based completions in the result are
ordered the same as in \code{a} and \code{b}, with those in \code{a}
preceding those in \code{b}.

\progresslevel{none}
\end{quote}
