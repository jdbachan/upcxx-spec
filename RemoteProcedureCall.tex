\chapter{Remote Procedure Call}
\label{ch:rpc}

\commentout{
\noindent
\begin{plstlisting}[frame=single,style=upcxx] 
namespace upcxx {

 template<typename Func, typename ...Args>
 void rpc_ff(intrank_t recipient, Func &&func, Args &&...args);

 template<typename Func, typename ...Args>
 future_invoke_result_t<Func, Args...>
   rpc(intrank_t recipient, Func &&func, Args &&...args);

 template<typename Func, typename ...Args>
 void rpc(intrank_t recipient,
          promise_invoke_result_t<Func, Args...> &pro,
          Func &&func, Args &&...args);

 // signaling puts

 template<typename T,
          typename RemoteCompletionFunc,
          typename ...RemoteCompletionArgs>
 future<> rput_then_rpc(
   T const *src, global_ptr<T> dest,
   std::size_t count,
   RemoteCompletionFunc &&remote_completion_func,
   RemoteCompletionArgs &&...remote_completion_args);

 template<typename T,
          typename RemoteCompletionFunc,
          typename ...RemoteCompletionArgs>
 void rput_then_rpc(
   T const *src, global_ptr<T> dest,
   std::size_t count,
   promise<> &source_completion,
   RemoteCompletionFunc &&remote_completion_func,
   RemoteCompletionArgs &&...remote_completion_args);

 template<typename T, typename SourceCompletionFunc,
          typename RemoteCompletionFunc,
          typename ...RemoteCompletionArgs>
 void rput_then_rpc(
   T const *src, global_ptr<T> dest,
   std::size_t count,
   persona &source_completion_persona,
   SourceCompletionFunc source_completion_func,
   RemoteCompletionFunc &&remote_completion_func,
   RemoteCompletionArgs &&...remote_completion_args);

} // namespace upcxx
\end{plstlisting}
}

\section{Overview}

\pnum
\upcxx provides remote procedure calls (RPCs) for injecting function calls into other processes.
These injections are 
one-sided, meaning the recipient is not required to explicitly 
acknowledge which functions are expected.  Concurrent with a process's 
execution, incoming \RPCs accumulate in an internal queue managed by
\upcxx. The only control a process has over inbound \RPCs
is when it would like to check its inbox for arrived function calls and 
execute them. Draining the \RPC inbox  is one of the many 
responsibilities of the progress API (see Ch. \ref{ch:progress}, {\em 
Progress}).

\pnum
There are two main flavors of \RPC in \upcxx: {\em fire-and-forget} (\rpcff) and
{\em round trip} (\rpc). Each takes a function {\tt Func} together
with variadic arguments {\tt Args}. \commentout{
\begin{plstlisting}
template<typename Func, typename ...Args>
void upcxx::rpc_ff(intrank_t recipient, Func &&func, Args &&...args);

template<typename Func, typename ...Args>
future</*result type of func(args...)*/> upcxx::rpc(
  intrank_t recipient,
  Func &&func, Args &&...args);
\end{plstlisting}
}

\pnum
The \code{rpc_ff} call serializes the given function 
and arguments into a message destined for the recipient, and guarantees 
that this function call will be placed eventually in the recipient's 
inbox. The round-trip \rpc call does the same, but also
forces the recipient to reply to the sender  of the \RPC with
a message containing 
the return value of the function, providing the value for operation
completion of the sender's invocation of \rpc. Thus,
when the future is ready, the sender knows the recipient has executed 
the function call. Additionally, if the return value of \code{func} is a
future, the recipient will wait for that future to become ready before 
sending its result back to the sender.

\pnum
There are important restrictions on what the permissible types for
\code{func} and its bound arguments can be for \RPC functions. First, the
\code{Func} type must be a function object (has a publicly accessible
overload of the function call operator, \code{operator()}). Second,
\code{Func} must be Serializable, and all \code{Args...}
types must be DefinitelySerializable (see Ch. \ref{ch:serialization},
{\em Serialization}).

\section{Remote Hello World Example}

\pnum
Figure \ref{fig:rpchello} shows a simple alternative {\em Hello World} 
example where each process issues an \rpc to its neighbor, where the last rank wraps around to 0.

\begin{figure}[h]
\begin{numlisting}
#include <upcxx/upcxx.hpp>
#include <iostream>
void hello_world(intrank_t num){ 
  std::cout << "Rank " << num <<"  told rank " << upcxx::rank_me()
     << " to say Hello World" << std::endl;
}
int main(int argc, char** argv[]){
  upcxx::init();           // Start UPC++ state
  intrank_t remote = (upcxx::rank_me()+1)%upcxx::rank_n();
  auto f = upcxx::rpc(remote, hello_world, upcxx::rank_me());
  f.wait();
  upcxx::finalize();      // Close down UPC++ state
  return 0;
}
\end{numlisting}
\caption{HelloWorld with Remote Procedure Call}
\label{fig:rpchello}
\end{figure}

\section{API Reference}

\begin{plstlisting}
template<typename Func, typename ...Args>
void rpc_ff(intrank_t recipient, Func &&func, Args &&...args);
template<typename Completions, typename Func, typename ...Args>
RType rpc_ff(intrank_t recipient, Completions cxs,
             Func &&func, Args &&...args);
template<typename Func, typename ...Args>
void rpc_ff(team &team, intrank_t recipient,
            Func &&func, Args &&...args);
template<typename Completions, typename Func, typename ...Args>
RType rpc_ff(team &team, intrank_t recipient, Completions cxs,
             Func &&func, Args &&...args);
\end{plstlisting}
\Indx{rpc\_ff@{\tt rpc\_ff}}
\begin{quote}
\precondition \code{Func} must be a Serializable type
and a function-object type. Each of \code{Args...} must be a
DefinitelySerializable
type, or \code{dist_object<T>&}, or \code{team&}. The call
\code{func(args...)} must not throw an exception.

\pnum
In the first and third variants, the \code{func} and \code{args...} are
serialized and internally buffered before the call returns. The call
\code{rpc_ff(rank, func, args...)} is equivalent to \code{rpc_ff(rank,
  source_cx::as_buffered(), func, args...)}.

\pnum
In the second and fourth variants, if buffered source completion is
not requested,
the \code{func} and \code{args...} are serialized at an unspecified
time between the invocation of \code{rpc_ff} and source completion.
The serialized results are retained internally until they are
eventually sent.

\pnum
In the first two variants, the target of the RPC is the process whose
rank is \code{recipient} in the world team (Ch. \ref{ch:teams}). In
the latter two variants, the target is the process whose rank is
\code{recipient} relative to the the given team.

\pnum
After their receipt on the target, the data are deserialized and
\code{func(args...)} is enlisted for execution during user-level
progress of the master persona. So long as the sending persona continues
to make internal-level progress it is guaranteed that the message will 
eventually arrive at the recipient. See \S\ref{api:progress_required} 
\code{progress_required} for an understanding of how much 
internal-progress is necessary.

\pnum
The execution of \code{func(args...)} is never performed
synchronously, even if the target is the same as the calling process and
this function is invoked during user-level progress.

\pnum
Special handling is applied to those members of \code{args} which are
either a reference to \code{dist_object} type or a \code{team}, as
described in
\S\ref{sec:special_rpc_args}.

\begin{completions}
\item{Source} Indicates completion of serialization of the
  function object and arguments.
\end{completions}

\ordering All evaluations {\em sequenced-before} this
call will have a {\em happens-before} relationship with the
source-completion notification actions (future readying, promise
fulfillment, or persona LPC enlistment) and the recipient's invocation
of \code{func}.

\progresslevel{internal}
\end{quote}

\begin{plstlisting}
template<typename Func, typename ...Args>
future_invoke_result_t<Func, Args...>
  rpc(intrank_t recipient, Func &&func, Args &&...args);
template<typename Completions, typename Func, typename ...Args>
RType rpc(intrank_t recipient, Completions cxs,
          Func &&func, Args &&...args);
template<typename Func, typename ...Args>
future_invoke_result_t<Func, Args...>
  rpc(team &team, intrank_t recipient,
      Func &&func, Args &&...args);
template<typename Completions, typename Func, typename ...Args>
RType rpc(team &team, intrank_t recipient, Completions cxs,
          Func &&func, Args &&...args);
\end{plstlisting}
\Indx{rpc@{\tt rpc}}
\begin{quote}

\begin{sloppypar}
\precondition \code{Func} must be a Serializable type
and a function-object type. Each of \code{Args...} must be either a
DefinitelySerializable
type, or \code{dist_object<T>&}, or \code{team&}. Additionally,
\code{std::result_of<Func(Args...)>::type} must be either a
DefinitelySerializable type that is not \code{view<U, IterType>},
or \code{future<T...>}, where each type in \code{T...} must be
DefinitelySerializable but not \code{view<U, IterType>}. The call
\code{func(args...)} must not throw an
exception.
\end{sloppypar}

\pnum
Similar to \code{rpc_ff}, this call sends \code{func} and
\code{args...} to be executed remotely, but additionally provides an
operation-completion event that produces the value returned from the
remote invocation of \code{func(args...)}, if it is non-void.

\pnum
In the first and third variants, the \code{func} and \code{args...} are
serialized and internally buffered before the call returns. The call
\code{rpc(rank, func, args...)} is equivalent to:

\begin{plstlisting}
rpc(rank,
    source_cx::as_buffered() | operation_cx::as_future(),
    func, args...)
\end{plstlisting}

\pnum
In the second and fourth variants, if buffered source completion is
not requested,
the \code{func} and \code{args...} are serialized at an unspecified
time between the invocation of \code{rpc} and source completion.
The serialized results are retained internally until they are
eventually sent.

\pnum
In the first two variants, the target of the RPC is the process whose
rank is \code{recipient} in the world team (Ch. \ref{ch:teams}). In
the latter two variants, the target is the process whose rank is
\code{recipient} relative to the the given team.

\pnum
After their receipt on the target, the data are deserialized and
\code{func(args...)} is enlisted for execution during user-level
progress of the master persona. 

\pnum
In the first variant, the returned future is readied upon operation
completion.

\pnum
For futures provided by an operation-completion request, or promises
used in promise-based operation-completion requests, the type of the
future or promise must correspond to the return type of
\code{func(args...)} as follows:
\begin{pitemize}
\item If the return type is of the form \code{future<T...>}, then a
  future provided by operation completion also has type
  \code{future<T...>}, and promises used in operation-completion
  requests must permit invocation of \code{fulfill_result} with values
  of type \code{T...}.
\item If the return type is some other non-\code{void} type \code{T},
  then a future provided by operation completion has type
  \code{future<T>}, and promises used in operation-completion requests
  must permit invocation of \code{fulfill_result} with a value
  of type \code{T}.
\item If the return type is \code{void}, then a future provided by
  operation completion has type \code{future<>}, and promises used in
  operation-completion requests may have any type
  \code{promise<T...>}.
\end{pitemize}

\pnum
Within user-progress of the recipient's master persona, the result
from invoking \code{func(args...)} will be immediately serialized and
eventually sent back to the initiating process. Upon receipt, it will be
deserialized, and operation-completion notifications will take place
during subsequent user-progress of the initiating persona.

\pnum
The execution of \code{func(args...)} is never performed
synchronously, even if the target is the same as the calling process and
this function is invoked during user-level progress.

\pnum
The same special handling applied to \code{dist_object&} and \code{team&}
arguments by \code{rpc_ff} is also done by \code{rpc}.

\begin{completions}
\item{Source} Indicates completion of serialization of the
  function object and arguments.
\item{Operation} Indicates completion of all aspects of the
  operation: serialization, deserialization, remote invocation,
  transfer of any result, and destruction of any internally managed
  values are complete. This completion produces a value as described
  above.
\end{completions}

\ordering All evaluations {\em sequenced-before} this
call will have a {\em happens-before} relationship with the invocation
of \code{func}. The return from \code{func}, will have a {\em
  happens-before} relationship with the operation-completion actions
(future readying, promise fulfillment, or persona LPC enlistment). For
LPC completions, all evaluations {\em sequenced-before} this call will
have a {\em happens-before} relationship with the execution of the
completion function.

\progresslevel{internal}
\end{quote}
